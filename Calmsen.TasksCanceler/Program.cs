﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.TasksCanceler
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var cancellationToken = CancellationToken.None;
            var tokenSource1 = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            var tokenSource2 = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            var tasksCanceler = new TasksCanceler(new[] { tokenSource1, tokenSource2 });
            var task1 = Method1(tokenSource1.Token);
            var task2 = Method2(tokenSource2.Token);
            tasksCanceler.ApplyCancelFunction(new Task[] { task1, task2 });
            try
            {
                await Task.WhenAll(task1, task2);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                // throw tasksCanceler.Exception;
            }

            await Task.Delay(20000);
        }

        private static async Task Method1(CancellationToken cancellationToken)
        {
            var n = 0;
            while (true)
            {
                await Task.Delay(1000, cancellationToken);
                n++;
                if (n == 5)
                {
                    throw new InvalidOperationException("Some exception from task 1");
                }
            }
        }

        private static async Task Method2(CancellationToken cancellationToken)
        {
            var n = 0;
            while (true)
            {
                await Task.Delay(1000, cancellationToken);
                n++;
                if (n == 10)
                {
                    // return;
                    // throw new InvalidOperationException("Some exception from task 1");
                }
            }
        }
    }
}
