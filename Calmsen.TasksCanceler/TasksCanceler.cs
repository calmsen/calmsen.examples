﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.TasksCanceler
{
    public class TasksCanceler
    {
        private readonly CancellationTokenSource[] cancellationTokenSources;

        public TasksCanceler(CancellationTokenSource[] cancellationTokenSources)
        {
            this.cancellationTokenSources = cancellationTokenSources;
        }

        public Exception Exception { get; private set; }

        public void CancelAllTasks(Task faultedTask)
        {
            if (!faultedTask.IsFaulted)
            {
                throw new ArgumentException("Task must be faulted.");
            }

            this.Exception = faultedTask.Exception?.InnerExceptions.FirstOrDefault() ?? faultedTask.Exception;

            foreach (var cancellationTokenSource in this.cancellationTokenSources)
            {
                cancellationTokenSource.Cancel();
            }
        }

        public void ApplyCancelFunction(Task[] tasks)
        {
            foreach (var task in tasks)
            {
                task.ContinueWith(this.CancelAllTasks, TaskContinuationOptions.OnlyOnFaulted);
                task.ContinueWith(NoOperation, TaskContinuationOptions.OnlyOnRanToCompletion);
            }
        }

        private void NoOperation(Task task)
        {
        }
    }
}