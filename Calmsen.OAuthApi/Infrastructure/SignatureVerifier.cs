﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Calmsen.OAuthApi.Infrastructure
{
    public class SignatureVerifier
    {
        private readonly string _apiUrl;

        public SignatureVerifier(string apiUrl)
        {
            _apiUrl = apiUrl;
        }

        public async Task<bool> VerifySignatureBySerial(string serial, string data, string signature)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    HttpResponseMessage response = await client.PostAsJsonAsync(_apiUrl + $"/cryptography/verify-sign-by-serial/{serial}", new { data, signature });
                    if (response.IsSuccessStatusCode)
                    {
                        string isValidAsString = await response.Content.ReadAsStringAsync();
                        if (string.IsNullOrEmpty(isValidAsString))
                            return false;
                        return Convert.ToBoolean(isValidAsString);
                    }
                }
            }
            catch (Exception e)
            {
                // TODO: logging
            }
            return false;  
        }
    }
}