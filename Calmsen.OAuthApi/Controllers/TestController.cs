﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Calmsen.OAuthApi.Controllers
{
    [RoutePrefix("api/test")]
    public class TestController : ApiController
    {
        [Route("")]
        [HttpGet]
        public string Test()
        {
            return "TEST";
        }

        [Route("auth")]
        [HttpGet]
        [Authorize]
        public string TestAuth()
        {
            return $"TEST AUTH: {User.Identity.Name}";
        }

        [Route("internal-services")]
        [HttpGet]
        [Authorize(Roles = "InternalServices")]
        public string TestInternalServices()
        {
            return $"TEST INTERNAL SERVICES: {User.Identity.Name} has role InternalServices";
        }
    }
}
