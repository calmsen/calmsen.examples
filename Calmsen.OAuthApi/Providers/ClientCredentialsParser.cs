﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calmsen.OAuthApi.Providers
{
    public class ClientCredentialsParser
    {
        public List<ClientCredential> ParseClientCredentials(string clientCredentialsAsString)
        {
            var clientCredentials = new List<ClientCredential>();
            clientCredentialsAsString.Replace(" ", "");
            foreach(var clientCredentialAsString in clientCredentialsAsString.Split(';'))
            {
                var clientCredentialParts = clientCredentialAsString.Split(',');
                var clientCredential = new ClientCredential
                {
                    ClientId = clientCredentialParts[0].Split('=')[1],
                    ClientSecret = clientCredentialParts[1].Split('=')[1]
                };
                clientCredentials.Add(clientCredential);
            }
            return clientCredentials;
        }
    }
}