﻿using Calmsen.OAuthApi.Infrastructure;
using Calmsen.OAuthApi.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Calmsen.OAuthApi.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private readonly List<ClientCredential> _clientCredentials;
        private readonly string[] _internalServices;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
            _clientCredentials = new ClientCredentialsParser().ParseClientCredentials(WebConfigurationManager.AppSettings["clientCredentials"]);
            _internalServices = WebConfigurationManager.AppSettings["internalServices"].Replace(" ", "").Split(',');
        }

        public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            var claims = context.Scope.Select(x => new Claim("urn:oauth:scope", x)).ToList();
            if ( _internalServices.Contains(context.ClientId))
            {
                claims.Add(new Claim(ClaimTypes.Role, "InternalServices"));
            }

            var identity = new ClaimsIdentity(new GenericIdentity(
                    context.ClientId, OAuthDefaults.AuthenticationType), claims);
            context.Validated(identity);
            return Task.CompletedTask;
        }


        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            ApplicationUser user = null;

            var userStore = new OracleUserStore();
            user =  null;
            // TODO: check user credentials
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties());
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.CompletedTask;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;
            context.TryGetFormCredentials(out clientId, out clientSecret);

            if (clientId == null)
            {
                context.Validated();
            }
            else if (_clientCredentials.FirstOrDefault(x => x.ClientId.Equals(clientId) && x.ClientSecret.Equals(clientSecret)) != null)
            {
                context.Validated(clientId);
            }

            return base.ValidateClientAuthentication(context);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.CompletedTask;
        }
    }
}