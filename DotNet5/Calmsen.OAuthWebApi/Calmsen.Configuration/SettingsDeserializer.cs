﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;

namespace Calmsen.Configuration
{

    public class SettingsDeserializer
    {
        /// <summary>
        /// Использовать только из класса Startup
        /// </summary>
        public static SettingsDeserializer Current;

        private JsonSerializerSettings _jsonSerializerSettings;

        public string AppName { get; private set; }
        public string Env { get; private set; }
        private readonly string _transform;
        private readonly string _binDir;

        public SettingsDeserializer(string appName, string transform, string env)
        {
            AppName = appName;
            _transform = transform;
            Env = env;

            _binDir = Path.GetDirectoryName(typeof(SettingsDeserializer).Assembly.Location).TrimEnd('/').TrimEnd('\\');
        }

        public object DeserializeSettings(Type type)
        {
            var settings = Activator.CreateInstance(type);
            string configName = type.Name;
            FillSettings(settings, configName, false);
            FillSettings(settings, configName, true);
            FillLocalSettings(settings, configName);
            TransformSettings(settings, configName, false);
            TransformSettings(settings, configName, true);

            return settings;
        }

        public bool IsEnvirement(string env)
        {
            return string.Equals(env?.ToUpper(), Env.ToUpper());
        }

        private void FillLocalSettings(object settings, string configName)
        {
            string fullFilePath = Path.Combine(_binDir, $"{configName}.Local.json");
            if (!File.Exists(fullFilePath))
                return;
            string json = File.ReadAllText(fullFilePath);
            json = ReplaceVariables(json);
            JsonConvert.PopulateObject(json, settings, JsonSerializerSettings);
        }

        private void FillSettings(object settings, string configName, bool envirement)
        {
            string fullFilePath = GetFullFilePath(_binDir, configName, envirement);
            if (fullFilePath == null)
                return;
            if (!File.Exists(fullFilePath))
                return;
            string json = File.ReadAllText(fullFilePath);
            json = ReplaceVariables(json);
            JsonConvert.PopulateObject(json, settings, JsonSerializerSettings);
        }

        private void TransformSettings(object settings, string configName, bool envirement)
        {
            if (_transform == null)
                return;

            string fullFilePath = GetFullFilePath(_binDir, _transform, envirement);
            if (fullFilePath == null)
                return;
            if (!File.Exists(fullFilePath))
                return;
            string json = File.ReadAllText(fullFilePath);
            try
            {
                var jObject = JObject.Parse(json);
                var jToken = jObject.SelectToken($"$.{configName}");
                if (jToken == null)
                    jToken = jObject.SelectToken($"$.{configName.Replace("Settings", "")}");
                if (jToken == null)
                    return;
                json = jToken.ToString();
                json = ReplaceVariables(json);
                JsonConvert.PopulateObject(json, settings, JsonSerializerSettings);
            }
            catch { }

        }

        private string GetFullFilePath(string fileDir, string configName, bool envirement)
        {
            string fullFilePath = Path.Combine(fileDir, configName);

            if (envirement)
            {
                if (Env == null)
                    return null;
                fullFilePath += $".{Env}";
            }

            fullFilePath += ".json";
            return fullFilePath;
        }

        private string ReplaceVariables(string json)
        {
            string binDirEscaped = _binDir.Replace(@"\", @"\\");
            json = json.Replace("%AssemblyLocation%", binDirEscaped);
            return json;
        }

        private JsonSerializerSettings JsonSerializerSettings
        {
            get
            {
                if (_jsonSerializerSettings == null)
                {
                    _jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    };
                }

                return _jsonSerializerSettings;
            }
        }
    }
}
