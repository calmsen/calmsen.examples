﻿namespace Calmsen.DependencyLoader.Interfaces
{
    public interface IDependencyModule
    {
        void OnLoad(IDependencyProvider dependencyProvider);
    }
}