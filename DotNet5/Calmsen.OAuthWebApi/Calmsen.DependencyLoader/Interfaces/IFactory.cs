﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calmsen.DependencyLoader.Interfaces
{
    public interface IFactory<T>
    {
        T Create();
    }
}
