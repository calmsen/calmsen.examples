﻿using System;
using System.Reflection;

namespace Calmsen.DependencyLoader.Interfaces
{
    public interface IDependencyProvider
    {
        void Init(params IDependencyModule[] modules);
        object Get(Type type);
        T Get<T>();


        void BindSettings<TFrom>()
            where TFrom : class;
        void BindFactory<TFrom, TFactory>(bool unbind = true)
            where TFrom : class
            where TFactory : class, IFactory<TFrom>;

        void Bind<TFrom, TTo>(bool unbind = true)
            where TFrom : class
            where TTo : class, TFrom;
        void Bind(Type type, bool unbind = true);
        void Bind<T>(bool unbind = true)
            where T : class;
        void Bind<T>(T obj, bool unbind = true)
            where T : class;
        void BindAllBaseClasses<TBase>();
        void BindAllBaseClasses<TBase>(params Assembly[] assemblies);
        void BindAllInterfaces<TBase>();
    }
}
