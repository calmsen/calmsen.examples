﻿using System;
using System.Linq;
using System.Reflection;
using Calmsen.DependencyLoader.Interfaces;
using Calmsen.Configuration;
using Ninject;
using Ninject.Extensions.Conventions;


namespace Calmsen.DependencyLoader
{
    public class NinjectDependencyProvider : IDependencyProvider
    {
        private StandardKernel _kernel;

        public void Init(params
            IDependencyModule[] modules)
        {
            var settings = new NinjectSettings { InjectAttribute = typeof(InjectAttribute) };
            _kernel = new StandardKernel(settings);
            foreach (var module in modules)
                module.OnLoad(this);
        }

        public object Get(Type type)
        {
            return _kernel.Get(type);
        }

        public T Get<T>()
        {
            return _kernel.Get<T>();
        }

        public void BindSettings<TFrom>()
            where TFrom : class
        {
            Bind(SettingsDeserializer.Current.DeserializeSettings(typeof(TFrom)));
        }

        public void BindFactory<TFrom, TFactory>(bool unbind = true)
            where TFrom : class
            where TFactory : class, IFactory<TFrom>
        {
            if (unbind)
            {
                _kernel.Unbind<TFrom>();
                _kernel.Unbind<TFactory>();
            }
            _kernel.Bind<TFactory>().ToSelf();
            _kernel.Bind<TFrom>().ToMethod(x => x.Kernel.Get<TFactory>().Create()).InSingletonScope();
        }

        public void Bind<TFrom, TTo>(bool unbind = true)
            where TFrom : class
            where TTo : class, TFrom
        {
            if (unbind)
            {
                _kernel.Unbind<TFrom>();
            }
            _kernel.Bind<TFrom>().To<TTo>().InSingletonScope();
        }

        public void Bind<T>(T obj, bool unbind = true)
            where T : class
        {
            if (unbind)
            {
                _kernel.Unbind(obj.GetType());
            }
            _kernel.Bind(obj.GetType()).ToConstant(obj).InSingletonScope();
        }

        public void Bind(Type type, bool unbind = true)
        {
            if (unbind)
            {
                _kernel.Unbind(type);
            }
            _kernel.Bind(type).ToSelf().InSingletonScope();
        }

        public void Bind<T>(bool unbind = true)
            where T : class
        {
            if (unbind)
            {
                _kernel.Unbind<T>();
            }
            _kernel.Bind<T>().ToSelf().InSingletonScope();
        }

        public void BindAllBaseClasses<TBase>()
        {
            _kernel.Bind(
                x => x.FromAssemblyContaining<TBase>()
                    .SelectAllClasses()
                    .InheritedFrom<TBase>()
                    .BindAllBaseClasses()
                    .Configure(y => y.InSingletonScope()));
        }

        public void BindAllBaseClasses<TBase>(params Assembly[] assemblies)
        {
            BindAllBaseClasses<TBase>();
            if (assemblies == null || assemblies.Length == 0)
                return;
            assemblies.Where(x => !string.Equals(typeof(TBase).Assembly.FullName, x.FullName));
            _kernel.Bind(
                x => x.From(assemblies)
                    .SelectAllClasses()
                    .InheritedFrom<TBase>()
                    .BindAllBaseClasses()
                    .Configure(y => y.InSingletonScope()));
        }

        public void BindAllInterfaces<TBase>()
        {
            _kernel.Bind(
                x => x.FromAssemblyContaining<TBase>()
                    .SelectAllClasses()
                    .InheritedFrom<TBase>()
                    .BindAllInterfaces()
                    .Configure(y => y.InSingletonScope()));
        }
    }
}
