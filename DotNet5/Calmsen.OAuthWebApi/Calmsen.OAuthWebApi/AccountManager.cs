﻿using Calmsen.OAuthWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi
{
    public class AccountManager
    {
        private readonly JsonWebTokenManager _jsonWebTokenManager;

        public AccountManager(JsonWebTokenManager jsonWebTokenManager)
        {
            _jsonWebTokenManager = jsonWebTokenManager;
        }

        public async Task<AccessTokenData> GrantByPassword(GenetateTokenForm form)
        {
            // TODO: get user by user_name and password from db
            var user = new ApplicationUser
            {
                Id = "123",
                UserName = "develop"
            };

            bool needGenerateRefreshToken = !string.IsNullOrEmpty(form.ClientSecret);
            var claims = GetUserClaims(user);
            return await CreateToken(
                claims,
                needGenerateRefreshToken ? TokenType.AccessToken : TokenType.LongAccessToken,
                needGenerateRefreshToken
            );
        }

        private List<Claim> GetUserClaims(ApplicationUser user)
        {

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName)
            };
            claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, "Owner"));
            if (user.Roles?.Count > 0)
            {
                foreach (var role in user.Roles)
                {
                    claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));
                }
            }

            return claims;
        }

        private async Task<AccessTokenData> CreateToken(IEnumerable<Claim> claims, TokenType tokenType, bool needGenerateRefreshToken)
        {
            var tokenInfo = _jsonWebTokenManager.CreateToken(claims, tokenType);
            
            string refreshToken = null;
            if (needGenerateRefreshToken)
            {
                var refreshTokenInfo = _jsonWebTokenManager.CreateToken(claims, TokenType.RefreshToken);
                refreshToken = refreshTokenInfo.Token;
            }
            return new AccessTokenData
            {
                AccessToken = tokenInfo.Token,
                TokenType = "Bearer",
                Scope = tokenInfo.Scope,
                ExpiresIn = (long)Math.Floor(tokenInfo.ExpiresIn.TotalSeconds),
                RefreshToken = refreshToken
            };
        }

    }
}
