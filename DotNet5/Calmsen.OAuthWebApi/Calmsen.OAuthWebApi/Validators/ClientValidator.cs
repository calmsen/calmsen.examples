﻿using Calmsen.OAuthWebApi.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi.Validators
{
    public class ClientValidator
    {
        private readonly IClientCredentials _clientCredentials;

        public ClientValidator(IClientCredentials clientCredentials)
        {
            _clientCredentials = clientCredentials;
        }

        internal Task ValidateAsync()
        {
            if (string.IsNullOrEmpty(_clientCredentials.ClientId))
                throw new InvalidDataException("client_id must be passed.");

            //if (string.IsNullOrEmpty(ClientSecret))
            //    throw new InvalidDataException("client_secret must be passed.");
            
            // TODO: create validation logic for client
            return Task.CompletedTask;
        }
    }
}
