﻿using Calmsen.OAuthWebApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi.Validators
{
    public class GrantByPasswordValidator
    {
        private readonly GenetateTokenForm _form;

        public GrantByPasswordValidator(GenetateTokenForm form)
        {
            _form = form;
        }

        internal async Task ValidateAsync()
        {
            if (string.IsNullOrEmpty(_form.UserName))
                throw new InvalidDataException("userName must be passed.");

            if (!string.IsNullOrEmpty(_form.UserName) && string.IsNullOrEmpty(_form.Password))
                throw new InvalidDataException("password must be passed.");
            // TODO: create validation logic by password
            // TODO: create ClientValidatorFactory
            await new ClientValidator(_form).ValidateAsync();
        }
    }
}
