using Calmsen.DependencyLoader.AspNet;
using Calmsen.Logging;
using Calmsen.Logging.AspNet;
using Calmsen.OAuthBusinessLogic;
using Calmsen.OAuthWebApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //var root = (IConfigurationRoot)Configuration;
            //var debugView = root.GetDebugView();

            var dependencyProvider = new AspNetCoreDependencyProvider(services);
            dependencyProvider.Init(
                new LoggingDependencyModule(),
                new LoggingAspNetDependencyModule(),
                new OAuthBusinessLogicDependencyModule()
            );
            services.AddScoped<AccountManager>();
            services.AddScoped<JsonWebTokenManager>();
            services.AddControllersWithViews();
            services.Configure<AuthOptions>(Configuration.GetSection("AuthOptions"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
