﻿using Calmsen.Logging;
using Calmsen.OAuthWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogWriter _logger;

        public HomeController(ILogWriter logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            _logger.Write("Index Page");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
