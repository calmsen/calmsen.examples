﻿using Calmsen.OAuthWebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AccountManager _accountManager;

        public AccountController(AccountManager accountManager)
        {
            _accountManager = accountManager;
        }

        [HttpPost("/token")]
        public async Task GenetateToken()
        {
            var form = new GenetateTokenForm(Request.Form);
            await form.Validate();

            AccessTokenData tokenData;
            if (string.Equals(form.GrantType, "password"))
                tokenData = await _accountManager.GrantByPassword(form);
            else
                throw new InvalidOperationException();

            // сериализация ответа
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(tokenData, Formatting.Indented));
        }
    }
}
