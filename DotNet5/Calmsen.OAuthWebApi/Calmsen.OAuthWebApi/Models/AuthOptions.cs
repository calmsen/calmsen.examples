﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Calmsen.OAuthWebApi.Models
{

    public class AuthOptions
    {

        /// <summary>
        /// Издатель токена
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// Ключ для шифрации
        /// </summary>
        public string SecretKey { get; set; }

        public int AuthorizationCodeLifetime { get; set; }
        public int ImplicitTokeLifetime { get; set; }
        public int RefreshTokeLifetime { get; set; }
        public int Lifetime { get; set; }
        public int LongLifetime { get; set; }
        public int ActivationTokenLifetime { get; set; }
        public int ActivationRefreshTokenLifetime { get; set; }
        public string ScopeClaimType { get; set; }
        public string SessionGuidClaimType { get; set; }
        public string RedirectUriHashClaimType { get; set; }
        public string OperationIdClaimType { get; set; }
        public string CodeChallengeClaimType { get; set; }
        public string CodeChallengeMethodClaimType { get; set; }
        public string[] AvailablePathToAllowAccessTokenInQuery { get; set; }
        public string RsaPrivateKey { get; set; }
        public string RsaPublicKey { get; set; }
        public bool UseRsaSecurityKey { get; set; }

        public TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters
            {
                // укзывает, будет ли валидироваться издатель при валидации токена
                ValidateIssuer = true,
                // строка, представляющая издателя
                ValidIssuer = Issuer,

                // будет ли валидироваться потребитель токена
                ValidateAudience = false,
                // будет ли валидироваться время существования
                ValidateLifetime = true,
                LifetimeValidator = LifetimeValidator,

                // установка ключа безопасности
                IssuerSigningKey = UseRsaSecurityKey ? GetRsaPublicKey() : GetSymmetricSecurityKey(),
                // валидация ключа безопасности
                ValidateIssuerSigningKey = true,
                CryptoProviderFactory = new CryptoProviderFactory()
                {
                    CacheSignatureProviders = false
                }
            };
        }

        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        }

        public RsaSecurityKey GetRsaPublicKey()
        {
            var publicKey = Convert.FromBase64String(RsaPublicKey);

            RSA rsa = RSA.Create();
            //rsa.ImportRSAPublicKey(publicKey, out _);
            rsa.ImportSubjectPublicKeyInfo(publicKey, out _);

            return new RsaSecurityKey(rsa);
        }

        public bool LifetimeValidator(DateTime? notBefore,
            DateTime? expires,
            SecurityToken securityToken,
            TokenValidationParameters validationParameters)
        {
            if (expires == null) return false;
            return DateTime.UtcNow < expires;
        }
    }
}
