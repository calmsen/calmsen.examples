﻿using Calmsen.OAuthWebApi.Interfaces;
using Calmsen.OAuthWebApi.Validators;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi.Models
{
    public class GenetateTokenForm: IClientCredentials
    {
        public string GrantType { get; }
        public string UserName { get; }
        public string Password { get; }
        public string ClientId { get; }
        public string ClientSecret { get; }

        private readonly IFormCollection _formCollection;

        public GenetateTokenForm(IFormCollection formCollection)
        {
            _formCollection = formCollection;

            GrantType = GetFormParameter("grant_type");
            UserName = GetFormParameter("user_name");
            if (!string.IsNullOrEmpty(UserName))
                UserName = UserName.Replace(' ', '+');// заменяем обратно пробелы на +, так как механизм декодирования url заменяет + на пробелы
            Password = GetFormParameter("password");
            ClientId = GetFormParameter("client_id");
            ClientSecret = GetFormParameter("client_secret");
        }
        private string GetFormParameter(string paramName)
        {
            if (string.IsNullOrEmpty(paramName))
                return null;

            string paramValue = _formCollection[paramName];
            if (string.IsNullOrEmpty(paramValue))
            {
                paramName = paramName.Split('_').Aggregate((s1, s2) => s1 + s2.Substring(0, 1).ToUpper() + s2.Substring(1)); //Camel Case
                paramValue = _formCollection[paramName];
            }
            if (string.IsNullOrEmpty(paramValue))
            {
                paramName = paramName.Substring(0, 1).ToUpper() + paramName.Substring(1); // Pascale Case
                paramValue = _formCollection[paramName];
            }
            if (string.IsNullOrEmpty(paramValue))
            {
                paramName = paramName.ToLower();
                paramValue = _formCollection[paramName];
            }
            if (!string.IsNullOrEmpty(paramValue))
                paramValue = paramValue.Trim();
            return paramValue;
        }


        public async Task Validate()
        {
            if (string.IsNullOrEmpty(GrantType))
                throw new InvalidDataException("grant_type must be passed.");

            if (!string.Equals(GrantType, "password"))
                throw new InvalidDataException("Unsupported grant_type.");


            if (string.Equals(GrantType, "password"))
                // TODO: create GrantByPasswordValidatorFactory
                await new GrantByPasswordValidator(this).ValidateAsync();
            else
                throw new InvalidDataException("Unsupported grantType");
        }
    }
}
