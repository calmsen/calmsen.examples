﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi.Models
{
    public class TokenInfo
    {
        public DateTime CreatedDateUtc { get; internal set; }
        public string Token { get; internal set; }
        public TimeSpan ExpiresIn { get; internal set; }
        public string Scope { get; internal set; }
        public TokenType Type { get; internal set; }
    }
}
