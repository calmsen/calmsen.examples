﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi.Models
{
    public enum TokenType
    {
        LongAccessToken,
        AccessToken,
        RefreshToken,
        AuthorizationCode,
        ActivationToken,
        ActivationRefreshToken
    }
}
