﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthWebApi.Models
{
    public class ApplicationUser
    {
        public string Id { get; set; }

        public string UserName { get; set; }
        public List<string> Roles { get; set; }
    }
}
