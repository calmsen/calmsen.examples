﻿using Calmsen.OAuthWebApi.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Calmsen.OAuthWebApi
{
    public class JsonWebTokenManager
    {
        private const string FakeIssuer = "_fake_";
        private readonly AuthOptions _authOptions;
        public JsonWebTokenManager(IOptions<AuthOptions> authOptions)
        {
            _authOptions = authOptions.Value;
        }

        public TokenInfo CreateToken(IEnumerable<Claim> claims, TokenType tokenType)
        {
            var identity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            return CreateToken(identity, tokenType);
        }

        public TokenInfo CreateToken(ClaimsIdentity identity, TokenType tokenType)
        {
            int lifetime = 0;
            switch (tokenType)
            {
                case TokenType.LongAccessToken:
                    lifetime = _authOptions.LongLifetime;
                    break;
                case TokenType.AccessToken:
                    lifetime = _authOptions.Lifetime;
                    break;
                case TokenType.RefreshToken:
                    lifetime = _authOptions.RefreshTokeLifetime;
                    break;
                case TokenType.AuthorizationCode:
                    lifetime = _authOptions.AuthorizationCodeLifetime;
                    break;
                case TokenType.ActivationToken:
                    lifetime = _authOptions.ActivationTokenLifetime;
                    break;
                case TokenType.ActivationRefreshToken:
                    lifetime = _authOptions.ActivationRefreshTokenLifetime;
                    break;
                default:
                    // Do nothing
                    break;
            }
            lifetime = lifetime == 0 ? 5 : lifetime;

            var now = DateTime.UtcNow;
            var expires = now.Add(TimeSpan.FromMinutes(lifetime));
            // создаем JWT-токен
            RSA rsa = null;
            var jwt = new JwtSecurityToken(
                    issuer: (tokenType == TokenType.LongAccessToken || tokenType == TokenType.AccessToken)
                        ? _authOptions.Issuer
                        : FakeIssuer,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: expires,
                    signingCredentials: _authOptions.UseRsaSecurityKey ? GetRsaSigningCredentials(out rsa) : GetHmacSigningCredentials());
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            rsa?.Dispose();
            return new TokenInfo
            {
                CreatedDateUtc = now,
                Token = encodedJwt,
                ExpiresIn = expires - now,
                Scope = string.Join(' ', identity.Claims.Where(x => x.Type == _authOptions.ScopeClaimType).Select(x => x.Value).ToArray())
            };
        }

        public ClaimsPrincipal GetPrincipal(string token, bool fakeIssuer = false)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;
                var tokenValidationParameters = _authOptions.GetTokenValidationParameters();
                if (fakeIssuer)
                    tokenValidationParameters.ValidIssuer = FakeIssuer;
                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);

                return principal;
            }
            catch (Exception)
            {
                //should write log
                return null;
            }
        }


        private SigningCredentials GetHmacSigningCredentials()
        {
            return new SigningCredentials(_authOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256);
        }

        private SigningCredentials GetRsaSigningCredentials(out RSA rsa)
        {
            var privateKey = Convert.FromBase64String(_authOptions.RsaPrivateKey);

            rsa = RSA.Create();
            rsa.ImportRSAPrivateKey(privateKey, out _);

            var signingCredentials = new SigningCredentials(new RsaSecurityKey(rsa), SecurityAlgorithms.RsaSha256)
            {
                CryptoProviderFactory = new CryptoProviderFactory { CacheSignatureProviders = false }
            };
            return signingCredentials;
        }
    }
}
