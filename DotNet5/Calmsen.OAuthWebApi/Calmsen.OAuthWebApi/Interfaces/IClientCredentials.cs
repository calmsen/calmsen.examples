﻿namespace Calmsen.OAuthWebApi.Interfaces
{
    public interface IClientCredentials
    {
        public string ClientId { get; }
        public string ClientSecret { get; }
    }
}
