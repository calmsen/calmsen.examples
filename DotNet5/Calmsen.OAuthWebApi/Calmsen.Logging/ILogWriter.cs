﻿using Calmsen.Logging;
using System;

namespace Calmsen.Logging
{
    public interface ILogWriter
    {
        void Write(Exception ex, LevelType? level = null, string categoryName = null);
        void Write(string message, LevelType? level = null, string categoryName = null);
    }
}
