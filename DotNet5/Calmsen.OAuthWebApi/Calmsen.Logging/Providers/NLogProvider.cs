﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calmsen.Logging.Providers
{
    public class NLogProvider
    {
        public string FileName { get; set; }
        public Dictionary<string, LevelType> LogLevel { get; set; }
    }
}
