﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Calmsen.Configuration;
using Calmsen.Logging;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Calmsen.Logging
{
    public class LogWriter : ILogWriter
    {
        private readonly LoggingSettings _loggingSettings;
        private string _dateInitialized;
        private Dictionary<string, LevelType> _categoryLevels;

        public LogWriter(LoggingSettings loggingSettings)
        {
            _loggingSettings = loggingSettings;
        }

        public void Init()
        {
            var dateNow = DateTime.Now.ToShortDateString();
            if (_dateInitialized != null && _dateInitialized.Equals(dateNow))
                return;
            NLog.LogManager.AutoShutdown = false;
            if (!string.IsNullOrEmpty(_loggingSettings.NLog.FileName) && !Directory.Exists(_loggingSettings.NLog.FileName))
                Directory.CreateDirectory(_loggingSettings.NLog.FileName);

            var config = new LoggingConfiguration();

            InitCategoryLevelsFromSettings();
            foreach(var item in _categoryLevels)
                ConfigureFileLog(config, item);

            LogManager.Configuration = config;
            _dateInitialized = dateNow;
        }

        public void Write(Exception ex, LevelType? level = null, string categoryName = null)
        {
            Init();

            foreach (var item in _categoryLevels)
            {
                if (categoryName != null && !string.Equals(item.Key, categoryName))
                    continue;

                var logger = LogManager.GetLogger(item.Key);
                logger.Log(MapToLogLevel(level ?? item.Value), ex, ex.Message);
            }
            
        }

        public void Write(string message, LevelType? level = null, string categoryName = null)
        {
            Init();
            foreach (var item in _categoryLevels)
            {
                if (categoryName != null && !string.Equals(item.Key, categoryName))
                    continue;

                var logger = LogManager.GetLogger(item.Key);
                logger.Log(MapToLogLevel(level ?? item.Value), message);
            }
        }

        private void InitCategoryLevelsFromSettings()
        {
            var categoryLevels = _loggingSettings?.LogLevel ?? new Dictionary<string, LevelType>();
            var nlogCategoryLevels = _loggingSettings?.NLog?.LogLevel ?? new Dictionary<string, LevelType>();
            
            foreach(var item in categoryLevels)
            {
                if (!nlogCategoryLevels.ContainsKey(item.Key))
                    nlogCategoryLevels.Add(item.Key, item.Value);
            }

            if (!nlogCategoryLevels.Any())
                nlogCategoryLevels.Add(SettingsDeserializer.Current?.AppName ?? "Default", LevelType.Information);
            _categoryLevels = nlogCategoryLevels;
        }

        private void ConfigureFileLog(LoggingConfiguration config, KeyValuePair<string, LevelType> categoryLevel)
        {
            var logfile = new FileTarget(categoryLevel.Key)
            {
                Name = categoryLevel.Key,
                FileName = Path.Combine(_loggingSettings.NLog.FileName, DateTime.Now.Date.ToString("yyyy"),
                    DateTime.Now.Date.ToString("yyyy.MM"),
                    $"{categoryLevel.Key}_{DateTime.Now.Date:yyyy.MM.dd}.txt"),
                Encoding = Encoding.Unicode
            };

            config.AddRule(
                MapToLogLevel(categoryLevel.Value),
                NLog.LogLevel.Fatal,
                logfile,
                categoryLevel.Key
            );

        }

        private NLog.LogLevel MapToLogLevel(LevelType? level)
        {
            switch (level)
            {
                case LevelType.Trace: return NLog.LogLevel.Trace;
                case LevelType.Debug: return NLog.LogLevel.Debug;
                case LevelType.Information: return NLog.LogLevel.Info;
                case LevelType.Warning: return NLog.LogLevel.Warn;
                case LevelType.Error: return NLog.LogLevel.Error;
                case LevelType.Critical: return NLog.LogLevel.Fatal;
                default: return NLog.LogLevel.Info;
            }
        }
    }
}
