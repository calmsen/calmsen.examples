﻿using Calmsen.DependencyLoader.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calmsen.Logging
{
    public class ExampleLogWriterDecoratorFactory : IFactory<ILogWriter>
    {
        private readonly LogWriter _logWriter;
        private readonly DummyLogWriter _dummyLogWriter;

        public ExampleLogWriterDecoratorFactory(LogWriter logWriter, DummyLogWriter dummyLogWriter)
        {
            _logWriter = logWriter;
            _dummyLogWriter = dummyLogWriter;
        }

        public ILogWriter Create()
        {
            var dummyLogWriterDecorator = new LogWriterDecorator<DummyLogWriter>(_dummyLogWriter, _logWriter);
            return dummyLogWriterDecorator;
        }

        public class DummyLogWriter : ILogWriter
        {
            public void Write(Exception ex, LevelType? level = null, string categoryName = null)
            {
            }

            public void Write(string message, LevelType? level = null, string categoryName = null)
            {
            }
        }
    }
}
