﻿using Calmsen.DependencyLoader.Interfaces;

namespace Calmsen.Logging
{
    public class LoggingDependencyModule : IDependencyModule
    {
        public void OnLoad(IDependencyProvider dependencyProvider)
        {
            dependencyProvider.BindSettings<LoggingSettings>();
            dependencyProvider.Bind<ILogWriter, LogWriter>();
            
        }
    }
}