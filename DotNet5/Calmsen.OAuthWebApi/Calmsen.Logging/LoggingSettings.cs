﻿
using Calmsen.Logging;
using Calmsen.Logging.Providers;
using System.Collections.Generic;

namespace Calmsen.Logging
{
    public class LoggingSettings
    {
        public NLogProvider NLog { get; set; }
        public Dictionary<string, LevelType> LogLevel { get; set; }
    }
}
