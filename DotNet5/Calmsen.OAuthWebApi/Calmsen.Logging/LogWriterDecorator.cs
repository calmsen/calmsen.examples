﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calmsen.Logging
{
    public class LogWriterDecorator<TDecorator> : ILogWriter
        where TDecorator: ILogWriter
    {
        private readonly TDecorator _decorator;
        private readonly ILogWriter _component;

        public LogWriterDecorator(TDecorator decorator, ILogWriter component)
        {
            _decorator = decorator;
            _component = component;
        }

        public void Write(Exception ex, LevelType? level = null, string categoryName = null)
        {
            _component.Write(ex, level, categoryName);
            _decorator.Write(ex, level, categoryName);
        }

        public void Write(string message, LevelType? level = null, string categoryName = null)
        {
            _component.Write(message, level, categoryName);
            _decorator.Write(message, level, categoryName);
        }
    }
}
