﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calmsen.Logging
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LevelType
    {
        Trace,
        Debug,
        Information,
        Warning,
        Error,
        Critical
    }
}
