﻿using Calmsen.Logging;
using Microsoft.Extensions.Logging;
using System;

namespace Calmsen.Logging.AspNet
{
    public class AspNetLogWriterAdapter : ILogWriter
    {
        private readonly ILogger<AspNetLogWriterAdapter> _logger;

        public AspNetLogWriterAdapter(ILogger<AspNetLogWriterAdapter> logger)
        {
            _logger = logger;
        }

        public void Write(Exception ex, LevelType? level = null, string categoryName = null)
        {
            switch (level)
            {
                case LevelType.Trace:
                    _logger.LogTrace(ex, ex.Message);
                    break;
                case LevelType.Debug:
                    _logger.LogDebug(ex, ex.Message);
                    break;
                case LevelType.Information:
                    _logger.LogInformation(ex, ex.Message);
                    break;
                case LevelType.Warning:
                    _logger.LogWarning(ex, ex.Message);
                    break;
                case LevelType.Error:
                    _logger.LogTrace(ex, ex.Message);
                    break;
                case LevelType.Critical:
                    _logger.LogCritical(ex, ex.Message);
                    break;
                default:
                    _logger.LogInformation(ex, ex.Message);
                    break;
            }
        }

        public void Write(string message, LevelType? level = null, string categoryName = null)
        {

            switch (level)
            {
                case LevelType.Trace:
                    _logger.LogTrace(message);
                    break;
                case LevelType.Debug:
                    _logger.LogDebug(message);
                    break;
                case LevelType.Information:
                    _logger.LogInformation(message);
                    break;
                case LevelType.Warning:
                    _logger.LogWarning(message);
                    break;
                case LevelType.Error:
                    _logger.LogTrace(message);
                    break;
                case LevelType.Critical:
                    _logger.LogCritical(message);
                    break;
                default:
                    _logger.LogInformation(message);
                    break;
            }
        }
    }
}
