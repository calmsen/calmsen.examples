﻿using Calmsen.DependencyLoader.Interfaces;
using Calmsen.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calmsen.Logging.AspNet
{
    public class LogWriterDecoratorFactory : IFactory<ILogWriter>
    {
        private readonly LogWriter _logWriter;
        private readonly AspNetLogWriterAdapter _aspNetLogWriterAdapter;

        public LogWriterDecoratorFactory(LogWriter logWriter, AspNetLogWriterAdapter aspNetLogWriterAdapter)
        {
            _logWriter = logWriter;
            _aspNetLogWriterAdapter = aspNetLogWriterAdapter;
        }

        public ILogWriter Create()
        {
            var aspNetLogWriterDecorator = new LogWriterDecorator<AspNetLogWriterAdapter>(_aspNetLogWriterAdapter, _logWriter);
            return aspNetLogWriterDecorator;
        }

        public class DummyLogWriter : ILogWriter
        {
            public void Write(Exception ex, LevelType? level = null, string categoryName = null)
            {
            }

            public void Write(string message, LevelType? level = null, string categoryName = null)
            {
            }
        }
    }
}
