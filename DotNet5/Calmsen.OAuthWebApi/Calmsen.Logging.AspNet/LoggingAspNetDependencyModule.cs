﻿using Calmsen.DependencyLoader.Interfaces;

namespace Calmsen.Logging.AspNet
{
    public class LoggingAspNetDependencyModule : IDependencyModule
    {
        public void OnLoad(IDependencyProvider dependencyProvider)
        {
            dependencyProvider.Bind<LogWriter>(); // используется для фабрики декоратора.
            dependencyProvider.Bind<AspNetLogWriterAdapter>(); // используется для фабрики декоратора.
            dependencyProvider.BindFactory<ILogWriter, LogWriterDecoratorFactory>();

        }
    }
}