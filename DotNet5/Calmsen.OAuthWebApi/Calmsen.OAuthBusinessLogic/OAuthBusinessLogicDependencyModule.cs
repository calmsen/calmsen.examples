﻿using Calmsen.DependencyLoader.Interfaces;
using Calmsen.OAuthBusinessLogic.Infrastructure;

namespace Calmsen.OAuthBusinessLogic
{
    public class OAuthBusinessLogicDependencyModule : IDependencyModule
    {
        public void OnLoad(IDependencyProvider dependencyProvider)
        {
            dependencyProvider.BindSettings<OAuthBusinessLogicSettings>();
            dependencyProvider.Bind<DatabaseContext>();
        }
    }
}
