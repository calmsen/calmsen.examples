﻿using Calmsen.Logging;
using Calmsen.OAuthBusinessLogic.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Calmsen.OAuthBusinessLogic.Infrastructure
{
    public class DatabaseContext : DbContext
    {
        private readonly OAuthBusinessLogicSettings _settings;
        private readonly ILogWriter _logWriter;

        //public DatabaseContext(OAuthBusinessLogicSettings settings, ILogWriter logWriter)
        //{
        //    _settings = settings;
        //    _logWriter = logWriter;            
        //}

        public DatabaseContext()
        {
        }

        public DbSet<OptionDb> Options { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_settings?.SqlConnectionString ?? "Host=localhost;Port=5432;Database=oauth_dev;Username=postgres;Password=a");
        }
        public async Task ExecuteInTransaction(Func<Task> action)
        {
            if (Database.CurrentTransaction != null)
            {
                await action();
                return;
            }

            using (IDbContextTransaction transaction = Database.BeginTransaction())
            {
                try
                {
                    await action();
                    await SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    _logWriter.Write(e);
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
