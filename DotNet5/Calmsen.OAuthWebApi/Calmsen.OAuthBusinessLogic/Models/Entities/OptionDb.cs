﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Calmsen.OAuthBusinessLogic.Models.Entities
{
    public class OptionDb
    {
        [Key]
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
