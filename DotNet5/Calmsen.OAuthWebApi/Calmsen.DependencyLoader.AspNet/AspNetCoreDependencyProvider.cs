﻿using Calmsen.Configuration;
using Calmsen.DependencyLoader;
using Calmsen.DependencyLoader.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Calmsen.DependencyLoader.AspNet
{

    public class AspNetCoreDependencyProvider : IDependencyProvider
    {
        private readonly IServiceCollection _services;

        public AspNetCoreDependencyProvider(IServiceCollection services)
        {
            _services = services;
        }

        public void Init(params IDependencyModule[] modules)
        {
            foreach (var module in modules)
                module.OnLoad(this);
        }

        public void BindSettings<TFrom>()
            where TFrom : class
        {
            Bind(SettingsDeserializer.Current.DeserializeSettings(typeof(TFrom)));
        }

        public void BindFactory<TFrom, TFactory>(bool unbind = true)
            where TFrom : class
            where TFactory : class, IFactory<TFrom>
        {

            Unbind(typeof(TFrom), unbind);
            Unbind(typeof(TFactory), unbind);
            _services.AddScoped<TFactory>();
            _services.AddScoped(x => {
                return x.GetRequiredService<TFactory>().Create();
            });
        }

        public void Bind<TFrom, TTo>(bool unbind = true)
            where TFrom : class
            where TTo : class, TFrom
        {
            Unbind(typeof(TFrom), unbind);
            _services.AddScoped<TFrom, TTo>();
        }

        public void Bind(Type type, bool unbind = true)
        {
            Unbind(type, unbind);
            _services.AddScoped(type);
        }

        public void Bind<T>(bool unbind = true)
            where T : class
        {
            Unbind(typeof(T), unbind);
            _services.AddScoped<T>();
        }

        public void Bind<T>(T obj, bool unbind = true)
            where T : class
        {
            Unbind(obj.GetType(), unbind);
            _services.AddSingleton(obj.GetType(), obj);
        }

        public void BindAllBaseClasses<TBase>()
        {
            var baseType = typeof(TBase);
            BindAllBaseClasses(baseType, baseType.Assembly);
        }
        public void BindAllBaseClasses<TBase>(params Assembly[] assemblies)
        {
            var baseType = typeof(TBase);
            BindAllBaseClasses(baseType, baseType.Assembly);

            if (assemblies == null || assemblies.Length == 0)
                return;

            assemblies.Where(x => !string.Equals(baseType.Assembly.FullName, x.FullName));
            foreach (var assembly in assemblies)
            {
                BindAllBaseClasses(baseType, assembly);
            }
        }

        public void BindAllInterfaces<TBase>()
        {
            throw new NotImplementedException();
        }

        public object Get(Type type)
        {
            throw new NotImplementedException();
        }

        public T Get<T>()
        {
            throw new NotImplementedException();
        }

        private void Unbind(Type type, bool unbind)
        {
            if (unbind)
            {
                var serviceDescriptors = _services.Where(x => x.ServiceType == type).ToList();
                foreach (var serviceDescriptor in serviceDescriptors)
                {
                    _services.Remove(serviceDescriptor);
                }
            }
        }

        private void BindAllBaseClasses(Type baseType, Assembly assembly)
        {
            var types = FindDerivedTypesFromAssembly(assembly, baseType, true);
            foreach (var type in types)
            {
                _services.AddScoped(baseType, type);
            }
        }

        private IEnumerable<Type> FindDerivedTypesFromAssembly(Assembly assembly, Type baseType, bool classOnly)
        {
            if (assembly == null)
                throw new ArgumentNullException("assembly", "Assembly must be defined");
            if (baseType == null)
                throw new ArgumentNullException("baseType", "Parent Type must be defined");

            // get all the types
            var types = assembly.GetTypes();

            // works out the derived types
            foreach (var type in types)
            {
                // if classOnly, it must be a class
                // useful when you want to create instance
                if (classOnly && !type.IsClass)
                    continue;

                if (baseType.IsInterface)
                {
                    var it = type.GetInterface(baseType.FullName);

                    if (it != null)
                        // add it to result list
                        yield return type;
                }
                else if (type.IsSubclassOf(baseType))
                {
                    // add it to result list
                    yield return type;
                }
            }
        }

    }
}
