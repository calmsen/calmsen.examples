﻿using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using Grpc.Net.Client.Web;
using SimpleGrpcService;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace gRpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE4MDEzMzEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiUi5SQUtITUFOS1VMT1YiLCJzZXNzaW9uR3VpZCI6IjZjOGIwNGIyLWU1YmUtNDNiMy04ZWQ1LWQ2MmM1YzRhYWRkNCIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6WyJVc2VyIiwiU3RhbmRhcnRSZWdpc3RyYXIiLCJSZWdpc3RyYXJBZG1pbmlzdHJhdG9yIiwiU2VydmljZUNlbnRlclJlZ2lzdHJhciJdLCJuYmYiOjE2MTk5NTgwNDEsImV4cCI6MTYxOTk1ODEwMSwiaXNzIjoiQXRsYXMtMiJ9.59PbHyWdu8MtR0C17bc9CHyB-RG1b16oSHkKxX6vJ10";
            var credentials = CallCredentials.FromInterceptor((context, metadata) =>
            {
                if (!string.IsNullOrEmpty(token))
                {
                    metadata.Add("Authorization", $"Bearer {token}");
                }
                return Task.CompletedTask;
            });

            //создаем канал для обмена сообщениями с сервером
            // параметр - адрес сервера gRPC
            using var channel = GrpcChannel.ForAddress("https://localhost:5001", new GrpcChannelOptions
            {
                // for http/1.1: 
                HttpHandler = new GrpcWebHandler(new HttpClientHandler()),
                //HttpHandler = new SocketsHttpHandler
                //{
                //    EnableMultipleHttp2Connections = true,

                //    // ...configure other handler settings
                //},
                // for global authorization
                //Credentials = ChannelCredentials.Create(new SslCredentials(), credentials)
            });

            // создаем клиента
            var client = new Greeter.GreeterClient(channel);
            Console.Write("Введите имя: ");
            string name = Console.ReadLine();
            // обмениваемся сообщениями с сервером
            var headers = new Metadata();
            headers.Add("Authorization", $"Bearer {token}");
            var reply = await client.SayHelloAsync(new HelloRequest { Name = name }, headers, deadline: DateTime.UtcNow.AddSeconds(50));
            Console.WriteLine("Ответ сервера: " + reply.Message);

            WorkWithTypes();

            Console.ReadKey();
        }

        private static void WorkWithTypes()
        {
            // Create Timestamp and Duration from .NET DateTimeOffset and TimeSpan.
            var meeting = new Meeting
            {
                Start = Timestamp.FromDateTimeOffset(DateTimeOffset.Now), // also FromDateTime()
                Duration = Duration.FromTimeSpan(new TimeSpan(1, 0, 0))
            };

            // Convert Timestamp and Duration to .NET DateTimeOffset and TimeSpan.
            var time = meeting.Start.ToDateTimeOffset();
            var duration = meeting.Duration?.ToTimeSpan();


            var person = new Person
            {
                Name = null,
                Age = null,
                Data = ByteString.CopyFrom(new byte[0]),
                Data2 = UnsafeByteOperations.UnsafeWrap(new byte[0])
                //Data2 = ByteString.CopyFrom(new byte[0])
            };
            // Add one item.
            person.Roles.Add("user");

            // Add all items from another collection.
            var roles = new[] { "admin", "manager" };
            person.Roles.Add(roles);

            // Add one item.
            person.Attributes["created_by"] = "James";

            // Add all items from another collection.
            var attributes = new Dictionary<string, string>
            {
                ["last_modified"] = DateTime.UtcNow.ToString()
            };
            person.Attributes.Add(attributes);

            // Create a status with a Person message set to detail.
            var status = new SimpleGrpcService.Status();
            status.Detail = Any.Pack(new Person { FirstName = "James" });

            // Read Person message from detail.
            if (status.Detail.Is(Person.Descriptor))
            {
                var person2 = status.Detail.Unpack<Person>();
                // ...
            }

            var response = new ResponseMessage
            {
                Person = person
            };

            switch (response.ResultCase)
            {
                case ResponseMessage.ResultOneofCase.Person:
                    var a = response.Person;
                    break;
                case ResponseMessage.ResultOneofCase.Error:
                    var b = response.Error;
                    break;
                default:
                    throw new ArgumentException("Unexpected result.");
            }
        }
    }
}
