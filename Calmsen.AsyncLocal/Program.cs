﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.AsyncLocal
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
            Console.ReadKey();
        }
        static AsyncLocal<int> context = new AsyncLocal<int>();

        static async Task MainAsync()
        {
            context.Value = 1;
            Console.WriteLine("Should be 1: " + context.Value);
            await Async();
            Console.WriteLine("Should be 1: " + context.Value);
        }

        static async Task Async()
        {
            Console.WriteLine("Should be 1: " + context.Value);
            context.Value = 2;
            Console.WriteLine("Should be 2: " + context.Value);
            await Task.Yield();
            Console.WriteLine("Should be 2: " + context.Value);
        }
    }
}
