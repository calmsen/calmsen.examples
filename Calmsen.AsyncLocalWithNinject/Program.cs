﻿using Ninject;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.AsyncLocalWithNinject
{
    class Program
    {
        public class TestService { }

        static StandardKernel kernel = new StandardKernel();
        static void Main(string[] args)
        {
            kernel.Bind<TestService>().ToSelf().InScope(x => {
                return context.Value;
            });

            Task.WaitAll(MainAsync(1), MainAsync(2));
        }

        class IntHolder
        {
            public int Id { get; set; }
            public int Value { get; set; }
        }

        static AsyncLocal<IntHolder> context = new AsyncLocal<IntHolder>();

        static async Task MainAsync(int id)
        {
            context.Value = new IntHolder { Id = id };
            context.Value.Value = 1;
            Console.WriteLine("Should be 1: " + context.Value.Value + ", Id: " + context.Value.Id + " ThreadId:" + Thread.CurrentThread.ManagedThreadId + ", Hash:" + kernel.Get<TestService>().GetHashCode());
            await Async();
            Console.WriteLine("Should be 2: " + context.Value.Value + ", Id: " + context.Value.Id + " ThreadId:" + Thread.CurrentThread.ManagedThreadId + ", Hash:" + kernel.Get<TestService>().GetHashCode());
        }

        static async Task Async()
        {
            Console.WriteLine("Should be 1: " + context.Value.Value + ", Id: " + context.Value.Id + " ThreadId:" + Thread.CurrentThread.ManagedThreadId + ", Hash:" + kernel.Get<TestService>().GetHashCode());
            context.Value.Value = 2;
            Console.WriteLine("Should be 2: " + context.Value.Value + ", Id: " + context.Value.Id + " ThreadId:" + Thread.CurrentThread.ManagedThreadId + ", Hash:" + kernel.Get<TestService>().GetHashCode());
            await AsyncN();
            await Task.Delay(5000);
            Console.WriteLine("Should be 2: " + context.Value.Value + ", Id: " + context.Value.Id + " ThreadId:" + Thread.CurrentThread.ManagedThreadId + ", Hash:" + kernel.Get<TestService>().GetHashCode());
        }

        static async Task AsyncN()
        {
            context.Value = new IntHolder { Id = 3 };
            context.Value.Value = 4;
            Console.WriteLine("Should be 4: " + context.Value.Value + ", Id: " + context.Value.Id + " ThreadId:" + Thread.CurrentThread.ManagedThreadId + ", Hash:" + kernel.Get<TestService>().GetHashCode());
            context.Value.Value = 5;
            Console.WriteLine("Should be 5: " + context.Value.Value + ", Id: " + context.Value.Id + " ThreadId:" + Thread.CurrentThread.ManagedThreadId + ", Hash:" + kernel.Get<TestService>().GetHashCode());
            await Task.Yield();
            Console.WriteLine("Should be 5: " + context.Value.Value + ", Id: " + context.Value.Id + " ThreadId:" + Thread.CurrentThread.ManagedThreadId + ", Hash:" + kernel.Get<TestService>().GetHashCode());
        }
    }
}
