﻿using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace Calmsen.BouncyCastleCertificateGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            GenerateCaCert();
            GenerateUserCert();
            Console.WriteLine("Hello World!");
        }
        private static void GenerateCaCert()
        {

            var KeyGenerate = new RsaKeyPairGenerator();

            KeyGenerate.Init(new KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), 1024));

            AsymmetricCipherKeyPair kp = KeyGenerate.GenerateKeyPair();

            var gen = new X509V3CertificateGenerator();

            var certName = new X509Name("CN=CA");
            var serialNo = new BigInteger("1", 10);

            gen.SetSerialNumber(serialNo);
            gen.SetSubjectDN(certName);
            gen.SetIssuerDN(certName);
            gen.SetNotAfter(DateTime.Now.AddYears(100));
            gen.SetNotBefore(DateTime.Now);
            gen.SetSignatureAlgorithm("SHA1WITHRSA");
            gen.SetPublicKey(kp.Public);
            var myCert = gen.Generate(kp.Private);
            byte[] result = DotNetUtilities.ToX509Certificate(myCert).Export(X509ContentType.Cert);

            FileStream fs = new FileStream(@"temp\test1.crt", FileMode.CreateNew);
            fs.Write(result, 0, result.Length);
            fs.Flush();
            fs.Close();
        }

        private static void GenerateUserCert()
        {

            var KeyGenerate = new RsaKeyPairGenerator();

            KeyGenerate.Init(new KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), 1024));

            AsymmetricCipherKeyPair kp2 = KeyGenerate.GenerateKeyPair();

            var gen2 = new X509V3CertificateGenerator();

            var certName = new X509Name("CN=CA");
            var serialNo = new BigInteger("1", 10);

            var certName2 = new X509Name("CN=User");
            var serialNo2 = new BigInteger("2", 10);

            gen2.SetSerialNumber(serialNo2);
            gen2.SetSubjectDN(certName2);
            gen2.SetIssuerDN(certName);
            gen2.SetNotAfter(DateTime.Now.AddYears(100));
            gen2.SetNotBefore(DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)));
            gen2.SetSignatureAlgorithm("SHA1WITHRSA");
            gen2.SetPublicKey(kp2.Public);
            gen2.AddExtension(
                X509Extensions.AuthorityKeyIdentifier.Id,
                false,
                new AuthorityKeyIdentifier(
                    SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(kp2.Public),
                    new GeneralNames(new GeneralName(certName)),
                    serialNo));

            var newCert2 = gen2.Generate(kp2.Private);
            byte[] result = DotNetUtilities.ToX509Certificate(newCert2).Export(X509ContentType.Cert);
            FileStream fs = new FileStream(@"temp\test2.crt", FileMode.CreateNew);
            fs.Write(result, 0, result.Length);
            fs.Flush();
            fs.Close();
        }
    }
}
