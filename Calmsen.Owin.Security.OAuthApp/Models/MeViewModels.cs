﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Calmsen.Owin.Security.OAuthApp.Models
{
    // Models returned by MeController actions.
    public class GetViewModel
    {
        public string Hometown { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}