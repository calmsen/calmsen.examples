using System;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using NUnit.Framework;

namespace Calmsen.DiadocSdk.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var data = System.IO.File.ReadAllBytes(@"c:\temp\data.txt");
            var crypt         = new Diadoc.Api.Cryptography.WinApiCrypt();
            var cert = new X509Certificate2(@"c:\temp\newcert.cer");
            var signData = crypt.Sign(data, cert.RawData);

            System.IO.File.WriteAllBytes(@"c:\temp\data.txt.sgn", signData);
            Assert.Pass();
        }

        [Test]
        public void Test2()
        {
            var data = System.IO.File.ReadAllBytes(@"c:\temp\data.txt");
            var cert = new X509Certificate2(@"c:\temp\newcert.cer");
            var signData = Encrypt(data, cert);

            System.IO.File.WriteAllText(@"c:\temp\data.txt.sgn", signData);
            Assert.Pass();
        }

        public string Encrypt(byte[] msg, X509Certificate2 certificate)
        {
            ContentInfo contentInfo = new ContentInfo(msg);
            var signedCms = new SignedCms(contentInfo, true);
            CmsSigner cmsSigner = new CmsSigner(certificate);
            signedCms.ComputeSignature(cmsSigner);
            return Convert.ToBase64String(signedCms.Encode());
        }
    }
}