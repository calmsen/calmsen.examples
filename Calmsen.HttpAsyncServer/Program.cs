﻿using System;

namespace Calmsen.HttpAsyncServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new HttpServer();
            server.Start().Wait();
        }
    }
}
