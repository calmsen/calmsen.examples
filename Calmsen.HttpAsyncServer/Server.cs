﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Calmsen.HttpAsyncServer
{
    public class HttpServer : IDisposable
    {
        private bool disposedValue;
        private HttpListener _listener;

        public async Task Start()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add("http://localhost:8888/");
            _listener.Start();
            Console.WriteLine("Ожидание подключений...");

            while (true)
            {
                HttpListenerContext context = await _listener.GetContextAsync();
                ProcessRequest(context);
            }
        }

        protected virtual string ProccessMessage(HttpListenerContext context, string requestMessage)
        {
            return "{\"inputData\": \"" + requestMessage + "\"}";
        }

        private async void ProcessRequest(HttpListenerContext context)
        {
            try
            {
                string requestMessage = await ReadAsync(context);
                string responseMessage = ProccessMessage(context, requestMessage);
                await WriteAsync(context, responseMessage);
            }
            catch (Exception e)
            {
                string responseMessage = "{\"error\": \"" + e.Message + "\"}";
                await WriteAsync(context, responseMessage);
            }
        }

        private async Task<string> ReadAsync(HttpListenerContext context)
        {
            HttpListenerRequest request = context.Request;
            using (Stream input = request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(input, Encoding.UTF8))
                {
                    return await readStream.ReadToEndAsync();
                }
            }
        }

        private async Task WriteAsync(HttpListenerContext context, string data)
        {
            HttpListenerResponse response = context.Response;

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(data);
            response.ContentLength64 = buffer.Length;
            using (Stream output = response.OutputStream)
            {
                await output.WriteAsync(buffer, 0, buffer.Length);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    (_listener as IDisposable)?.Dispose();
                }
                _listener = null;
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
