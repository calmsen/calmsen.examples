﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;

namespace Calmsen.TablesGenerator
{
    class Program
    {
        private static string admin = "logistic_document_admin";
        private static string user = "logistic_document_user";
        private static string folderPath = @"C:\info\документы\LogisticDocument\Tables";
        private static string migrationFilePath = @"C:\projects\gitlab.hoff.ru\logistic.document.database\migrations\V5.0__DEVAX-20985_create_common_tables.sql";
        static void Main(string[] args)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                IgnoreBlankLines = false,
                Encoding = Encoding.UTF8,

            };
            var tables = new StringBuilder();
            foreach (var file in Directory.GetFiles(folderPath))
            {
                var tableName = Path.GetFileNameWithoutExtension(file);
                using (var reader = new StreamReader(file))
                using (var csv = new CsvReader(reader, config))
                {
                    csv.Context.RegisterClassMap<ColumnInfoMap>();
                    var columnInfos = csv.GetRecords<ColumnInfo>().ToArray();
                    tables.AppendLine("------------------------------------");
                    tables.AppendLine(GenerateTable(tableName, columnInfos));
                }
            }
            File.WriteAllText(migrationFilePath, tables.ToString());
        }

        private static string GenerateTable(string tableName, ColumnInfo[] columnInfos)
        {
            // в планах,имя,тип,ключ,not null,default,описание
            var tableTemplate = @"
CREATE TABLE IF NOT EXISTS __TABLE_NAME__ (
__COLUMNS__
__PRIMARY_KEY__
);

ALTER TABLE __TABLE_NAME__ OWNER TO ""__ADMINROLE__"";
GRANT ALL ON TABLE __TABLE_NAME__ TO ""__ADMINROLE__"";
GRANT INSERT, UPDATE, DELETE, SELECT, TRUNCATE ON TABLE __TABLE_NAME__ TO ""__USERROLE__"";

__COMMENTS__
			";

            var columnTemplate = "    __COLUMN_NAME__ __COLUMN_TYPE____NOT_NULL____DEFAULT__,";
            var commentTemplate = "COMMENT ON COLUMN __TABLE_NAME__.__COLUMN_NAME__ IS '__COMMENT_TEXT__';";
            var primaryKeyTemplate = "    CONSTRAINT __TABLE_NAME_WITHOUT_SCHEMA___pk PRIMARY KEY (__COLUMN_NAMES__)";

            var columns = new List<string>();
            var comments = new List<string>();
            var primaryColummns = new Dictionary<string, string>();
            foreach (var columnInfo in columnInfos)
            {
                var column = columnTemplate.Replace("__COLUMN_NAME__", columnInfo.ColumnName.Trim());
                column = column.Replace("__COLUMN_TYPE__", columnInfo.ColumnType.Trim());
                column = column.Replace("__NOT_NULL__", columnInfo.NotNull.Trim() == "+" ? " NOT NULL" : "");
                var defaultValue = columnInfo.Default.Trim();
                if (defaultValue != "" && (columnInfo.ColumnType.Trim().IndexOf("varchar") == 0 ||
                                           columnInfo.ColumnType.Trim() == "text"))
                {
                    defaultValue = $"'{defaultValue}'";
                }
                column = column.Replace("__DEFAULT__", defaultValue != "" ? $" DEFAULT {defaultValue}" : "");
                columns.Add(column);

                if (columnInfo.Comment.Trim() != "")
                {
                    var comment = commentTemplate.Replace("__COLUMN_NAME__", columnInfo.ColumnName.Trim());
                    comment = comment.Replace("__COMMENT_TEXT__", columnInfo.Comment.Trim());
                    comments.Add(comment);
                }

                var key = columnInfo.Key.Trim().ToUpper();
                if (key.IndexOf("PK") == 0)
                {
                    primaryColummns.Add(key, columnInfo.ColumnName);
                }
            }

            var primaryKey = "";
            if (primaryColummns.Count > 0)
            {
                var primaryColumns = primaryColummns.OrderBy(x => x.Key).Select(x => x.Value).ToArray();
                primaryKey = primaryKeyTemplate.Replace("__TABLE_NAME_WITHOUT_SCHEMA__", tableName.Split('.')[1]);
                primaryKey = primaryKey.Replace("__COLUMN_NAMES__", string.Join(',', primaryColumns));
                columns.Last().TrimEnd(',');
            }

            var table = tableTemplate.Replace("__COLUMNS__", string.Join(Environment.NewLine, columns));
            table = table.Replace("__COMMENTS__", string.Join(Environment.NewLine, comments));
            table = table.Replace("__PRIMARY_KEY__", primaryKey);
            table = table.Replace("__TABLE_NAME__", tableName);
            table = table.Replace("__ADMINROLE__", admin);
            table = table.Replace("__USERROLE__", user);
            return table;
        }
    }
}