﻿namespace Calmsen.TablesGenerator
{
    public class ColumnInfo
    {
        public string JiraTask { get; set; }

        public string ColumnName { get; set; }
        
        public string ColumnType { get; set; }
        
        public string Key { get; set; }
        
        public string NotNull { get; set; }
        
        public string Default { get; set; }
        
        public string Comment { get; set; }
    }
}