﻿using CsvHelper.Configuration;

namespace Calmsen.TablesGenerator
{
    /// <summary>
    /// Заголовки в планах,имя,тип,ключ,not null,default,описание
    /// </summary>
    public class ColumnInfoMap : ClassMap<ColumnInfo>
    {
        public ColumnInfoMap()
        {
            Map(m => m.JiraTask).Name("в планах");
            Map(m => m.ColumnName).Name("имя");
            Map(m => m.ColumnType).Name("тип");
            Map(m => m.Key).Name("ключ");
            Map(m => m.NotNull).Name("not null");
            Map(m => m.Default).Name("default");
            Map(m => m.Comment).Name("описание");
        }

    }
}