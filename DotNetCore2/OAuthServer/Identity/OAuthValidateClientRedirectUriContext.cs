﻿namespace OAuthServer.Identity
{
    public class OAuthValidateClientRedirectUriContext
    {
        public string ClientId { get; set; }
        public string RedirectUri { get; set; }
        public string RequestUri { get; set; }
        public bool IsValidated { get; private set; }

        public bool Validated()
        {
            return IsValidated = true;
        }

        public bool Validated(string redirectUri)
        {
            RedirectUri = redirectUri;
            return IsValidated = true;
        }
    }
}
