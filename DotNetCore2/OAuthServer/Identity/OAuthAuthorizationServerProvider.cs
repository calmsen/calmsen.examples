﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OAuthServer.Identity
{
    public class OAuthAuthorizationServerProvider
    {
        private ClientInfo[] _clientInfos;

        public OAuthAuthorizationServerProvider(ClientInfo[] clientInfos)
        {
            _clientInfos = clientInfos;
        }

        public virtual Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            var clientInfo = _clientInfos.FirstOrDefault(x => x.ClientId.Equals(context.ClientId));
            if (clientInfo != null)
            {
                if (!string.IsNullOrEmpty(context.RedirectUri))
                {
                    if (Equals(context.RedirectUri.TrimEnd('/'), clientInfo.RedirectUri.TrimEnd('/')))
                    {
                        context.Validated(context.RedirectUri);
                    }
                    else
                    {
                        if (context.RedirectUri.IndexOf(clientInfo.BaseUrl.TrimEnd('/')) == 0)
                        {
                            context.Validated(context.RedirectUri);
                        }
                    }
                }
                else
                {
                    var baseUrl = new Uri(context.RequestUri);
                    if (Equals(baseUrl.AbsoluteUri.TrimEnd('/'), clientInfo.BaseUrl.TrimEnd('/')))
                    {
                        context.Validated(baseUrl.AbsoluteUri);
                    }
                }
                context.Validated(clientInfo.BaseUrl);
            }
            return Task.FromResult<object>(null);
        }

        public virtual Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;
            if (context.TryGetFormCredentials(out clientId, out clientSecret))
            {
                var clientInfo = _clientInfos.FirstOrDefault(x => x.ClientId.Equals(context.ClientId));
                if (clientInfo != null && Equals(clientId, clientInfo.ClientId) && Equals(clientSecret, clientInfo.ClientSecret))
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }
    }
}
