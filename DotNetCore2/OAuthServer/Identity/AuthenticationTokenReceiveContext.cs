﻿
namespace OAuthServer.Identity
{
    public class AuthenticationTokenReceiveContext
    {
        public string Token { get; protected set; }
        public AuthenticationTicket Ticket { get; protected set; }

        public void DeserializeTicket(string protectedData)
        {

        }
        public void SetTicket(AuthenticationTicket ticket)
        {
        }
    }
}
