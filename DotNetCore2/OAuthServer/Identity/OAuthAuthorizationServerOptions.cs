﻿
using Microsoft.AspNetCore.Http;
using System;

namespace OAuthServer.Identity
{
    public class OAuthAuthorizationServerOptions
    {
        public PathString TokenEndpointPath { get; set; }
        public PathString AuthorizeEndpointPath { get; set; }
        public OAuthAuthorizationServerProvider Provider { get; set; }
        public AuthenticationTokenProvider AuthorizationCodeProvider { get; set; }
        public TimeSpan AccessTokenExpireTimeSpan { get; set; }
    }
}
