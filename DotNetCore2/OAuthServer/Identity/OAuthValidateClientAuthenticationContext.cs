﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;

namespace OAuthServer.Identity
{
    public class OAuthValidateClientAuthenticationContext
    {
        public string ClientId { get; set; }
        public bool IsValidated { get; private set; }

        private readonly IHttpContextAccessor _httpContextAccessor;

        public OAuthValidateClientAuthenticationContext(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public bool Validated()
        {
            return IsValidated = true;
        }

        public bool TryGetBasicCredentials(out string clientId, out string clientSecret)
        {
            throw new NotImplementedException();
        }

        public bool TryGetFormCredentials(out string clientId, out string clientSecret)
        {
            clientId = _httpContextAccessor.HttpContext.Request.Form.TryGetValue("client_id", out StringValues clientIdValue)
                ? clientIdValue.ToString()
                : null;
            clientSecret = _httpContextAccessor.HttpContext.Request.Form.TryGetValue("client_secret", out StringValues clientSecretValue)
                ? clientSecretValue.ToString()
                : null;
            return !string.IsNullOrEmpty(clientId) && !string.IsNullOrEmpty(clientSecret);
        }
    }
}
