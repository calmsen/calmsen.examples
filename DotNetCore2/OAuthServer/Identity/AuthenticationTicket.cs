﻿using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;

namespace OAuthServer.Identity
{
    public class AuthenticationTicket
    {
        public ClaimsIdentity Identity { get; }
        public AuthenticationProperties Properties { get; }

        public AuthenticationTicket(ClaimsIdentity identity, AuthenticationProperties properties)
        {
        }
    }
}
