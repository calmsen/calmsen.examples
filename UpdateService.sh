#!/bin/bash

PROJ_NAME=$1
PUB_DIR_PATH="/home/${USER}/${PROJ_NAME}"
PROJ_SERVICE_PATH_IN_PUB_DIR="${PUB_DIR_PATH}/${PROJ_NAME}.service"

SYSTEMD_USER_PATH="/home/${USER}/.config/systemd/user/"
PROJ_SERVICE_PATH="${SYSTEMD_USER_PATH}/${PROJ_NAME}.service"

BASE_DIR_PATH="/srv/${PROJ_NAME}"
CURRENT_DIR_PATH="${BASE_DIR_PATH}/current"
BACKUP_DIR_PATH="${BASE_DIR_PATH}/backups"


if [ -z $PROJ_NAME ];then
    echo "A project name must be passed. ERROR 1"; exit 1
fi

if [ ! -d $BASE_DIR_PATH ];then
    echo "Create base dir in /srv dir"
    mkdir $BASE_DIR_PATH || { echo "Failed to create base dir. ERROR 2"; exit 2; }
fi

if [ ! -d $BACKUP_DIR_PATH ];then
    echo "Create backups dir"
    mkdir $BACKUP_DIR_PATH || { echo "Failed to create backups dir. ERROR 3"; exit 3; }
fi

if [ ! -d $PUB_DIR_PATH ];then
    echo "A publish dir does not exist. ERROR 4"; exit 4
fi

if [ ! -d $SYSTEMD_USER_PATH ];then
    echo "Create ${SYSTEMD_USER_PATH} dir"
    mkdir -p $SYSTEMD_USER_PATH || { echo "Failed to create ${SYSTEMD_USER_PATH} dir. ERROR 5"; exit 5; }
fi

if [ ! -f $PROJ_SERVICE_PATH_IN_PUB_DIR ];then
    echo "A ${PROJ_NAME}.service does not exist in publish dir. ERROR 6"; exit 6
fi

if [ -d $CURRENT_DIR_PATH ];then
	echo "Archive is creating.."
	TEMP_ARCHIVE_NAME="tmp.tar.gz"
	TEMP_ARCHIVE_PATH="${BASE_DIR_PATH}/${TEMP_ARCHIVE_NAME}"
	if [ -f $TEMP_ARCHIVE_PATH ]; then
		rm $TEMP_ARCHIVE_PATH || { echo "Failed to remove old temp archive file. ERROR 7"; exit 7; }
	fi
	tar -czvf $TEMP_ARCHIVE_PATH $CURRENT_DIR_PATH || { echo "Failed to create archive file. ERROR 8"; exit 8; }
	echo "Archive created"

	echo "Calculate hash of archive"
	ARCHIVE_HASH=`md5sum ${TEMP_ARCHIVE_PATH} | awk '{ print $1 }'` || { echo "Failed to calculate hash of archive. ERROR 9"; exit 9; }
	echo "Hash of archive equals ${ARCHIVE_HASH}"

	echo "Check if there is a ${ARCHIVE_HASH} archive in the folder ${BACKUP_DIR_PATH}"
	TARGET_ARCHIVE_PATH="${BACKUP_DIR_PATH}/${ARCHIVE_HASH}.tar.gz"
	if [ -f $TARGET_ARCHIVE_PATH ]; then
		echo "Archive with name ${ARCHIVE_HASH} already exists. Removing archive.."
		rm $TEMP_ARCHIVE_PATH || { echo "Failed to remove temp archive. ERROR 10"; exit 10; }
		echo "Archive removed"
	else
		echo "Archive with name ${ARCHIVE_HASH} does not exist. Moving it to folder ${BACKUP_DIR_PATH}"
		mv $TEMP_ARCHIVE_PATH  $TARGET_ARCHIVE_PATH || { echo "Failed to move temp archive to target archive. ERROR 11"; exit 11; }
		echo "Archive moved"
	fi
fi

if [ -f $PROJ_SERVICE_PATH ]; then
	echo "Service \"${PROJ_NAME}\" is stopping..."
	systemctl --user stop ${PROJ_NAME}.service || { echo "Failed to stop ${PROJ_NAME}.service. ERROR 12"; exit 12; }
	echo "Service stopped successfully."
	rm $PROJ_SERVICE_PATH  || { echo "Failed to remove ${PROJ_SERVICE_PATH}. ERROR 13"; exit 13; }
fi

if [ -d $CURRENT_DIR_PATH ];then
    echo "The current dir is removing.."
    rm -R $CURRENT_DIR_PATH || { echo "Failed to remove current dir. ERROR 14"; exit 14; }
    echo "The current dir removed"
fi

echo "The publish dir copying into current dir.."
cp -R $PUB_DIR_PATH $CURRENT_DIR_PATH || { echo "Failed to copy publish dir into current dir. ERROR 15"; exit 15; }
#chown -R dotnet:dotnet $CURRENT_DIR_PATH
echo "The publish dir copyed into current dir"

cp $PROJ_SERVICE_PATH_IN_PUB_DIR $PROJ_SERVICE_PATH  || { echo "Failed to copy ${PROJ_SERVICE_PATH_IN_PUB_DIR} to ${PROJ_SERVICE_PATH}. ERROR 16"; exit 16; }
 systemctl --user daemon-reload
echo "${PROJ_NAME}.service starting.."
systemctl --user start ${PROJ_NAME}.service
STATUS="$(systemctl --user is-active ${PROJ_NAME}.service)"
echo "${PROJ_NAME}.service: ${STATUS}"
sleep 5
echo "Recheck after 5 seconds.."
STATUS="$(systemctl --user is-active ${PROJ_NAME}.service)"
echo "${PROJ_NAME}.service: ${STATUS}"
if [ $STATUS == "active" ]; then
    echo "${PROJ_NAME}.service running."; exit 0
else
    echo "Failed to start ${PROJ_NAME}.service. ERROR 17"; exit 17
fi
