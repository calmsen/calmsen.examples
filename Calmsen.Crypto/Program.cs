﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace Calmsen.Crypto
{
    class Program
    {
        static void Main(string[] args)
        {
            // generating public/private keys
            //
            //Debug.WriteLine("private: " + RSA.ToXmlString(true));
            //Debug.WriteLine("public: " + RSA.ToXmlString(false));

            //var publicKey = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            var publicKey =
                "-----BEGIN PUBLIC KEY-----MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBANKqK6hoG9YYyDn9Ee0pwaOF0jKyg/MkkLfC8ia+oNzID+vMc/Xu8+fS0DJ2WLF2PgLtacLA7tlxAjjc1YjTP8sCAwEAAQ==-----END PUBLIC KEY-----";

            //var privateKey = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent><P>/aULPE6jd5IkwtWXmReyMUhmI/nfwfkQSyl7tsg2PKdpcxk4mpPZUdEQhHQLvE84w2DhTyYkPHCtq/mMKE3MHw==</P><Q>3WV46X9Arg2l9cxb67KVlNVXyCqc/w+LWt/tbhLJvV2xCF/0rWKPsBJ9MC6cquaqNPxWWEav8RAVbmmGrJt51Q==</Q><DP>8TuZFgBMpBoQcGUoS2goB4st6aVq1FcG0hVgHhUI0GMAfYFNPmbDV3cY2IBt8Oj/uYJYhyhlaj5YTqmGTYbATQ==</DP><DQ>FIoVbZQgrAUYIHWVEYi/187zFd7eMct/Yi7kGBImJStMATrluDAspGkStCWe4zwDDmdam1XzfKnBUzz3AYxrAQ==</DQ><InverseQ>QPU3Tmt8nznSgYZ+5jUo9E0SfjiTu435ihANiHqqjasaUNvOHKumqzuBZ8NRtkUhS6dsOEb8A2ODvy7KswUxyA==</InverseQ><D>cgoRoAUpSVfHMdYXW9nA3dfX75dIamZnwPtFHq80ttagbIe4ToYYCcyUz5NElhiNQSESgS5uCgNWqWXt5PnPu4XmCXx6utco1UVH8HGLahzbAnSy6Cj3iUIQ7Gj+9gQ7PkC434HTtHazmxVgIR5l56ZjoQ8yGNCPZnsdYEmhJWk=</D></RSAKeyValue>";
            var privateKey =
                "-----BEGIN RSA PRIVATE KEY-----MIIBPAIBAAJBANKqK6hoG9YYyDn9Ee0pwaOF0jKyg/MkkLfC8ia+oNzID+vMc/Xu8+fS0DJ2WLF2PgLtacLA7tlxAjjc1YjTP8sCAwEAAQJBALNg6yPO4kT/fR7DeowzTgrrW/yHPQvASunuOTr8fwT+WfMybKMdC6T+DwVZZYpYm/38NL+tIBw/TizIgFoUu0ECIQDo/YkzabupB3xcHyx7vXcNVNpTIE9V2x+1RftkGE5KAwIhAOd4Mo3wzivyg8JlRi4K7KVfqhPYjJKxbxutwzJ8xayZAiAFqOMEVLUyTfQ/aKJz/w49TKphCo/izgffJ2wL6ms8cQIhAMK0wRHU9hjRrzPerOIeHmIdzptTFSxwRN4rlH7rpfxBAiEAq4BVuPi1hPhcaI7U7SyEY/UeVBr0lREen9hI9Lr+2lo=-----END RSA PRIVATE KEY-----";

            //var data = "testing";
            var data = "Заказ Номер заказа готов. Согласуйте дату самовывоза по Дата";
            // Зашифровать строку
            var base64Encrypted = CryptoUtils.ConvertToBase64EncryptedWithRsa512(publicKey, data);
            // Расшифровать строку в base64
            data = CryptoUtils.ConvertFromBase64EncryptedWithRsa512(privateKey, base64Encrypted);
            Console.WriteLine(data);
        }
    }
}