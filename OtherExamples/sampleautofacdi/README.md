# SampleAutofacDI

Примеры использования autofac в приложениb ASP.NET

# Содержание

* [Настройка контейнера](#НастройкаКонтейнера)
* [Регистрация сервисов](#РегистрацияСервисов)
* [Привязка к типу сервиса](#ПривязкаКТипуСервиса)
* [Привязка сервисов по соглашению](#ПривязкаСервисовПоСоглашению)
* [Привязка декоратора](#ПривязкаДекоратора)
* [Привязка именованного сервиса](#ПривязкаИменованногоСервиса)
* [Область действия экземпеляра](#ОбластьДействияЭкземпеляра)
* [Работа с перехватчиками](#РаботаСПерехватчиками)

# <a name="Настройка контейнера">Настройка контейнера</a>
Установить nuget пакеты Autofac и Autofac.Extensions.DependencyInjection. 
Добавить фабрику в билдер хоста:
```csharp
private static IHostBuilder CreateHostBuilder(string[] args)
{
    return Host.CreateDefaultBuilder(args)
        .UseServiceProviderFactory(new AutofacServiceProviderFactory())
        // ... 
}
```

# <a name="РегистрацияСервисов">Регистрация сервисов</a>
Регистрация сервисов производится в методе ConfigureContainer класса Startup.
```csharp
public void ConfigureContainer(ContainerBuilder builder)
{
    builder.RegisterType<ValuesRepository>().As<IValuesRepository>();
}
```
# <a name="ПривязкаКТипуСервиса">Привязка к типу сервиса</a>
Привязать сервис к собственному типу:
```csharp
builder.RegisterType<Worker>();
// или так:
builder.RegisterType<Worker>().AsSelf();
```
Привязать сервис к интерфейсу:
```csharp
builder.RegisterType<Worker>().As<IWorker>();
```
Привязать сервис к собственному типу и интерфейсу:
```csharp
builder.RegisterType<Worker>().AsSelf().As<IWorker>();
```
# <a name="ПривязкаСервисовПоСоглашению">Привязка сервисов по соглашению</a>
Привязать сервис ко всем наследуемым интерфейсам:
```csharp
builder.RegisterType<Worker>().AsImplementedInterfaces();
```
Привязать сервисы из сборки по определенному условию:
```csharp
// привязывает все типы из сборки, заканчивающие на Repository ко всем наследуемым интерфейсам.
builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
       .Where(t => t.Name.EndsWith("Repository"))
       .AsImplementedInterfaces();
```
Привязать сервисы из сборки по определенному условию к первому наследуемому интерфейсу:
```csharp
builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
       .Where(t => t.Name.EndsWith("Repository"))
       .As(x => t => t.GetInterfaces()[0]);
```
Привязать генерик сервисы из сборки по определенному условию:
```csharp
// привязывает все типы из сборки, заканчивающие на CommandHandler к генерик интерфейсу.
// то есть регистриует все обработчики команд.
builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
       .Where(t => t.Name.EndsWith("CommandHandler"))
       .AsClosedTypesOf(typeof(ICommandHandler<,>));
```

# <a name="ПривязкаДекоратора">Привязка декоратора</a>
Привязать сервис, как декоратор:
```csharp
builder.RegisterDecorator<SomeWorkerDecorator, IWorker>();
```
Привязать генерик сервис, как генерик декоратор:
```csharp
builder.RegisterGenericDecorator(typeof(LoggingDecoratorCommandHandler<,>), typeof(ICommandHandler<,>));
```

# <a name="ПривязкаИменованногоСервиса">Привязка именованного сервиса</a>
Регистрация именованного сервиса к интерфейсному типу
```csharp
builder.RegisterType<IdlConnectionStringProvider>().Named<IConnectionStringProvider>("Idl");
```
Зависимость должна быть предварена атрибутом [KeyFilter("Idl")]
```csharp
public class ValuesRepository : IValuesRepository
{
    public ValuesRepository([KeyFilter("Idl")] IConnectionStringProvider connectionStringProvider)
    {
        // ...
    }
}
```
# <a name="ОбластьДействияЭкземпеляра">Область действия экземпеляра</a>
Регистрация сервиса на каждую зависимость (Instance Per Dependency). В других контейнерах называется transient instance или factory instance.
Такой тип сервисов указывается по умолчанию или с указанием InstancePerDependency.
```csharp
builder.RegisterType<Worker>();
// или так:
builder.RegisterType<Worker>().InstancePerDependency();
```
Регистрация сервиса как singleton:
```csharp
builder.RegisterType<Worker>().SingleInstance();
```
Регистрация сервиса к жизни вложенной области (Instance Per Lifetime Scope). В других контейнерах называется request instance.
```csharp
builder.RegisterType<Worker>().InstancePerLifetimeScope();
```
См. более подробно об областях действия экземпеляра на [странице](https://autofac.readthedocs.io/en/latest/lifetime/instance-scope.html)


# <a name="РаботаСПерехватчиками">Работа с перехватчиками</a>
Перехватчик должен реализовать интерфейс IInterceptor. Н-р:
```csharp
public class PermissionsInterceptor : IInterceptor
{
    private readonly AccessTokenProvider accessTokenProvider;

    public PermissionsInterceptor(AccessTokenProvider accessTokenProvider)
    {
        this.accessTokenProvider = accessTokenProvider;
    }

    public void Intercept(IInvocation invocation)
    {
        // проверяем сначала доступ
        if (this.accessTokenProvider.AccessToken != "123")
        {
            throw new InvalidOperationException("Доступ запрещен.");
        }
        
        // выполняем метод если доступ разрешен
        invocation.Proceed();
    }
}
```
Регистрация перехватчика:
```csharp
builder.RegisterType<PermissionsInterceptor>().AsSelf();
```
Перехватываемый сервис должен быть зарегисрирован так:
```csharp
builder.RegisterType<ValuesRepository>().As<IValuesRepository>()
                .WithAttributeFiltering()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(PermissionsInterceptor));
```
После этого каждый метод интерфейса IValuesRepository будет перехвачен и в дальнейшем логика вызова будет уже контролироваться классом PermissionsInterceptor.  
\
\
Регистрация именованного перехватчика:
```csharp
builder.RegisterType<PermissionsInterceptor>().Named<IInterceptor>("permissions");
```
Перехватываемый сервис именнованным перехватчиком должен быть зарегисрирован так:
```csharp
builder.RegisterType<ValuesService>().As<IValuesService>()
                .WithAttributeFiltering()
                .EnableInterfaceInterceptors();
```
Перехватываемый сервис должен быть предварен атрибутом с указанием именованного перехватчика:
```csharp
[Intercept("permissions")]
public class ValuesService : IValuesService
{
}
```

Перехватчик перехватывает каждый метод сервиса. Анализ методов можно делать посредством данных метода. Н-р:
```csharp
// Класс перехватчика
public class PermissionsInterceptor : IInterceptor
{
    public void Intercept(IInvocation invocation)
    {
        // Делаем проверку доступа только тогда, когда метод предварен атрибутом Permission
        if (invocation.MethodInvocationTarget.GetAttribute<PermissionAttribute>() == null)
        {
            invocation.Proceed();
            return;
        }

        // проверяем доступ
        if (this.accessTokenProvider.AccessToken != "123")
        {
            throw new InvalidOperationException("Доступ запрещен.");
        }

        // выполняем метод если доступ разрешен
        invocation.Proceed();
    }
}

// Класс атрибута
public class PermissionAttribute : Attribute
{
}

// Класс перехватываемого сервиса
[Intercept("permissions")]
public class ValuesService : IValuesService
{
    [Permission]
    public void SetValues(string[] values)
    {
        // ...
    }
}
```