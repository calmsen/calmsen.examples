// <copyright file="IdlConnectionStringProvider.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using Microsoft.Extensions.Configuration;

namespace SampleAutofacDI.SeedWork
{
    public class IdlConnectionStringProvider : DefaultConnectionStringProvider
    {
        public IdlConnectionStringProvider(IConfiguration config) : base("Idl", config)
        {
        }
    }
}
