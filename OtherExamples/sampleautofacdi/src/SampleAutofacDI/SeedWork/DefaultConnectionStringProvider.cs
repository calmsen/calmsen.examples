// <copyright file="DefaultConnectionStringProvider.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using Microsoft.Extensions.Configuration;

namespace SampleAutofacDI.SeedWork
{
    public class DefaultConnectionStringProvider : IConnectionStringProvider
    {
        public const string DefaultConnectionName = "DefaultConnection";

        public DefaultConnectionStringProvider(string value)
        {
            this.Value = value;
        }

        public DefaultConnectionStringProvider(string name, IConfiguration config)
        {
            this.Value = config.GetConnectionString(name);
        }

        public DefaultConnectionStringProvider(IConfiguration config) : this(DefaultConnectionName, config)
        {
        }

        public string Value { get; }
    }
}
