// <copyright file="IConnectionStringProvider.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

namespace SampleAutofacDI.SeedWork
{
    public interface IConnectionStringProvider
    {
        string Value { get; }
    }
}
