// <copyright file="ValuesController.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using Microsoft.AspNetCore.Mvc;

using SampleAutofacDI.AutofacTests;

namespace SampleAutofacDI.Controllers
{
    [ApiController]
    public class ValuesController
    {
        private readonly AccessTokenProvider accessTokenProvider;

        public ValuesController(AccessTokenProvider accessTokenProvider)
        {
            this.accessTokenProvider = accessTokenProvider;
        }

        [HttpGet("api/v1/values")]
        public string[] GetValues([FromServices] IValuesService valuesService)
        {
            return valuesService.GetValues();
        }

        [HttpPost("api/v1/values")]
        public void SetValues([FromServices] IValuesService valuesService, string[] values, string accessToken = null)
        {
            this.accessTokenProvider.AccessToken = accessToken;
            valuesService.SetValues(values);
        }
    }
}
