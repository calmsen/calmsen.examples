// <copyright file="PermissionsInterceptor.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System;

using Castle.Core.Internal;
using Castle.DynamicProxy;

namespace SampleAutofacDI.AutofacTests
{
    public class PermissionsInterceptor : IInterceptor
    {
        private readonly AccessTokenProvider accessTokenProvider;

        public PermissionsInterceptor(AccessTokenProvider accessTokenProvider)
        {
            this.accessTokenProvider = accessTokenProvider;
        }

        public void Intercept(IInvocation invocation)
        {
            // Делаем проверку доступа только тогда, когда метод предварен атрибутом Permission
            if (invocation.MethodInvocationTarget.GetAttribute<PermissionAttribute>() == null)
            {
                invocation.Proceed();
                return;
            }

            // проверяем доступ
            if (this.accessTokenProvider.AccessToken != "123")
            {
                throw new InvalidOperationException("Доступ запрещен.");
            }

            // выполняем метод если доступ разрешен
            invocation.Proceed();
        }
    }
}
