// <copyright file="ValuesRepository.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System;

using Autofac.Features.AttributeFilters;

using SampleAutofacDI.SeedWork;

namespace SampleAutofacDI.AutofacTests
{
    public class ValuesRepository : IValuesRepository
    {
        private static string[] valuesStorage = new[] { "value1" };
        private readonly IConnectionStringProvider connectionStringProvider;

        public ValuesRepository([KeyFilter("Idl")] IConnectionStringProvider connectionStringProvider)
        {
            this.connectionStringProvider = connectionStringProvider;
        }

        public string[] GetValues()
        {
            Console.WriteLine($"ConnectionString: {this.connectionStringProvider.Value}");
            return valuesStorage;
        }

        [Permission]
        public void SetValues(string[] values)
        {
            valuesStorage = values;
        }
    }
}
