// <copyright file="IValuesRepository.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

namespace SampleAutofacDI.AutofacTests
{
    public interface IValuesRepository
    {
        string[] GetValues();

        void SetValues(string[] values);
    }
}
