// <copyright file="IValuesService.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

namespace SampleAutofacDI.AutofacTests
{
    public interface IValuesService
    {
        string[] GetValues();

        void SetValues(string[] values);
    }
}
