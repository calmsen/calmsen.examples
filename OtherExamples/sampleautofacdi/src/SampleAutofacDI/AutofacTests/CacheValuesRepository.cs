// <copyright file="CacheValuesRepository.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System;

using Autofac.Features.Decorators;

using Microsoft.Extensions.Caching.Distributed;

using SampleAutofacDI.SeedWork;

namespace SampleAutofacDI.AutofacTests
{
    public class CacheValuesRepository : IValuesRepository
    {
        private readonly IValuesRepository decorated;
        private readonly IDecoratorContext context;
        private readonly IDistributedCache cache;

        public CacheValuesRepository(IValuesRepository decorated, IDecoratorContext context, IDistributedCache cache)
        {
            this.decorated = decorated ?? throw new ArgumentNullException(nameof(decorated));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.cache = cache;
        }

        public string[] GetValues()
        {
            Console.WriteLine(this.context);
            return this.cache.GetOrCreate("values", () => this.decorated.GetValues());
        }

        public void SetValues(string[] values)
        {
            this.decorated.SetValues(values);
        }
    }
}
