// <copyright file="CreatePersonCommandHandler.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System.Threading;
using System.Threading.Tasks;

namespace SampleAutofacDI.AutofacTests.Handlers
{
    public class CreatePersonCommandHandler : ICommandHandler<CreatePersonCommand, int>
    {
        public Task<int> Handle(CreatePersonCommand command, CancellationToken cancellationToken)
        {
            return Task.FromResult(123);
        }
    }
}
