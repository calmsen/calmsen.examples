// <copyright file="ExceptionDecoratorCommandHandler.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

namespace SampleAutofacDI.AutofacTests.Handlers
{
    public class ExceptionDecoratorCommandHandler<TCommand, TResponse> : ICommandHandler<TCommand, TResponse> where TCommand : ICommand<TResponse>
    {
        private readonly ICommandHandler<TCommand, TResponse> decorated;
        private readonly ILogger<ExceptionDecoratorCommandHandler<TCommand, TResponse>> logger;

        public ExceptionDecoratorCommandHandler(
            ICommandHandler<TCommand, TResponse> decorated,
            ILogger<ExceptionDecoratorCommandHandler<TCommand, TResponse>> logger)
        {
            this.decorated = decorated;
            this.logger = logger;
        }

        public async Task<TResponse> Handle(TCommand command, CancellationToken cancellationToken)
        {
            try
            {
                return await this.decorated.Handle(command, cancellationToken);
            }
            catch (Exception e)
            {
                this.logger.LogError($"Failed {typeof(TCommand).Name}.", e);
                throw;
            }
        }
    }
}
