// <copyright file="UpdatePersonCommandHandler.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System.Threading;
using System.Threading.Tasks;

namespace SampleAutofacDI.AutofacTests.Handlers
{
    public class UpdatePersonCommandHandler : ICommandHandler<UpdatePersonCommand, int>
    {
        public Task<int> Handle(UpdatePersonCommand command, CancellationToken cancellationToken)
        {
            return Task.FromResult(123);
        }
    }
}
