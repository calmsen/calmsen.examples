// <copyright file="LoggingDecoratorCommandHandler.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

namespace SampleAutofacDI.AutofacTests.Handlers
{
    public class LoggingDecoratorCommandHandler<TCommand, TResponse> : ICommandHandler<TCommand, TResponse> where TCommand : ICommand<TResponse>, ICommandLogging
    {
        private readonly ICommandHandler<TCommand, TResponse> decorated;
        private readonly ILogger<LoggingDecoratorCommandHandler<TCommand, TResponse>> logger;

        public LoggingDecoratorCommandHandler(
            ICommandHandler<TCommand, TResponse> decorated,
            ILogger<LoggingDecoratorCommandHandler<TCommand, TResponse>> logger)
        {
            this.decorated = decorated;
            this.logger = logger;
        }

        public async Task<TResponse> Handle(TCommand command, CancellationToken cancellationToken)
        {
            this.logger.LogInformation($"{typeof(TCommand).Name} executing.");
            var result = await this.decorated.Handle(command, cancellationToken);
            this.logger.LogInformation($"{typeof(TCommand).Name} executed.");
            return result;
        }
    }
}
