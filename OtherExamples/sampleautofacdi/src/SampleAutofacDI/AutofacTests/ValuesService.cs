// <copyright file="ValuesService.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using Autofac.Extras.DynamicProxy;

namespace SampleAutofacDI.AutofacTests
{
    [Intercept("permissions")]
    public class ValuesService : IValuesService
    {
        private readonly IValuesRepository valuesRepository;

        public ValuesService(IValuesRepository valuesRepository)
        {
            this.valuesRepository = valuesRepository;
        }

        public string[] GetValues()
        {
            return this.valuesRepository.GetValues();
        }

        [Permission]
        public void SetValues(string[] values)
        {
            this.valuesRepository.SetValues(values);
        }
    }
}
