// <copyright file="LocalizedValuesRepository.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System;

using Autofac.Features.Decorators;

namespace SampleAutofacDI.AutofacTests
{
    public class LocalizedValuesRepository : IValuesRepository
    {
        private readonly IValuesRepository decorated;
        private readonly IDecoratorContext context;

        public LocalizedValuesRepository(IValuesRepository decorated, IDecoratorContext context)
        {
            this.decorated = decorated ?? throw new ArgumentNullException(nameof(decorated));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public string[] GetValues()
        {
            Console.WriteLine(this.context);
            var values = this.decorated.GetValues();
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] == "value1")
                {
                    values[i] = "значение1";
                }
            }

            return values;
        }

        public void SetValues(string[] values)
        {
            this.decorated.SetValues(values);
        }
    }
}
