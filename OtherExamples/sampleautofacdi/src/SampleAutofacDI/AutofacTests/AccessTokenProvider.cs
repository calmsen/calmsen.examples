// <copyright file="AccessTokenProvider.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

namespace SampleAutofacDI.AutofacTests
{
    public class AccessTokenProvider
    {
        public string AccessToken { get; set; }
    }
}
