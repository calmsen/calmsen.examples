// <copyright file="Startup.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System.Reflection;

using Autofac;
using Autofac.Extras.DynamicProxy;
using Autofac.Features.AttributeFilters;

using Castle.DynamicProxy;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using SampleAutofacDI.AutofacTests;
using SampleAutofacDI.AutofacTests.Handlers;
using SampleAutofacDI.SeedWork;

namespace SampleAutofacDI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDistributedMemoryCache();

            services.AddSingleton<IdlConnectionStringProvider>();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            // регистрация сервиса к собственному типу на область действия запроса.
            builder.RegisterType<AccessTokenProvider>().AsSelf().InstancePerLifetimeScope();

            // регистрация именованного сервиса к интерфейсному типу.
            // зависимость должна быть предварена атрибутом [KeyFilter("Idl")]
            builder.RegisterType<IdlConnectionStringProvider>().Named<IConnectionStringProvider>("Idl");

            // регистрация сервиса с указанным перехватчиком.
            builder.RegisterType<PermissionsInterceptor>().AsSelf();
            builder.RegisterType<ValuesRepository>().As<IValuesRepository>()
                .WithAttributeFiltering()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(PermissionsInterceptor));

            // builder.RegisterType<ValuesRepository>().As<IValuesRepository>();
            // builder.RegisterType<ValuesRepository>().As<IValuesRepository>().WithParameter(ResolvedParameter.ForNamed<IConnectionStringProvider>("Idl"));

            // регистрация сервиса с именованным перехватчиком.
            // класс сервиса должен быть предварен атрибутом [Intercept("permissions")].
            builder.RegisterType<PermissionsInterceptor>().Named<IInterceptor>("permissions");
            builder.RegisterType<ValuesService>().As<IValuesService>()
                .WithAttributeFiltering()
                .EnableInterfaceInterceptors();

            // регистрация декораторов
            builder.RegisterDecorator<LocalizedValuesRepository, IValuesRepository>();
            builder.RegisterDecorator<CacheValuesRepository, IValuesRepository>();

            // регистрация генерик декораторов
            builder.RegisterGenericDecorator(typeof(LoggingDecoratorCommandHandler<,>), typeof(ICommandHandler<,>));
            builder.RegisterGenericDecorator(typeof(ExceptionDecoratorCommandHandler<,>), typeof(ICommandHandler<,>));

            // регистрация генерик сервисов из указанной сборки.
            // var types = Assembly.GetExecutingAssembly().GetTypes().Where(t => !t.IsGenericType && t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(ICommandHandler<,>))).ToArray();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())

                // .Where(t => !t.IsGenericType && t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(ICommandHandler<,>)))
                // .AsImplementedInterfaces()
                .AsClosedTypesOf(typeof(ICommandHandler<,>));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
