﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Pkcs;

namespace Signer
{
    class Program
    {
        static void Main(string[] args)
        {
            var certPath = GetArgValue(args, "c");
            if (certPath == null)
            {
                Console.WriteLine("Error: Specify path to pfx certificate.");
                Exit(ErrorType.CertPathNotSpecified);
            }
            var filePath = GetArgValue(args, "f");
            if (filePath == null)
            {
                Console.WriteLine("Error: Specify path to file.");
                Exit(ErrorType.FilePathNotSpecified);
            }
            var sgnPath = GetArgValue(args, "s");
            if (sgnPath == null)
            {
                Console.WriteLine("Error: Specify path to sgn file.");
                Exit(ErrorType.SignaturePathNotSpecified);
            }
            var detached = GetArgValue(args, "d");
            if (detached == null)
            {
                Console.WriteLine("Error: Specify type of signature - detached or attached?");
                Exit(ErrorType.DetachedFlagNotSpecified);
            }
            var pfxPassword = GetArgValue(args, "p");
            if (pfxPassword == null)
            {
                Console.WriteLine("Error: Specify password for pfx certificate.");
                Exit(ErrorType.PfxPasswordNotSpecified);
            }

            if (!File.Exists(certPath))
            {
                Console.WriteLine($"Error: file does not exists at the specified path {certPath}.");
                Exit(ErrorType.CertFileNotExists);
            }
            if (!File.Exists(filePath))
            {
                Console.WriteLine($"Error: file does not exists at the specified path {filePath}.");
                Exit(ErrorType.DataFileNotExists);
            }
            if (File.Exists(sgnPath))
            {
                Console.WriteLine($"Error: file already exists at the specified path {sgnPath}.");
                Exit(ErrorType.SignatureFileAlreadyExists);
            }


            var certPfx = File.ReadAllBytes(certPath);
            var data = File.ReadAllBytes(filePath);
            using var gostCert = new X509Certificate2(certPfx, pfxPassword, X509KeyStorageFlags.CspNoPersistKeySet);
            var signed = Sign(data, gostCert, string.Equals(detached, "true"));
            File.WriteAllBytes(sgnPath, signed);
        }

        private static void Exit(ErrorType errorType)
        {
            Environment.Exit((int)errorType);
        }

        private static string GetArgValue(IReadOnlyList<string> args, string argName, string defaultValue = null)
        {
            if (args == null)
                return defaultValue;

            try
            {

                for (var i = 0; i < args.Count; i++)
                    if (args[i].Equals($"-{argName}"))
                    {
                        return args[i + 1];
                    }
            }
            catch { }
            return defaultValue;
        }
        private static byte[] Sign(byte[] data, X509Certificate2 certificate, bool detached)
        {
            var contentInfo = new ContentInfo(data);
            var signedCms = new SignedCms(contentInfo, detached);
            var cmsSigner = new CmsSigner(certificate);
            signedCms.ComputeSignature(cmsSigner);
            return signedCms.Encode();
        }

        private enum ErrorType
        {
            CertPathNotSpecified = 1,

            FilePathNotSpecified = 2,

            SignaturePathNotSpecified = 3,

            DetachedFlagNotSpecified = 4,

            PfxPasswordNotSpecified = 5,

            CertFileNotExists = 6,

            DataFileNotExists = 7,

            SignatureFileAlreadyExists = 8,
        }

    }
}