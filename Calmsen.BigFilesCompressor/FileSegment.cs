﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calmsen.BigFilesCompression
{
    public class FileSegment: IFileSegment
    {
        public FileSegment(int segmentNumber, long segmentOffset, byte[] segmentBytes)
        {
            SegmentNumber = segmentNumber;
            SegmentOffset = segmentOffset;
            SegmentBytes = segmentBytes;
        }

        public int SegmentNumber { get; }
        public long SegmentOffset { get; set; }
        public byte[] SegmentBytes { get; }
    }
}
