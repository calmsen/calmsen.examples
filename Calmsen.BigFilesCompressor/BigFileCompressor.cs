﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.BigFilesCompression
{
    public class BigFileCompressor
    {
        private const int FileSegmentSize = 1024 * 1024 * 100;
        private readonly string _sourceFilePath;
        private readonly string _destinationFilePath;
        private readonly int _wokerCount;
        private readonly CancellationToken _cancellationToken;

        public BigFileCompressor(string sourceFilePath, string destinationFilePath, int wokerCount, CancellationToken cancellationToken)
        {
            _sourceFilePath = sourceFilePath;
            _destinationFilePath = destinationFilePath;
            _wokerCount = wokerCount;
            _cancellationToken = cancellationToken;
        }

        public async Task CompressAsync()
        {
            var sourceFileSegments = new BlockingCollection<IFileSegment>(_wokerCount);
            
            var fileReader = new BigFileReader(_sourceFilePath, sourceFileSegments, FileSegmentSize, 1, _cancellationToken);
            var readerTask = fileReader.ReadFileAsync();

            var sourceFileInfo = new FileInfo(_sourceFilePath);
            using (var destinationFile = File.Create(_destinationFilePath))
            {
                var sourceFileSize = sourceFileInfo.Length;
                destinationFile.SetLength(sourceFileSize);
            }
            int segmentCount = (int)Math.Ceiling(sourceFileInfo.Length / (double)FileSegmentSize);

            var compressedFileSegments = new BlockingCollection<IFileSegment>(segmentCount);
            var compressedFileSegmentsQueueFiller = new FileSegmentsQueueFiller(compressedFileSegments, segmentCount);
            var workerTasks = new Task[_wokerCount];
            for (int i = 0; i < _wokerCount; i++)
            {
                int worker = i;
                workerTasks[i] = Task.Run(() => CompressSegmentWithWorkerAsync(sourceFileSegments, compressedFileSegmentsQueueFiller, worker));
            }

            var fileWriter = new BigFileWriter(_destinationFilePath, compressedFileSegments, 2, _cancellationToken);
            var writerTask = fileWriter.WriteFileAsync();

            await readerTask;
            await Task.WhenAll(workerTasks);
            await writerTask;
            if (compressedFileSegmentsQueueFiller.HeadFileSegmentOffset < sourceFileInfo.Length)
                using (var destinationFile = new FileStream(_destinationFilePath, FileMode.Open, FileAccess.ReadWrite))
                {
                    destinationFile.SetLength(compressedFileSegmentsQueueFiller.HeadFileSegmentOffset);
                }
        }

        private async Task CompressSegmentWithWorkerAsync(
            BlockingCollection<IFileSegment> sourceFileSegments,
            FileSegmentsQueueFiller compressedFileSegmentsQueueFiller, 
            int workerNumber
        )
        {
            IFileSegment fileSegment;
            while (!sourceFileSegments.IsCompleted)
            {
                if (sourceFileSegments.TryTake(out fileSegment))
                {
                    var compressedFileSegment = await CompressSegmentAsync(fileSegment, workerNumber);
                    await compressedFileSegmentsQueueFiller.AddFileSegment(compressedFileSegment);
                }
                    
            }
        }

        private async Task<IFileSegment> CompressSegmentAsync(IFileSegment fileSegment, int workerNumber)
        {
            var timer = new Stopwatch();
            timer.Start();

            Console.WriteLine($"The segment number {fileSegment.SegmentNumber} compressing  with worker {workerNumber}. ThreadId: {Thread.CurrentThread.ManagedThreadId}");
            using (var outputStream = new MemoryStream())
            {
                using (var gzipStream = new GZipStream(outputStream, CompressionMode.Compress))
                //using (var inputStream = new MemoryStream(fileSegment.SegmentBytes))
                //    await inputStream.CopyToAsync(gzipStream, _cancellationToken);
                await gzipStream.WriteAsync(
                            fileSegment.SegmentBytes,
                            0,
                            fileSegment.SegmentBytes.Length,
                            _cancellationToken
                        );
                byte[] compressedBytes = outputStream.ToArray();
                
                timer.Stop();
                TimeSpan timeTaken = timer.Elapsed;
                
                Console.WriteLine($"The segment number {fileSegment.SegmentNumber} was compressed. {compressedBytes.Length} bytes were compressed with worker {workerNumber}. ThreadId: {Thread.CurrentThread.ManagedThreadId}. Time taken: {timeTaken.ToString(@"m\:ss\.fff")}");
                return new FileSegment(fileSegment.SegmentNumber, 0, compressedBytes);
            }
        }
    }
}
