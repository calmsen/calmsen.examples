﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.BigFilesCompression
{
    public class FileSegmentsQueueFiller
    {
        public long HeadFileSegmentOffset => _headFileSegmentOffset;

        private readonly BlockingCollection<IFileSegment> _fileSegments;
        private readonly long _segmentCount;
        private SemaphoreSlim _headFileSegmentSemaphore = new SemaphoreSlim(1);
        private long _headFileSegment = 0;
        private long _headFileSegmentOffset = 0;
        private List<IFileSegment> _fileSegmentsToQueue = new List<IFileSegment>();

        public FileSegmentsQueueFiller(BlockingCollection<IFileSegment> fileSegments, long segmentCount)
        {
            _fileSegments = fileSegments;
            _segmentCount = segmentCount;
        }

        public async Task AddFileSegment(IFileSegment fileSegment)
        {
            if (_segmentCount == _headFileSegment)
                return;
            
            await _headFileSegmentSemaphore.WaitAsync();
            
            _fileSegmentsToQueue.Add(fileSegment);
            _fileSegmentsToQueue = _fileSegmentsToQueue.OrderBy(x => x.SegmentNumber).ToList();
            
            for (int i = 0; i < _fileSegmentsToQueue.Count; i++)
            {
                fileSegment = _fileSegmentsToQueue[i];
                if (fileSegment.SegmentNumber == _headFileSegment)
                {
                    fileSegment.SegmentOffset = _headFileSegmentOffset;
                    _headFileSegment++;
                    _headFileSegmentOffset += fileSegment.SegmentBytes.Length;
                    _fileSegments.Add(fileSegment);
                    Console.WriteLine($"The segment number {fileSegment.SegmentNumber} was added to queue.");
                    _fileSegmentsToQueue.RemoveAt(i);
                    i--;
                }
            }
            if (_segmentCount == _headFileSegment)
                _fileSegments.CompleteAdding();

            _headFileSegmentSemaphore.Release();
        }
    }
}
