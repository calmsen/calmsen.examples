﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.BigFilesCompression
{
    public class BigFileChunkCompressor
    {
        private const int SegmentSize = 1024 * 1024 * 100;
        private readonly int _wokerCount;

        public BigFileChunkCompressor(int wokerCount)
        {
            _wokerCount = wokerCount;
        }

        public void CompressFile(string inFile)
        {
            var sourceFileInfo = new FileInfo(inFile);
            long segmentCount = (long)Math.Ceiling(sourceFileInfo.Length / (double)SegmentSize);
            var queue = new ConcurrentQueue<int>();
            for (int i = 0; i < segmentCount; i++)
            {
                queue.Enqueue(i);
            }
            string destinationFileName = @$"Temp\{Guid.NewGuid()}_dst{Path.GetExtension(inFile)}.gz";
            var waitHandles = new WaitHandle[_wokerCount];
            for (int i = 0; i < _wokerCount; i++)
            {
                int worker = i;
                var handle = new EventWaitHandle(false, EventResetMode.ManualReset);
                
                var thread = new Thread(() =>
                {
                    CompressChunk(inFile, destinationFileName, queue);
                    handle.Set();
                });
                waitHandles[worker] = handle;
                thread.Start();
            }
            WaitHandle.WaitAll(waitHandles);

            File.Create(destinationFileName).Dispose();
            using (var outputSttream = new FileStream(destinationFileName, FileMode.Open, FileAccess.Write))
            {
                long segmentOffset = 0;
                for (int i = 0; i < segmentCount; i++)
                {
                    outputSttream.Seek(segmentOffset, SeekOrigin.Begin);
                    string chunkFileName = destinationFileName.Replace("_dst", "_" + i);
                    using (var inputSttream = new FileStream(chunkFileName, FileMode.Open, FileAccess.Read))
                    {
                        inputSttream.CopyTo(outputSttream);
                    }
                    var fileInfo = new FileInfo(chunkFileName);
                    segmentOffset += fileInfo.Length;
                    File.Delete(chunkFileName);
                }
            }

        }

        private void CompressChunk(string inFile, string destinationFileName, ConcurrentQueue<int> queue)
        {
            int segmentNumber;
            var segmentBytes = new byte[SegmentSize];
            while (!queue.IsEmpty)
            {
                if (queue.TryDequeue(out segmentNumber))
                {
                    using (FileStream sourceFile = File.Open(inFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (FileStream destinationFile = File.Create(destinationFileName.Replace("_dst", "_" + segmentNumber)))
                    using (GZipStream output = new GZipStream(destinationFile, CompressionMode.Compress))
                    {
                        long segmentOffset = segmentNumber * (long)SegmentSize;
                        sourceFile.Seek(segmentOffset, SeekOrigin.Begin);
                        int readBytesCount = sourceFile.Read(
                            segmentBytes,
                            0,
                            SegmentSize
                        );

                        output.Write(
                            readBytesCount != SegmentSize
                                ? segmentBytes.Take(readBytesCount).ToArray()
                                : segmentBytes,
                            0,
                            readBytesCount
                        );
                    }
                }
            }

            
        }
    }
}
