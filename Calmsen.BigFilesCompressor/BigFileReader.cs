﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.BigFilesCompression
{
    public class BigFileReader
    {
        private readonly string _filePath;
        private readonly BlockingCollection<IFileSegment> _fileSegments;
        private readonly int _segmentSize;
        private readonly int _wokerCount;
        private readonly CancellationToken _cancellationToken;

        public BigFileReader(string filePath, BlockingCollection<IFileSegment> fileSegments, int segmentSize, int wokerCount, CancellationToken cancellationToken)
        {
            _filePath = filePath;
            _fileSegments = fileSegments;
            _segmentSize = segmentSize;
            _wokerCount = wokerCount;
            _cancellationToken = cancellationToken;
        }

        public async Task ReadFileAsync()
        {
            var fileInfo = new FileInfo(_filePath);
            if (!fileInfo.Exists)
                throw new InvalidOperationException($"File {_filePath} does not exist.");
            
            if (fileInfo.Length == 0)
                throw new InvalidOperationException($"File {_filePath} is empty.");

            long segmentCount = (long)Math.Ceiling(fileInfo.Length / (double)_segmentSize);

            var queue = new ConcurrentQueue<int>();            
            for (int i = 0; i < segmentCount; i++)
            {
                queue.Enqueue(i);                               
            }
            var workerTasks = new Task[_wokerCount];
            for (int i = 0; i < _wokerCount; i++)
            {
                int worker = i;
                workerTasks[i] = Task.Run(() => ReadFileSegmentWithWorker(worker, queue));
            }
            await Task.WhenAll(workerTasks);
            _fileSegments.CompleteAdding();
        }

        private async Task ReadFileSegmentWithWorker(int workerNumber, ConcurrentQueue<int> queue)
        {
            int segmentNumber;
            while (!queue.IsEmpty)
            {
                if (queue.TryDequeue(out segmentNumber))
                {
                    var segmentBytes = await ReadFileSegmentAsync(segmentNumber, workerNumber);
                    _fileSegments.Add(segmentBytes);
                }                    
            }
        }

        private async Task<FileSegment> ReadFileSegmentAsync(int segmentNumber, int workerNumber)
        {
            var timer = new Stopwatch();
            timer.Start();

            Console.WriteLine($"The segment number {segmentNumber} reading with worker {workerNumber}. ThreadId: {Thread.CurrentThread.ManagedThreadId}");
            var segmentBytes = new byte[_segmentSize];
            using (var fileSttream = new FileStream(_filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                long segmentOffset = segmentNumber * (long)_segmentSize;
                fileSttream.Seek(segmentOffset, SeekOrigin.Begin);
                int readBytesCount = await fileSttream.ReadAsync(
                    segmentBytes, 
                    0,
                    _segmentSize, 
                    _cancellationToken
                );
                timer.Stop();
                TimeSpan timeTaken = timer.Elapsed;

                Console.WriteLine($"The segment number {segmentNumber} was read with worker {workerNumber}. {readBytesCount} bytes were read. ThreadId: {Thread.CurrentThread.ManagedThreadId}. Time taken: {timeTaken.ToString(@"m\:ss\.fff")}");
                return new FileSegment(
                    segmentNumber,
                    segmentOffset,
                    readBytesCount != _segmentSize
                        ? segmentBytes.Take(readBytesCount).ToArray()
                        : segmentBytes
                );
            };
        }
    }
}
