﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calmsen.BigFilesCompression
{
    public interface IFileSegment
    {
        int SegmentNumber { get; }
        long SegmentOffset { get; set; }
        byte[] SegmentBytes { get; }
    }
}
