﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.BigFilesCompression
{
    public class BigFileWriter
    {
        private readonly string _filePath;
        private readonly BlockingCollection<IFileSegment> _fileSegments;
        private readonly int _wokerCount;
        private readonly CancellationToken _cancellationToken;

        public BigFileWriter(string filePath, BlockingCollection<IFileSegment> fileSegments, int wokerCount, CancellationToken cancellationToken)
        {
            _filePath = filePath;
            _fileSegments = fileSegments;
            _wokerCount = wokerCount;
            _cancellationToken = cancellationToken;
        }

        public async Task WriteFileAsync()
        {
            var workerTasks = new Task[_wokerCount];
            for (int i = 0; i < _wokerCount; i++)
            {
                int worker = i;
                workerTasks[i] = Task.Run(() => WriteFileSegmentWithWorkerAsync(worker));
            }
            await Task.WhenAll(workerTasks);
        }

        private async Task WriteFileSegmentWithWorkerAsync(int workerNumber)
        {
            IFileSegment fileSegment;
            while (!_fileSegments.IsCompleted)
            {
                if (_fileSegments.TryTake(out fileSegment))
                    await WriteFileSegmentAsync(fileSegment, workerNumber);
            }
        }

        private async Task WriteFileSegmentAsync(IFileSegment fileSegment, int workerNumber)
        {
            var timer = new Stopwatch();
            timer.Start();

            Console.WriteLine($"The segment number {fileSegment.SegmentNumber} writing with worker {workerNumber}. ThreadId: {Thread.CurrentThread.ManagedThreadId}");
            using (var fileSttream = new FileStream(_filePath, FileMode.Open, FileAccess.Write, FileShare.Write))
            {
                fileSttream.Seek(fileSegment.SegmentOffset, SeekOrigin.Begin);
                await fileSttream.WriteAsync(
                    fileSegment.SegmentBytes,
                    0,
                    fileSegment.SegmentBytes.Length,
                    _cancellationToken
                );

                timer.Stop();
                TimeSpan timeTaken = timer.Elapsed;

                Console.WriteLine($"The segment number {fileSegment.SegmentNumber} was written  with worker {workerNumber}. {fileSegment.SegmentBytes.Length} bytes were written. ThreadId: {Thread.CurrentThread.ManagedThreadId}. Time taken: {timeTaken.ToString(@"m\:ss\.fff")}");
                
            };
        }
    }
}
