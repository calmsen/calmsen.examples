﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.BigFilesCompression
{
    class Program
    {
        static void Main(string[] args)
        {
            //var timer = new Stopwatch();
            //timer.Start();

            //string file = @"C:\игры\Barbi.Lebedinoe.ozero.iso";
            //var compressor = new BigFileChunkCompressor(10);
            //compressor.CompressFile(file);

            //timer.Stop();
            //TimeSpan timeTaken = timer.Elapsed;
            //Console.WriteLine("Time taken: " + timeTaken.ToString(@"m\:ss\.fff"));
            Run();
            //AnalizeLog();
        }

        
        private static void Run()
        {
            var timer = new Stopwatch();
            timer.Start();

            //string sourceFilePath = @"E:\Мой диск\книги\Manning.The.Art.of.Unit.Testing.with.Examples.in.dot.NET.Jun.2009.pdf";
            //string sourceFilePath = @"C:\игры\Barbi.Lebedinoe.ozero.iso";
            string sourceFilePath = @"C:\игры\Devil May Cry 5\data1.bin";
            string destinationFilePath = @$"Temp\{Guid.NewGuid()}{Path.GetExtension(sourceFilePath)}.gz";
            //string destinationFilePath = @$"Temp\{Guid.NewGuid()}{Path.GetExtension(sourceFilePath)}";
            var cts = new CancellationTokenSource();
            int wokerCount = Environment.ProcessorCount;
            var fileCompressor = new BigFileCompressor(sourceFilePath, destinationFilePath, wokerCount, cts.Token);
            fileCompressor.CompressAsync().Wait();
            timer.Stop();

            TimeSpan timeTaken = timer.Elapsed;

            Console.WriteLine("Time taken: " + timeTaken.ToString(@"m\:ss\.fff"));
        }

        private static void AnalizeLog()
        {
            int[] readWorkers = new int[12];
            int[] compressedWorkers = new int[12];
            int[] writeWorkers = new int[12];
            string[] logs = File.ReadAllLines(@"D:\temp\test4.txt");
            string readWorkerQuery = "was read with worker ";
            string compressedWorkerQuery = "were compressed with worker ";
            string writeWorkerQuery = "was written  with worker ";
            string timeQuery = "Time taken: ";
            var readTimeSpan = new TimeSpan(0, 0, 0, 0, 0);
            var compressedTimeSpan = new TimeSpan(0, 0, 0, 0, 0);
            var writeTimeSpan = new TimeSpan(0, 0, 0, 0, 0);
            foreach (string line in logs)
            {
                if (line.IndexOf(readWorkerQuery) > 0)
                {
                    string workerNumber = line.Substring(line.IndexOf(readWorkerQuery) + readWorkerQuery.Length, 2).Trim(' ', '.');
                    readWorkers[int.Parse(workerNumber)]++;
                    string[] timeParts = line.Substring(line.IndexOf(timeQuery) + timeQuery.Length, 8).Split(':', '.').Select(x => x.TrimStart('0')).Select(x => x == "" ? "0" : x).ToArray();
                    readTimeSpan = readTimeSpan.Add(new TimeSpan(0, 0, int.Parse(timeParts[0]), int.Parse(timeParts[1]), int.Parse(timeParts[2])));
                }
                else if (line.IndexOf(compressedWorkerQuery) > 0)
                {
                    string workerNumber = line.Substring(line.IndexOf(compressedWorkerQuery) + compressedWorkerQuery.Length, 2).Trim(' ', '.');
                    compressedWorkers[int.Parse(workerNumber)]++;
                    string[] timeParts = line.Substring(line.IndexOf(timeQuery) + timeQuery.Length, 8).Split(':', '.').Select(x => x.TrimStart('0')).Select(x => x == "" ? "0" : x).ToArray();
                    compressedTimeSpan = compressedTimeSpan.Add(new TimeSpan(0, 0, int.Parse(timeParts[0]), int.Parse(timeParts[1]), int.Parse(timeParts[2])));
                }
                else if (line.IndexOf(writeWorkerQuery) > 0)
                {
                    string workerNumber = line.Substring(line.IndexOf(writeWorkerQuery) + writeWorkerQuery.Length, 2).Trim(' ', '.');
                    writeWorkers[int.Parse(workerNumber)]++;
                    string[] timeParts = line.Substring(line.IndexOf(timeQuery) + timeQuery.Length, 8).Split(':', '.').Select(x => x.TrimStart('0')).Select(x => x == "" ? "0" : x).ToArray();
                    writeTimeSpan = writeTimeSpan.Add(new TimeSpan(0, 0, int.Parse(timeParts[0]), int.Parse(timeParts[1]), int.Parse(timeParts[2])));
                }

            }
            Console.WriteLine($"All read time: {readTimeSpan}");
            Console.WriteLine($"All compressed time: {compressedTimeSpan}");
            Console.WriteLine($"All write time: {writeTimeSpan}");
            for(int i = 0; i < 12; i++)
            {
                Console.WriteLine($"read worker {i}: {readWorkers[i]}");
            }
            for (int i = 0; i < 12; i++)
            {
                Console.WriteLine($"compressed worker {i}: {compressedWorkers[i]}");
            }
            for (int i = 0; i < 12; i++)
            {
                Console.WriteLine($"write worker {i}: {writeWorkers[i]}");
            }

            Console.ReadKey();

        }
    }
}
