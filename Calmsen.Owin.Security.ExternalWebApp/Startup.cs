﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Calmsen.Owin.Security.ExternalWebApp.Startup))]
namespace Calmsen.Owin.Security.ExternalWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
