﻿using Microsoft.Owin.Security.OAuth;
using Owin;
using System.Web.Http;

namespace Calmsen.Owin.Security.WebApi
{
    /// <summary>
    /// Внимание: Необходимо установить сборку Microsoft.Owin.Host.SystemWeb
    /// </summary>
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(GlobalConfiguration.Configuration);
        }

        private void ConfigureOAuth(IAppBuilder app)
        {
            //Token Consumption
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}