﻿using Calmsen.Examples.Infrastructure.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calmsen.Examples.Test.Utils
{
    [TestClass]
    public class StringUtilsTest
    {
        [TestMethod]
        public void TestGetNullOrSubstringOfSpecificLength()
        {
            var utils = new StringUtils();
            const int substringLength = 10;
            string input = null;
            string result = utils.GetNullOrSubstringOfSpecificLength(input, substringLength);
            Assert.AreEqual(input, result);

            input = $"the str less than {substringLength} symbols".Substring(0, substringLength - 4) + ".."; ;
            result = utils.GetNullOrSubstringOfSpecificLength(input, substringLength);
            Assert.AreEqual(input, result);

            input = $"the str equals {substringLength} symbols".Substring(0, substringLength - 2) + "..";
            result = utils.GetNullOrSubstringOfSpecificLength(input, substringLength);
            Assert.AreEqual(input, result);

            input = $"the str more than {substringLength} symbols";
            result = utils.GetNullOrSubstringOfSpecificLength(input, substringLength);
            Assert.AreEqual(input.Substring(0, substringLength), result);
        }
    }
}
