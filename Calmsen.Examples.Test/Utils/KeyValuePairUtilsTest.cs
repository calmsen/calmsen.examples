﻿using System.Collections.Generic;
using Calmsen.Examples.Infrastructure.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calmsen.Examples.Test.Utils
{
    [TestClass]
    public class KeyValuePairUtilsTest
    {
        [TestMethod]
        public void TestMapParametersToString()
        {
            var list = new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("name1", null),
                new KeyValuePair<string, object>("name2", "value2"),
                new KeyValuePair<string, object>("name3", new {  value = "value3" }),
                new KeyValuePair<string, object>("name4", 4),
                new KeyValuePair<string, object>("name5", true)
            };
            var utils = new KeyValuePairUtils();
            var result = utils.MapParametersToString(list);
            Assert.AreEqual(result, "name: name1, value: , name: name2, value: value2, name: name3, value: { value = value3 }, name: name4, value: 4, name: name5, value: True");
        }
    }
}
