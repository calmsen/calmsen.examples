﻿using Calmsen.Examples.Test.ServiceContainers.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calmsen.Examples.Test.ServiceContainers
{
    public class MyNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IService>().To<Service1>();
        }
    }
}
