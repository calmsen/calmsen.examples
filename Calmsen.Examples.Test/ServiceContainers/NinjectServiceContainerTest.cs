﻿using Calmsen.Examples.Infrastructure.ServiceContainers;
using Calmsen.Examples.Test.ServiceContainers.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Calmsen.Examples.Test.ServiceContainers
{
    [TestClass]
    public class NinjectServiceContainerTest
    {
        [TestMethod]
        public void Test_BindToSelfByType()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindToSelf(typeof(Service1));

            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);
        }

        [TestMethod]
        public void Test_BindToSelfByTypeWthSpecificScope()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindToSelf(typeof(Service1), ServiceContainerScope.Singleton);

            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            var service1_ = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);
            Assert.AreEqual(service1, service1_);

            container.BindToSelf(typeof(Service1), ServiceContainerScope.Transient);

            service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1_ = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);
            Assert.AreNotEqual(service1, service1_);

        }

        [TestMethod]
        public void Test_BindToSelfByGenericType()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindToSelf<Service1>();
            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);
        }

        [TestMethod]
        public void Test_BindToSelfByPassObject()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            object obj = new Service1();
            container.BindToSelfOfConstant(obj);

            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);
        }

        [TestMethod]
        public void Test_BindToSelfByPassGenericObject()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            var obj = new Service1();
            container.BindToSelfOfConstant(obj);

            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);
        }
        
        [TestMethod]
        public void Test_BindToImplementaionByType()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindToImplementation(typeof(IService), typeof(Service1));

            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);

            service1 = container.Get(typeof(IService));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<IService>();
            Assert.AreNotEqual(service1, null);
        }

        [TestMethod]
        public void Test_BindToImplementaionByGenericType()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindToImplementation<IService, Service1>();

            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);

            service1 = container.Get(typeof(IService));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<IService>();
            Assert.AreNotEqual(service1, null);
        }

        [TestMethod]
        public void Test_BindToImplementaionByGenericTypeWithRebindAsFalse()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindToImplementation<IService, Service1>(rebind: false);
            container.BindToImplementation<IService, Service2>(rebind: false);

            var services = container.GetAll(typeof(IService));
            Assert.IsTrue(services.Count() >= 2);
            Assert.IsTrue(services.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services.Any(x => Equals(typeof(Service2), x.GetType())));
        }


        [TestMethod]
        public void Test_BindToImplementaionByPassObject()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            Type type = typeof(IService);
            object obj = new Service1();
            container.BindToImplementationOfConstant(type, obj);

            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);
        }

        [TestMethod]
        public void Test_BindToImplementaionByPassGenericObject()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            var obj = new Service1();
            container.BindToImplementationOfConstant<IService, Service1>(obj);

            var service1 = container.Get(typeof(Service1));
            Assert.AreNotEqual(service1, null);
            service1 = container.Get<Service1>();
            Assert.AreNotEqual(service1, null);
        }
        
        [TestMethod]
        public void Test_BindBaseTypeByBaseType()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            Type baseType = typeof(BaseService);
            container.BindBaseType(baseType);

            var services = container.GetAll(baseType);
            Assert.IsTrue(services.Count() >= 2);
            Assert.IsTrue(services.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services.Any(x => Equals(typeof(Service2), x.GetType())));
        }

        [TestMethod]
        public void Test_BindBaseTypeByBaseGenericType()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindBaseType<BaseService>();

            var services = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            Assert.IsTrue(services.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services.Any(x => Equals(typeof(Service2), x.GetType())));

            container.BindBaseType<IService>();

            var services_ = container.GetAll<IService>();
            Assert.IsTrue(services_.Count() >= 2);
            Assert.IsTrue(services_.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services_.Any(x => Equals(typeof(Service2), x.GetType())));
        }

        [TestMethod]
        public void Test_BindBaseTypeByBaseGenericTypeWithScope()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindBaseType<BaseService>(ServiceContainerScope.Singleton);

            var services = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            var service1 = services.FirstOrDefault(x => Equals(typeof(Service1), x.GetType()));
            var service2 = services.FirstOrDefault(x => Equals(typeof(Service2), x.GetType()));

            var services_ = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            var service1_ = services_.FirstOrDefault(x => Equals(typeof(Service1), x.GetType()));
            var service2_ = services_.FirstOrDefault(x => Equals(typeof(Service2), x.GetType()));

            Assert.AreEqual(service1, service1_);
            Assert.AreEqual(service2, service2_);

            container.BindBaseType<BaseService>(ServiceContainerScope.Transient);

            services = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            service1 = services.FirstOrDefault(x => Equals(typeof(Service1), x.GetType()));
            service2 = services.FirstOrDefault(x => Equals(typeof(Service2), x.GetType()));

            services_ = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            service1_ = services_.FirstOrDefault(x => Equals(typeof(Service1), x.GetType()));
            service2_ = services_.FirstOrDefault(x => Equals(typeof(Service2), x.GetType()));

            Assert.AreNotEqual(service1, service1_);
            Assert.AreNotEqual(service2, service2_);
        }

        [TestMethod]
        public void Test_BindSelectionByBaseType()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            Type baseType = typeof(BaseService);
            container.BindSelection(baseType, t => true);

            var services = container.GetAll(baseType);
            Assert.IsTrue(services.Count() >= 2);
            Assert.IsTrue(services.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services.Any(x => Equals(typeof(Service2), x.GetType())));
        }

        [TestMethod]
        public void Test_BindSelectionByBaseGenericType()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindSelection<BaseService>(t => true);

            var services = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            Assert.IsTrue(services.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services.Any(x => Equals(typeof(Service2), x.GetType())));

            container.BindSelection<IService>(t => true);

            var services_ = container.GetAll<IService>();
            Assert.IsTrue(services_.Count() >= 2);
            Assert.IsTrue(services_.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services_.Any(x => Equals(typeof(Service2), x.GetType())));
        }

        [TestMethod]
        public void Test_BindSelectionByBaseGenericTypeWithScope()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindSelection<BaseService>(t => true, ServiceContainerScope.Singleton);

            var services = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            var service1 = services.FirstOrDefault(x => Equals(typeof(Service1), x.GetType()));
            var service2 = services.FirstOrDefault(x => Equals(typeof(Service2), x.GetType()));

            var services_ = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            var service1_ = services_.FirstOrDefault(x => Equals(typeof(Service1), x.GetType()));
            var service2_ = services_.FirstOrDefault(x => Equals(typeof(Service2), x.GetType()));

            Assert.AreEqual(service1, service1_);
            Assert.AreEqual(service2, service2_);

            container.BindSelection<BaseService>(t => true, ServiceContainerScope.Transient);

            services = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            service1 = services.FirstOrDefault(x => Equals(typeof(Service1), x.GetType()));
            service2 = services.FirstOrDefault(x => Equals(typeof(Service2), x.GetType()));

            services_ = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            service1_ = services_.FirstOrDefault(x => Equals(typeof(Service1), x.GetType()));
            service2_ = services_.FirstOrDefault(x => Equals(typeof(Service2), x.GetType()));

            Assert.AreNotEqual(service1, service1_);
            Assert.AreNotEqual(service2, service2_);
        }

        [TestMethod]
        public void Test_Bind()
        {
            var container = new NinjectServiceContainer();
            container.Init();
            container.BindInNamespaceOf<BaseService>(BindableType.Self);

            var service1 = container.Get<Service1>();
            Assert.IsTrue(service1 != null);

            var service2 = container.Get<Service2>();
            Assert.IsTrue(service2 != null);

            container.BindInNamespaceOf<BaseService>(BindableType.DefaultInterface);

            var service1_ = container.Get<IService1>();
            Assert.IsTrue(service1 != null);

            var service2_ = container.Get<IService2>();
            Assert.IsTrue(service2 != null);

            container.BindInNamespaceOf<BaseService>(BindableType.AllBaseClasses);

            var services = container.GetAll<BaseService>();
            Assert.IsTrue(services.Count() >= 2);
            Assert.IsTrue(services.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services.Any(x => Equals(typeof(Service2), x.GetType())));

            container.BindInNamespaceOf<IService>(BindableType.AllInterfaces);

            var services_ = container.GetAll<IService>();
            Assert.IsTrue(services_.Count() >= 2);
            Assert.IsTrue(services_.Any(x => Equals(typeof(Service1), x.GetType())));
            Assert.IsTrue(services_.Any(x => Equals(typeof(Service2), x.GetType())));
        }
    }
}
