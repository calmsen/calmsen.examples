﻿using System.Diagnostics;
using System.Threading.Tasks;

namespace Calmsen.Examples.Test.PubSub
{
    public class Subscriber3
    {
        public Task<string> OnMessage(Message2 message)
        {
            Debug.WriteLine($"{message.Name} from Subscriber3");
            return Task.FromResult("OK");
        }
    }
}
