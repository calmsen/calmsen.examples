﻿using Calmsen.Examples.Infrastructure.PubSub;

namespace Calmsen.Examples.Test.PubSub
{
    public class Message2 : IMessage
    {
        public Message2(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
