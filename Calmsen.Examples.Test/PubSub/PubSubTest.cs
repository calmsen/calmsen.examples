﻿using Calmsen.Examples.Infrastructure.Provider;
using Calmsen.Examples.Infrastructure.PubSub;
using Calmsen.Examples.Test.Provider;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Calmsen.Examples.Test.PubSub
{
    [TestClass]
    public class PubSubTest
    {
        [TestMethod]
        public void Test_SubscrivbeWithBoolResult()
        {
            var subscriber1 = new Subscriber1();
            var subscriber2 = new Subscriber2();
            var publisher = new Publisher();
            publisher.Subscribe<Message1, bool>(subscriber1.OnMessage);
            publisher.Subscribe<Message1, bool>(subscriber2.OnMessage);
            var message1 = new Message1("Message1");

            publisher.Notify<Message1, bool>(message1).GetAwaiter().GetResult();
        }

        [TestMethod]
        public void Test_SubscrivbeWithStringResult()
        {
            var subscriber3 = new Subscriber3();
            var subscriber4 = new Subscriber4();
            var publisher = new Publisher();
            publisher.Subscribe<Message2, string>(subscriber3.OnMessage);
            publisher.Subscribe<Message2, string>(subscriber4.OnMessage);
            var message2 = new Message2("Message2");


            var t = publisher.Notify<Message2, string>(message2);
            t.Wait();
            var result = t.Result;
            Assert.AreEqual(result, "OK");
        }

        [TestMethod]
        public void Test_Unsubscribe()
        {
            var subscriber1 = new Subscriber1();
            var subscriber2 = new Subscriber2();
            var publisher = new Publisher();
            publisher.Subscribe<Message1, bool>(subscriber1.OnMessage);
            publisher.Subscribe<Message1, bool>(subscriber2.OnMessage);
            var message1 = new Message1("Message1");

            publisher.Notify<Message1, bool>(message1).GetAwaiter().GetResult();

            publisher.Unsubscribe<Message1, bool>(subscriber1.OnMessage);
            publisher.Notify<Message1, bool>(message1).GetAwaiter().GetResult();
        }

        [TestMethod]
        public void Test_SubscribeInParentScope()
        {

            var publishers = new List<IPublisher>
            {
            };
            var publisherFactories = new List<IFactory<IPublisher>>
            {   
            };
            var publisherProvider = new Provider<IPublisher>(publishers, publisherFactories);
            publisherProvider.Bind("publisher", () => new Publisher());

            var products = new List<BaseProduct>
            {
            };
            var factory2 = new Product2Factory();
            var factories = new List<IFactory<ProductInputData, BaseProduct>>
            {
                factory2
            };
            var productProvider = new Provider<BaseProduct>(products, factories);
            productProvider.Bind("p5", new Product5());
            var scope = new ProviderScope();
            var p2 = productProvider.Get("p2", new ProductInputData(), scope);
            var p5 = productProvider.Get("p5", scope);
            var publisher = publisherProvider.Get("publisher", scope);
            Assert.AreEqual(publisher, publisherProvider.Get("publisher", scope));
            var scope_ = new ProviderScope { ParentScope = scope };
            var p2_ = productProvider.Get("p2", new ProductInputData(), scope_);
            var p5_ = productProvider.Get("p5", scope_);
            var publisher_ = publisherProvider.Get("publisher", scope_);
            Assert.AreEqual(publisher_, publisherProvider.Get("publisher", scope_));

            Assert.AreNotEqual(publisher_, publisher);
            var subscriber1 = new Subscriber1();
            publisher.Subscribe<Message1, bool>(subscriber1.OnMessage);
            publisher_.Notify<Message1, bool>(new Message1("Message1")).GetAwaiter().GetResult();
        }
    }
}
