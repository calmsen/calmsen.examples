﻿using System.Diagnostics;
using System.Threading.Tasks;

namespace Calmsen.Examples.Test.PubSub
{
    public class Subscriber2
    {
        public Task<bool> OnMessage(Message1 message)
        {
            Debug.WriteLine($"{message.Name} from Subscriber2");
            return Task.FromResult(true);
        }
    }
}
