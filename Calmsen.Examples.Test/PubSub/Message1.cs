﻿using Calmsen.Examples.Infrastructure.PubSub;

namespace Calmsen.Examples.Test.PubSub
{
    public class Message1: IMessage
    {
        public Message1(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
