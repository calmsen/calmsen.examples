﻿using Calmsen.Examples.Infrastructure.PubSub;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Calmsen.Examples.Test.PubSub
{
    public class Subscriber4
    {
        public Task<string> OnMessage(Message2 message)
        {
            Debug.WriteLine($"{message.Name} from Subscriber4");
            return Task.FromResult("OK");
        }
    }
}
