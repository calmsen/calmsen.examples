﻿using System;
using Calmsen.Examples.Infrastructure.Ninject;
using Calmsen.Examples.Infrastructure.Ninject.BaseModuleTest;
using Calmsen.Examples.Infrastructure.Provider;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calmsen.Examples.Test
{
    [TestClass]
    public class NinjectTest
    {
        [TestMethod]
        public void TestNinjectBaseModule()
        {
            var ninjectModuleProvider = new NinjectServiceProvider();
            ninjectModuleProvider.Init();
            var provider = ninjectModuleProvider.Get<Provider<BaseModule>>();
            BaseModule m2 = provider.Get<TypeBaseModule<Type2>>();
            m2 = provider.Get("TypeBaseModule<Type2>");
        }
    }
}
