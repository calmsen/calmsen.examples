﻿using Calmsen.Examples.Infrastructure.Provider;

namespace Calmsen.Examples.Test.Provider
{
    [Key("p2")]
    class Product2Factory : IFactory<ProductInputData, BaseProduct>
    {
        public BaseProduct Create(ProductInputData context)
        {
            var p = new Product2();
            p.State = context.State;
            return p;
        }
    }
}
