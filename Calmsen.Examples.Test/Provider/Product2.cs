﻿using Calmsen.Examples.Infrastructure.Provider;

namespace Calmsen.Examples.Test.Provider
{
    class Product2 : BaseProduct, IInitable<ProductInputData>
    {
        public string State { get; set; }

        public void Init(ProductInputData data)
        {
            State = data.State;
        }
    }
}
