﻿using Calmsen.Examples.Infrastructure.Provider;

namespace Calmsen.Examples.Test.Provider
{
    public class ProductInputDataWithKey: IHasKey
    {
        public string State { get; set; }

        public string Key { get; set; }
    }
}
