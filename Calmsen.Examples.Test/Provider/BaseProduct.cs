﻿using Calmsen.Examples.Infrastructure.Provider;

namespace Calmsen.Examples.Test.Provider
{
    public abstract class BaseProduct : IHasProviderScope
    {
        public IProviderScope ProviderScope { get; set; }
    }

    public abstract class BaseProduct<T> : BaseProduct, ICloneable<T> where T : BaseProduct
    {
        public T Clone()
        {
            return MemberwiseClone() as T;
        }
    }
}
