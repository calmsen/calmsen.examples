﻿using System;
using System.Collections.Generic;
using Calmsen.Examples.Infrastructure.Provider;
using Calmsen.Examples.Test.Provider;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calmsen.Examples.Test
{
    [TestClass]
    public class ProviderTest
    {
        [TestMethod]
        public void TestProviderSingleMethod()
        {
            var productProvider = new Provider<Product5>(new Product5[] { new Product5() }, null);
            var product = productProvider.Single();
            Assert.IsTrue(product != null);
        }

        [TestMethod]
        public void TestProviderSingleMethodWithData()
        {
            var productProvider = new Provider<Product2>(new Product2[] { new Product2() }, null);
            var data = new ProductInputData { State = "2" };
            var product = productProvider.Single(data);
            Assert.IsTrue(product != null);
            Assert.AreEqual(product.State, data.State);
        }

        [TestMethod]
        public void TestProviderGenericGetMethod()
        {
            var productProvider = new Provider<BaseProduct>(new BaseProduct[] { new Product5(), new Product2() }, null);
            var product = productProvider.Get<Product5>();
            Assert.IsTrue(product != null);
        }

        [TestMethod]
        public void TestProviderGenericGetMethodWithData()
        {
            var productProvider = new Provider<BaseProduct>(new BaseProduct[] { new Product5(), new Product2() }, null);
            var data = new ProductInputData { State = "2" };
            var product = productProvider.Get<Product2, ProductInputData>(data);
            Assert.IsTrue(product != null);
            Assert.AreEqual(product.State, data.State);
        }

        [TestMethod]
        public void TestProviderTypeGetMethod()
        {
            var productProvider = new Provider<BaseProduct>(new BaseProduct[] { new Product5(), new Product2() }, null);
            var product = productProvider.Get(typeof(Product5));
            Assert.IsTrue(product != null);
        }

        [TestMethod]
        public void TestProviderTypeGetMethodWithData()
        {
            var productProvider = new Provider<BaseProduct>(new BaseProduct[] { new Product5(), new Product2() }, null);
            var data = new ProductInputData { State = "2" };
            var product = productProvider.Get(typeof(Product2), data);
            Assert.IsTrue(product != null);
            Assert.AreEqual((product as Product2)?.State, data.State);
        }

        [TestMethod]
        public void TestProviderGetMethodWithDataHasKey()
        {
            var productProvider = new Provider<BaseProduct>(new BaseProduct[] { new Product5(), new Product3() }, null);
            var data = new ProductInputDataWithKey { Key = "Product3", State = "3" };
            var product = productProvider.Get(data);
            Assert.IsTrue(product != null);
            Assert.AreEqual((product as Product3)?.State, data.State);
        }

        [TestMethod]
        public void TestProviderKeyGetMethod()
        {
            var productProvider = new Provider<BaseProduct>(new BaseProduct[] { new Product1(), new Product2() }, null);
            var product = productProvider.Get("Product1");
            Assert.IsTrue(product != null);
            product = productProvider.Get("p1");
            Assert.IsTrue(product != null);
        }

        [TestMethod]
        public void TestProviderKeyGetMethodWithData()
        {
            var productProvider = new Provider<BaseProduct>(new BaseProduct[] { new Product1(), new Product2() }, null);
            var data = new ProductInputData { State = "1" };
            var product = productProvider.Get("p1", data);
            Assert.IsTrue(product != null);
            Assert.AreEqual((product as Product1)?.State, data.State);
        }


        [TestMethod]
        public void TestProductProvider()
        {
            var product = new Product1();
            var products = new List<BaseProduct>
            {
                product
            };
            var factory2 = new Product2Factory();
            var factories = new List<ITypeFactory<BaseProduct>>
            {
                factory2
            };
            var productProvider = new Provider<BaseProduct>(products, factories);
            productProvider.Bind("p3", new Product3());
            productProvider.Bind("p4", () => new Product4());
            productProvider.Bind("p5", new Product5());
            productProvider.Bind<ProductInputData>("p6", context => new Product6());
            var p1 = productProvider.Get("p1");
            var p2 = productProvider.Get("p2", new ProductInputData { State = "124" });
            var p3 = productProvider.Get("p3");
            var p4 = productProvider.Get("p4");
            var p5 = productProvider.Get<Product5>();
        }

        [TestMethod]
        public void TestProductProviderContext()
        {
            var product = new Product1();
            var products = new List<BaseProduct>
            {
                product
            };
            var factory2 = new Product2Factory();
            var factories = new List<IFactory<ProductInputData, BaseProduct>>
            {
                factory2
            };
            var productProvider = new Provider<BaseProduct>(products, factories);
            productProvider.Bind("p5", new Product5());
            var context = new ProductInputData { State = "111" };
            var p1 = productProvider.Get("p1", context);
            var p2 = productProvider.Get("p2", context);
            var p5 = productProvider.Get("p5", context);
            var context_ = new ProductInputData { State = "222" };
            var p1_ = productProvider.Get("p1", context_);
            var p2_ = productProvider.Get("p2", context_);
            var p5_ = productProvider.Get("p5", context_);
        }
    }
}
