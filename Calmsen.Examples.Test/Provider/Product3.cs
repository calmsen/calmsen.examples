﻿using Calmsen.Examples.Infrastructure.Provider;

namespace Calmsen.Examples.Test.Provider
{
    class Product3 : BaseProduct, IInitable<ProductInputDataWithKey>
    {
        public string State { get; set; }

        public void Init(ProductInputDataWithKey data)
        {
            State = data.State;
        }

    }
}
