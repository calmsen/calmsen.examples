﻿using System;
using System.Collections.Generic;
using Calmsen.Examples.Infrastructure.Provider;
using Calmsen.Examples.Infrastructure.PubSub;

namespace Calmsen.Examples.Test.Provider
{
    public class ProductInputData
    {
        public string State { get; set; }
    }
}
