﻿using Calmsen.Examples.Infrastructure.Provider;

namespace Calmsen.Examples.Test.Provider
{
    [Key("p1")]
    public class Product1 : BaseProduct, IInitable<ProductInputData>
    {
        public string State { get; set; }

        public void Init(ProductInputData data)
        {
            State = data.State;
        }
    }
}
