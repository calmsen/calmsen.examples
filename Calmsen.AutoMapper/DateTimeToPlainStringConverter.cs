﻿using System;
using AutoMapper;

namespace Calmsen.AutoMapper
{
    internal class NullableDateTimeToPlainStringConverter : ITypeConverter<DateTime?, string>
    {
        public string Convert(DateTime? source, string destination, ResolutionContext context)
        {
            return source?.ToString("s");
        }
    }

    internal class DateTimeToPlainStringConverter : ITypeConverter<DateTime, string>
    {
        public string Convert(DateTime source, string destination, ResolutionContext context)
        {
            return source.ToString("s");
        }
    }
}