// <copyright file="Period.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

namespace Calmsen.AutoMapper.ViewModels
{
    public class Period
    {
        public string DateFrom { get; set; }

        public string DateTo { get; set; }

    }
}
