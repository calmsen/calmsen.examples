﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Calmsen.AutoMapper.Models;

namespace Calmsen.AutoMapper
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DateTime?, string>().ConvertUsing<NullableDateTimeToPlainStringConverter>();
                cfg.CreateMap<DateTime, string>().ConvertUsing<DateTimeToPlainStringConverter>();

                cfg.CreateMap<Period, ViewModels.Period>();
            });
            var mapper = config.CreateMapper();
            var a = mapper.Map<ViewModels.Period>(new Period
            {
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now.AddDays(2),
            });
            Console.WriteLine(a);
        }
    }
}
