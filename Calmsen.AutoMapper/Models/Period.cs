// <copyright file="Period.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System;

namespace Calmsen.AutoMapper.Models
{
    public class Period
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

    }
}
