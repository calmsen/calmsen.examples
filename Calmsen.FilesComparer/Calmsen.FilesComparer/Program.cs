﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Calmsen.FilesComparer
{
    class Program
    {
        static void Main(string[] args)
        {
            var file1 = @"__FILE1__";
            var file2 = @"__FILE2__";
            var file1AllLines = File.ReadAllLines(file1);
            var file2AllLines = File.ReadAllLines(file2);
            // Находим все отличия
            var file1AllLinesDiff = new List<string>();
            var file2AllLinesDiff = new List<string>();
            foreach (var lineOuter in file1AllLines)
            {
                var exists = false;
                foreach (var lineInner in file2AllLines)
                {
                    if (lineOuter.Trim() == lineInner.Trim())
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                {
                    file1AllLinesDiff.Add(lineOuter);
                }
            }
            foreach (var lineOuter in file2AllLines)
            {
                var exists = false;
                foreach (var lineInner in file1AllLines)
                {
                    if (lineOuter.Trim() == lineInner.Trim())
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                {
                    file2AllLinesDiff.Add(lineOuter);
                }
            }
            var file1Diff = Path.GetFileName(file1) + "_diff1";
            var file2Diff = Path.GetFileName(file1) + "_diff2";
            File.WriteAllLines(file1Diff, file1AllLinesDiff);
            File.WriteAllLines(file2Diff, file2AllLinesDiff);

            // Находим опции которые отличаются значением
            var fileAllLinesDiffValue = new List<string>();
            foreach (var lineOuter in file1AllLinesDiff)
            {
                if (lineOuter[0] == '#')
                {
                    continue;
                }

                var exists = false;
                foreach (var lineInner in file2AllLinesDiff)
                {
                    if (lineOuter.Split(" ")[0].Trim() == lineInner.Split(" ")[0].Trim())
                    {
                        exists = true;
                        break;
                    }
                }
                if (exists)
                {
                    fileAllLinesDiffValue.Add(lineOuter);
                }
            }
            var fileDiffValue = Path.GetFileName(file1) + "_diff_value";
            File.WriteAllLines(fileDiffValue, fileAllLinesDiffValue);
            // Находим опции которые присутствуют только в первом файле и только во втором файле
            var file1AllLinesOnly = new List<string>();
            var file2AllLinesOnly = new List<string>();
            foreach (var lineOuter in file1AllLinesDiff)
            {
                if (lineOuter[0] == '#')
                {
                    continue;
                }

                var exists = false;
                foreach (var lineInner in file2AllLinesDiff)
                {
                    if (lineOuter.Split(" ")[0].Trim() == lineInner.Split(" ")[0].Trim())
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                {
                    file1AllLinesOnly.Add(lineOuter);
                }
            }
            foreach (var lineOuter in file2AllLinesDiff)
            {
                if (lineOuter[0] == '#')
                {
                    continue;
                }

                var exists = false;
                foreach (var lineInner in file1AllLinesDiff)
                {
                    if (lineOuter.Split(" ")[0].Trim() == lineInner.Split(" ")[0].Trim())
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                {
                    file2AllLinesOnly.Add(lineOuter);
                }
            }
            var file1Only = Path.GetFileName(file1) + "_only1";
            var file2Only = Path.GetFileName(file2) + "_only2";
            File.WriteAllLines(file2Only, file2AllLinesOnly);
            Console.WriteLine("Hello World!");
        }
    }

    public class Test
    {
        private readonly int field = 1;

        public Test(int field)
        {
            this.field = field;
        }

        public void Method()
        {
            Console.WriteLine(this.field);
        }
    }
}
