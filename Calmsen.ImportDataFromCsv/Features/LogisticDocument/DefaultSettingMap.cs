﻿using CsvHelper.Configuration;

namespace Calmsen.ImportDataFromCsv.Features.LogisticDocument
{
    public class DefaultSettingMap : ClassMap<DefaultSetting>
    {
        public DefaultSettingMap()
        {
            Map(m => m.SettingName).Name("setting_name");
            Map(m => m.SettingType).Name("setting_type");
            Map(m => m.SettingDescription).Name("setting_description");
            Map(m => m.ValueBoolean).Name("value_boolean");
            Map(m => m.ValueInt).Name("value_int");
            Map(m => m.ValueReal).Name("value_real");
            Map(m => m.ValueText).Name("value_text");
        }
    }
}