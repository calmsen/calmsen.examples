﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Calmsen.ImportDataFromCsv.Features.LogisticDocument
{
    public class PackagingType
    {
        [Key]
        [Column("packaging_type")]
        public string Type { get; set; }

        [Column("name_ru")]
        public string NameRu { get; set; }

        [Column("name_en")]
        public string NameEn { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; }

        [Column("cargo_name")]
        public string CargoName{ get; set; }

        [Column("cargo_condition")]
        public string CargoCondition { get; set; }

        [Column("packing_description")]
        public string PackingDescription { get; set; }
    }
}