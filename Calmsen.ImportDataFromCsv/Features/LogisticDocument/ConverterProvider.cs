﻿using System.Collections;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Calmsen.ImportDataFromCsv.Features.LogisticDocument
{
    public static class ConverterProvider
    {
        public static ValueConverter<bool?, BitArray> GetBoolToBitArrayConverter()
        {
            return new ValueConverter<bool?, BitArray>(
                value => value == null ? null : new BitArray(new[] { (bool)value }),
                value => value == null ? null : value.Get(0));
        }
    }
}