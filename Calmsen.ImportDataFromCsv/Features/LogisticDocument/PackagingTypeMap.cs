﻿using CsvHelper.Configuration;

namespace Calmsen.ImportDataFromCsv.Features.LogisticDocument
{
    public class PackagingTypeMap : ClassMap<PackagingType>
    {
        public PackagingTypeMap()
        {
            Map(m => m.Type).Name("packaging_type");
            Map(m => m.NameRu).Name("name_ru");
            Map(m => m.NameEn).Name("name_en");
            Map(m => m.IsActive).Name("is_active").TypeConverterOption.BooleanValues(true, true, "1");
            Map(m => m.CargoName).Name("cargo_name");
            Map(m => m.CargoCondition).Name("cargo_condition");
            Map(m => m.PackingDescription).Name("packing_description");
        }

    }
}