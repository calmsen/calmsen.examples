﻿using Calmsen.ImportDataFromCsv.Features.LogisticDocument;
using Microsoft.EntityFrameworkCore;

namespace Calmsen.PostgreSql
{
    public class ApplicationContext : DbContext
    {
        public DbSet<PackagingType> PackagingTypes { get; set; }

        public DbSet<DefaultSetting> DefaultSettings { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=logistic_document;Username=logistic_document_user;Password=123456");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<PackagingType>()
                .ToTable("packaging_type", "common");
            modelBuilder.Entity<DefaultSetting>()
                .ToTable("default_setting", "common")
                .Property(x => x.ValueBoolean).HasColumnType("bit(1)")
                .HasConversion(ConverterProvider.GetBoolToBitArrayConverter());
        }
    }
}
