﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Calmsen.ImportDataFromCsv.Features.LogisticDocument
{
    public class DefaultSetting
    {
        [Key]
        [Column("setting_name")]
        public string SettingName { get; set; }

        [Column("setting_type")]
        public int SettingType { get; set; }

        [Column("value_text")]
        public string ValueText { get; set; }

        [Column("value_real")]
        public float? ValueReal { get; set; }

        [Column("value_int")]
        public int? ValueInt { get; set; }

        [Column("value_boolean")]
        public bool? ValueBoolean { get; set; }

        [Column("setting_description")]
        public string SettingDescription { get; set; }
    }
}