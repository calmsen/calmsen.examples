﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Calmsen.ImportDataFromCsv.Features.LogisticDocument;
using Calmsen.PostgreSql;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Calmsen.ImportDataFromCsv
{
    class Program
    {
        static void Main(string[] args)
        {
            const string csvFilePath = @"C:\info\документы\LogisticDocument\ImportDataFromCsv\default_setting_GO.csv";
            ImportToDb<ApplicationContext, DefaultSetting, DefaultSettingMap>(csvFilePath);
        }

        private static void ImportToDb<TDbContext, TEntity, TPackagingTypeMap>(string csvFilePath)
            where TDbContext : DbContext, new()
            where TEntity : class
            where TPackagingTypeMap : ClassMap<TEntity>
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                IgnoreBlankLines = false,
                Encoding = Encoding.UTF8,
                Delimiter = ";"
            };
            using var reader = new StreamReader(csvFilePath);
            using var csv = new CsvReader(reader, config);
            using var db = new TDbContext();
            csv.Context.RegisterClassMap<TPackagingTypeMap>();
            var records = csv.GetRecords<TEntity>().ToArray();
            db.Set<TEntity>().AddRange(records);
            db.SaveChanges();
        }
    }
}