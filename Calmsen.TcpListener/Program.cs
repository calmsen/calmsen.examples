﻿using System;

namespace Calmsen.ChatAsyncServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new Server();
            server.Start().Wait();
            Console.WriteLine("Hello World!");
        }
    }
}
