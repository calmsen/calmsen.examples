﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calmsen.ChatAsyncServer
{
    public class Client: IDisposable
    {
        private TcpClient _tcpClient;
        private NetworkStream _networkStream;
        private bool disposedValue;
        public Client(TcpClient tcpClient)
        {
            this._tcpClient = tcpClient;
        }

        public async Task<string> ReadAsync()
        {
            ThrowIfDisposed();
            var networkStream = GetStream();

            var data = new StringBuilder();
            var buffer = new byte[1024];
            int readedBytes;
            do
            {
                readedBytes = await networkStream.ReadAsync(buffer, 0, buffer.Length);
                data.Append(Encoding.UTF8.GetString(buffer, 0, readedBytes));
            }
            while (networkStream.DataAvailable);
            return data.ToString();
        }

        public async Task WriteAsync(string data)
        {
            ThrowIfDisposed();
            var networkStream = GetStream();

            byte[] dataAsBytes = Encoding.UTF8.GetBytes(data);
            await networkStream.WriteAsync(dataAsBytes, 0, dataAsBytes.Length);
        }

        private NetworkStream GetStream() => _networkStream = _networkStream ?? _tcpClient.GetStream();

        private void ThrowIfDisposed()
        {
            if (disposedValue)
            {
                string message;
                Console.WriteLine(message = "Client is disposed.");
                throw new InvalidOperationException(message);
            }

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _networkStream?.Dispose();
                    _networkStream = null;
                    _tcpClient?.Dispose();
                    _tcpClient = null;
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
