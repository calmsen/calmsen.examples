﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;

namespace Calmsen.ChatAsyncServer
{
    public class Server: IDisposable
    {
        private TcpListener _tcpListener;
        private List<Client> _clients = new List<Client>();
        private bool disposedValue;
        private object _clientsListLockObject = new object();
        public async Task Start()
        {
            try
            {
                if (_tcpListener != null)
                {
                    Console.WriteLine("Server is already running.");
                    return;
                }

                Console.WriteLine("Starting server..");
                int port = 13000;
                var localAddr = IPAddress.Parse("127.0.0.1");

                _tcpListener = new TcpListener(localAddr, port);

                _tcpListener.Start();
                Console.WriteLine("Server started successfully.");

                while(true)
                {
                    if (_tcpListener == null) // when server is stopped then _tcpListener is assigned to null
                        return;

                    var tcpClient = await _tcpListener.AcceptTcpClientAsync();
                    ProcessClient(new Client(tcpClient));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to start server.\r\nMessage: {e.Message}.\r\nStackTrace: {e.StackTrace}");
            }
        }

        public async void ProcessClient(Client client)
        {
            _clients.Add(client);
            try
            {
                // получаем имя пользователя
                string message = await client.ReadAsync();
                string userName = message;

                message = userName + " вошел в чат";
                // посылаем сообщение о входе в чат всем подключенным пользователям
                await BroadcastMessage(message, client);
                Console.WriteLine(message);
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        message = await client.ReadAsync();
                        message = String.Format("{0}: {1}", userName, message);
                        Console.WriteLine(message);
                        await BroadcastMessage (message, client);
                    }
                    catch
                    {
                        message = String.Format("{0}: покинул чат", userName);
                        Console.WriteLine(message);
                        await BroadcastMessage(message, client);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                lock (_clientsListLockObject)
                {
                    _clients.Remove(client);
                }
                
                client.Dispose();
            }
        }

        protected internal async Task BroadcastMessage(string message, Client client)
        {
            var clients = _clients.ToArray();
            foreach (var c in clients)
            {
                if (c != client)
                {
                    await c.WriteAsync(message);
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                    _tcpListener?.Stop();
                    foreach(var client in _clients)
                        client.Dispose();
                    _clients.Clear();
                    _tcpListener = null;
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
