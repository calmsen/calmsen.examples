﻿// <copyright file="BigFileController.cs" company="Hoff">
// Copyright (c) Company. All rights reserved.
// </copyright>

using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Gems.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace Logistic.Document;

public class BigFileController : ControllerBase
{
    private readonly TaskCompletionSource<bool> streamEnded = new TaskCompletionSource<bool>();

    [HttpGet("WriteBigFile")]
    public async Task WriteBigFile(CancellationToken cancellationToken)
    {
        this.PassBigFile(this.Response.Body, cancellationToken).SafeFireAndForget(true);
        await this.streamEnded.Task;
    }

    [HttpPost("ReadBigFile")]
    public async Task ReadBigFile()
    {
        await this.DownloadFile(new Uri("https://localhost:5001/WriteBigFile"), "c:/temp/testbigfile");
    }

    public async Task DownloadFile(Uri url, string outputFilePath)
    {
        const int bufferSize = 16 * 1024;
        using var outputFileStream = System.IO.File.Create(outputFilePath, bufferSize);
#pragma warning disable SYSLIB0014
        var req = WebRequest.CreateHttp(url);
#pragma warning restore SYSLIB0014
        using var response = await req.GetResponseAsync();
        await using var responseStream = response.GetResponseStream();
        var buffer = new byte[bufferSize];
        int bytesRead;
        do
        {
            bytesRead = await responseStream.ReadAsync(buffer, 0, bufferSize);
            outputFileStream.Write(buffer, 0, bytesRead);
        }
        while (bytesRead > 0);
    }

    private async Task PassBigFile(Stream destinationStream, CancellationToken cancellationToken)
    {
        var buffer = new byte[16 * 1024]; // 8KB buffer size (can be adjusted based on your needs)
        var l = 32 * 1024;
        while (l-- > 0)
        {
            for (var i = 0; i < buffer.Length; i++)
            {
                buffer[i] = (byte)new Random().Next(0, 254);
            }

            await destinationStream.WriteAsync(buffer, 0, buffer.Length, cancellationToken);
        }

        this.streamEnded.SetResult(true);
    }
}
