﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace Calmsen.SelfHost40
{
    class Program
    {
        static void Main(string[] args)
        {
            var selfHostConfiguraiton = new HttpSelfHostConfiguration("http://localhost:8888");

            selfHostConfiguraiton.Routes.MapHttpRoute(
                name: "DefaultApiRoute",
                routeTemplate: "api/{controller}",
                defaults: null
            );
            using (var server = new HttpSelfHostServer(selfHostConfiguraiton))
            {
                server.OpenAsync().Wait();
                Console.ReadLine();
            }

        }
    }

    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class ProductController: ApiController
    {
        public IList<Product> GetAllProducts()
        {
            return new List<Product>(){
            new Product(){ID = 1, Name="Product 1", Description="Desc 1"},
            new Product(){ID = 2, Name="Product 2", Description="Desc 2"},
            new Product(){ID = 3, Name="Product 3", Description="Desc 3"},
    };
        }
    }
}
