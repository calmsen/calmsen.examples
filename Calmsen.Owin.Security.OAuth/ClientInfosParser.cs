﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Calmsen.Owin.Security.OAuth
{
    public class ClientInfosParser
    {
        public ClientInfo[] Parse(string clientInfosAsString)
        {
            clientInfosAsString = Regex.Replace(clientInfosAsString, @"\s+", "");

            var clientInfos = new List<ClientInfo>();
            foreach (var clientInfoAsString in clientInfosAsString.Split(new char[] { ';' } , StringSplitOptions.RemoveEmptyEntries))
            {
                var clientInfoParts = clientInfoAsString.Split(',');
                var clientCredential = new ClientInfo
                {
                    ClientId = clientInfoParts[0].Split('=')[1],
                    ClientSecret = clientInfoParts[1].Split('=')[1],
                    BaseUrl = clientInfoParts[1].Split('=')[2],
                    RedirectUri = clientInfoParts[1].Split('=')[3]
                };
                clientInfos.Add(clientCredential);
            }
            return clientInfos.ToArray();
        }
    }
}
