﻿namespace Calmsen.Owin.Security.OAuth
{
    public class ClientInfo
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string BaseUrl { get; set; }
        public string RedirectUri { get; set; }
    }
}
