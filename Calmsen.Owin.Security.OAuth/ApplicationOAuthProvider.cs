﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Calmsen.Owin.Security.OAuth
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private ClientInfo[] _clientInfos;
        
        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            var clientInfo = _clientInfos.FirstOrDefault(x => x.ClientId.Equals(context.ClientId));
            if (clientInfo != null)
            {
                if (!string.IsNullOrEmpty(context.RedirectUri))
                {
                    if (Equals(context.RedirectUri.TrimEnd('/'), clientInfo.RedirectUri.TrimEnd('/')))
                    {
                        context.Validated(context.RedirectUri);
                    }
                    else
                    {
                        if (context.RedirectUri.IndexOf(clientInfo.BaseUrl.TrimEnd('/')) == 0)
                        {
                            context.Validated(context.RedirectUri);
                        }
                    }
                }
                else
                {
                    var baseUrl = new Uri(context.Request.Uri, "/");
                    if (Equals(baseUrl.AbsoluteUri.TrimEnd('/'), clientInfo.BaseUrl.TrimEnd('/')))
                    {
                        context.Validated(baseUrl.AbsoluteUri);
                    }
                }
                context.Validated(clientInfo.BaseUrl);
            }
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;
            if (context.TryGetFormCredentials(out clientId, out clientSecret))
            {
                var clientInfo = GetClientInfos().FirstOrDefault(x => x.ClientId.Equals(context.ClientId));
                if (clientInfo != null && Equals(clientId, clientInfo.ClientId) && Equals(clientSecret, clientInfo.ClientSecret))
                {
                    context.Validated();
                }
            }

            

            return Task.FromResult<object>(null);
        }



        private ClientInfo[] GetClientInfos()
        {
            if (_clientInfos == null)
            {
                var parser = new ClientInfosParser();
                string clientCredentials = WebConfigurationManager.AppSettings["clientInfos"];
                _clientInfos = parser.Parse(clientCredentials);
            }
            return _clientInfos;
        }
    }
}
