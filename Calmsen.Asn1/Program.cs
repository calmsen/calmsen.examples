﻿using System;
using System.Collections.Generic;
using System.Formats.Asn1;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace Calmsen.Asn1
{
    class Program
    {
        static void Main(string[] args)
        {
            var x509 = new X509Certificate2("2.cer");
			InstallChainCerts(x509);
		}

		private static void WriteLog(string message)
        {
			const string FileLogName = "file.log";
			File.AppendAllText(FileLogName, message + "\r\n");
        }

		private static string GetIssuerCertUrl(X509Certificate2 cert)
		{
			const string AuthorityInformationAccessOid = "1.3.6.1.5.5.7.1.1";
			string groupName = "url"; // именованная группа с результатом
			string pattern = @$"^\s*URL=(?<{groupName}>\S*)\s*$";

			// расширение со списком отзыва
			X509Extension extension = cert.Extensions[AuthorityInformationAccessOid];

			// не указан источник CRL
			if (extension == null)
				return null;

			// значение расширения
			string extenstionAsString = new AsnEncodedData(
					extension.Oid, extension.RawData).Format(true);
			WriteLog(extenstionAsString);
			// поиск по строке
			var regex = new Regex(pattern, RegexOptions.Multiline);
			MatchCollection matches = regex.Matches(extenstionAsString);

			List<string> items = new List<string>();
			foreach (Match match in matches)
				items.Add(match.Groups[groupName].Value);

			return items.FirstOrDefault();
		}

		private static string GetIssuerCertSerialNumber(X509Certificate2 cert)
		{
			const string AuthorityKeyIdentifierOid = "2.5.29.35";
			string groupName = "SerialNumber"; // именованная группа с результатом
			string pattern = @$"^\s*(Certificate SerialNumber|Серийный номер сертификата)=(?<{groupName}>[\S ]*)\s*$";

			// расширение со списком отзыва
			X509Extension extension = cert.Extensions[AuthorityKeyIdentifierOid];

			// не указан источник CRL
			if (extension == null)
				return null;
			// значение расширения
			string extenstionAsString = new AsnEncodedData(
					extension.Oid, extension.RawData).Format(true);
			WriteLog(extenstionAsString);
			// поиск по строке
			var regex = new Regex(pattern, RegexOptions.Multiline);
			MatchCollection matches = regex.Matches(extenstionAsString);

			List<string> items = new List<string>();
			foreach (Match match in matches)
				items.Add(match.Groups[groupName].Value);

			return items.FirstOrDefault()?.Replace(" ", "").ToUpper();
		}

		private static string GetIssuerCommonName(X509Certificate2 cert)
		{
			const string AuthorityKeyIdentifierOid = "2.5.29.35";
			string groupName = "CN"; // именованная группа с результатом
			string pattern = @$"^\s*CN=(?<{groupName}>[\S ]*)\s*$";

			// расширение со списком отзыва
			X509Extension extension = cert.Extensions[AuthorityKeyIdentifierOid];

			// не указан источник CRL
			if (extension == null)
				return null;
			// значение расширения
			string extenstionAsString = new AsnEncodedData(
					extension.Oid, extension.RawData).Format(true);
			WriteLog(extenstionAsString);
			// поиск по строке
			var regex = new Regex(pattern, RegexOptions.Multiline);
			MatchCollection matches = regex.Matches(extenstionAsString);

			List<string> items = new List<string>();
			foreach (Match match in matches)
				items.Add(match.Groups[groupName].Value);

			return items.FirstOrDefault();
		}

		private static void InstallCert(X509Certificate2 x509Certificate2, StoreName storeName)
        {
			var store = new X509Store(storeName, StoreLocation.CurrentUser);
			store.Open(OpenFlags.ReadWrite);
			store.Add(x509Certificate2);
			store.Close();
		}

		private static void InstallChainCerts(X509Certificate2 cert)
        {
			while (true)
			{				
				InstallIssuerCert(cert, out X509Certificate2 issuerCert);
				cert = issuerCert;
				string issuerCertSerialNumber = GetIssuerCertSerialNumber(cert);
				if (string.Equals(issuerCertSerialNumber, cert.SerialNumber, StringComparison.OrdinalIgnoreCase))
					return;
			}
        }



		private static void InstallIssuerCert(X509Certificate2 cert, out X509Certificate2 issuerCert)
        {
			string certUrl = GetIssuerCertUrl(cert);
			WriteLog($"certUrl: {certUrl}");
			issuerCert = DownloadCert(certUrl);
			string issuerCertSerialNumber = GetIssuerCertSerialNumber(issuerCert);
			WriteLog($"issuerCertSerialNumber: {issuerCertSerialNumber}");
			var storeName = string.Equals(issuerCertSerialNumber, issuerCert.SerialNumber, StringComparison.OrdinalIgnoreCase)
				? StoreName.Root
				: StoreName.CertificateAuthority;

			InstallCert(issuerCert, storeName);
		}

		private static X509Certificate2 DownloadCert(string url)
        {
			byte[] cert = null;
			using (var webClient = new WebClient())
			{
				cert = webClient.DownloadData(url);
			}

			return new X509Certificate2(cert);
		}
	}
}
