﻿using Calmsen.Examples.Infrastructure.Provider;
using System;
using System.Globalization;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;

namespace Calmsen.Examples
{
    class FloatValueWrapper
    {
        public float Value { get; set; }
    }

    class DecimalValueWrapper
    {
        public decimal Value { get; set; }
    }

    internal class Program
    {
        private static async Task Main(string[] args)
        {
            await Task.WhenAll(Array.Empty<Task>());
           
            Console.ReadLine();
        }
        
    }

}
