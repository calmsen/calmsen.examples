﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calmsen.Examples
{
    public sealed class Singleton
    {
        private Singleton() { }

        public static Singleton Source { get { return Nested.source; } }

        private class Nested
        {
            static Nested()
            {
            }

            internal static readonly Singleton source = new Singleton();
        }
    }
}
