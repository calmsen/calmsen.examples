﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Calmsen.Examples
{
    internal class FooSingleton
    {
        public static string GreetingMessage = "Bar";
        public static FooSingleton Instance { get; } = new FooSingleton();
        public TcpClient Client { get; }

        private FooSingleton()
        {
            Console.WriteLine("FooSingleton.ctor");
            //Client = new TcpClient("127.0.0.1", 21);
            Client = new TcpClient("127.0.0.1", 21);
        }

        public void Foo()
        {
            Console.WriteLine("Foo");
        }

    }
}
