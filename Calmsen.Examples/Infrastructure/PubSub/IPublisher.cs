﻿using System;
using System.Threading.Tasks;

namespace Calmsen.Examples.Infrastructure.PubSub
{
    public interface IPublisher
    {
        void Subscribe<TMessage, TResult>(Func<TMessage, Task<TResult>> subscriber) where TMessage : IMessage;        
        void Unsubscribe<TMessage, TResult>(Func<TMessage, Task<TResult>> subscriber) where TMessage : IMessage;
        Task<TResult> Notify<TMessage, TResult>(TMessage message) where TMessage : IMessage;
    }
}
