﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Calmsen.Examples.Infrastructure.Provider;

namespace Calmsen.Examples.Infrastructure.PubSub
{
    public class Publisher : IPublisher, IHasProviderScope
    {
        public IProviderScope ProviderScope { get; set; }
        private Dictionary<Type, List<object>> _subscribers = new Dictionary<Type, List<object>>();
                
        public async Task<TResult> Notify<TMessage, TResult>(TMessage message) where TMessage : IMessage
        {
            var results = new List<TResult>();
            List<object> subscribersSet = GetSubscribersSet<TMessage>();
            foreach (var subscriber in subscribersSet)
            {
                var subscriberWithResult = subscriber as Func<TMessage, Task<TResult>>;
                if (subscriberWithResult != null)
                    results.Add(await subscriberWithResult(message));
                else
                    throw new Exception("Not Found Subscriber.");
            }
            if ((ProviderScope?.ParentScope?.Singletones?.ContainsKey("publisher") ?? false)
                && ProviderScope.ParentScope.Singletones["publisher"].TryGetTarget(out object publisherInParentScope))
            {
                if (publisherInParentScope is IPublisher publisher)
                {
                    results.Add(await publisher.Notify<TMessage, TResult>(message));
                }
            }
            return results.Any() ? results[0] : default(TResult);
        }


        public void Subscribe<TMessage, TResult>(Func<TMessage, Task<TResult>> subscriber) where TMessage : IMessage
        {
            List<object> subscribersSet = GetSubscribersSet<TMessage>();
            subscribersSet.Add(subscriber);
        }

        public void Unsubscribe<TMessage, TResult>(Func<TMessage, Task<TResult>> subscriber) where TMessage : IMessage
        {
            List<object> subscribersSet = GetSubscribersSet<TMessage>();

            for (int i = 0; i < subscribersSet.Count; i++)
            {
                if (Equals(subscriber, subscribersSet[i]))
                {
                    subscribersSet.RemoveAt(i);
                }
            }

            if (!subscribersSet.Any())
                _subscribers.Remove(typeof(TMessage));
        }
        
        private List<object> GetSubscribersSet<TMessage>()
        {
            var messageType = typeof(TMessage);
            if (!_subscribers.ContainsKey(messageType))
                _subscribers.Add(typeof(TMessage), new List<object>());
            var subscribersSet = _subscribers[messageType];
            return subscribersSet;
        }
    }
}
