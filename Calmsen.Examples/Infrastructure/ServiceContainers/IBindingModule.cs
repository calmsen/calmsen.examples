﻿namespace Calmsen.Examples.Infrastructure.ServiceContainers
{
    public interface IBindingModule
    {
        void Load(IServiceContainer serviceContainer);
    }
}
