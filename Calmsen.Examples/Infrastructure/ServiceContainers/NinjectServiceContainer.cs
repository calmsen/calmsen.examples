﻿using Ninject;
using Ninject.Syntax;
using Ninject.Extensions.Conventions;
using Ninject.Extensions.Conventions.Syntax;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Calmsen.Examples.Infrastructure.ServiceContainers
{
    public class NinjectServiceContainer : IServiceContainer
    {
        private IKernel _kernel;

        public void Init()
        {
            _kernel = new StandardKernel(
                new NinjectSettings
                {
                    InjectAttribute = typeof(InjectAttribute),
                    LoadExtensions = true
                }
            );
        }

        public void Load(params IBindingModule[] modules)
        {
            foreach (var module in modules)
            {
                module.Load(this);
            }
        }

        public void BindToSelf(Type type, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton)
        {
            _kernel.Unbind(type);
            var bindable = _kernel.Bind(type).ToSelf();
            SetScope(bindable, serviceContainerScope);
        }

        public void BindToSelf<TService>(ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton)
        {
            _kernel.Unbind<TService>();
            var bindable = _kernel.Bind<TService>().ToSelf();
            SetScope(bindable, serviceContainerScope);
        }

        public void BindToSelfOfConstant(object service, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton)
        {
            Type type = service.GetType();
            _kernel.Unbind(type);
            var bindable = _kernel.Bind(type).ToConstant(service);
            SetScope(bindable, serviceContainerScope);
        }

        public void BindToSelfOfConstant<TService>(TService service, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton)
        {
            _kernel.Unbind<TService>();
            var bindable = _kernel.Bind<TService>().ToConstant(service);
            SetScope(bindable, serviceContainerScope);
        }

        public void BindToImplementation(Type type, Type typeImpl, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true)
        {
            if (rebind)
                _kernel.Unbind(type);
            var bindable = _kernel.Bind(type).To(typeImpl);
            SetScope(bindable, serviceContainerScope);
        }

        public void BindToImplementation<TService, TImplementation>(ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true) where TImplementation : TService
        {
            if (rebind)
                _kernel.Unbind<TService>();
            var bindable = _kernel.Bind<TService>().To<TImplementation>();
            SetScope(bindable, serviceContainerScope);
        }

        public void BindToImplementationOfConstant(Type type, object service, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true)
        {
            if (rebind)
                _kernel.Unbind(type);
            var bindable = _kernel.Bind(type).ToConstant(service);
            SetScope(bindable, serviceContainerScope);
        }

        public void BindToImplementationOfConstant<TService, TImplementation>(TImplementation service, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true) where TImplementation : TService
        {
            if (rebind)
                _kernel.Unbind<TService>();
            var bindable = _kernel.Bind<TService>().ToConstant(service);
            SetScope(bindable, serviceContainerScope);
        }

        public void BindBaseType(Type baseType, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true)
        {
            if (rebind)
                _kernel.Unbind(baseType);

            if (baseType.IsInterface)
            {
                _kernel.Bind(x => x
                    .FromAssemblyContaining(baseType)
                    .SelectAllClasses()
                    .InheritedFrom(baseType)
                    .BindAllInterfaces()
                    .Configure(b => SetScope(b, serviceContainerScope))
                );
            } 
            else 
            {
                _kernel.Bind(x => x
                    .FromAssemblyContaining(baseType)
                    .SelectAllClasses()
                    .InheritedFrom(baseType)
                    .BindAllBaseClasses()
                    .Configure(b => SetScope(b, serviceContainerScope))
                );
            }
        }

        public void BindBaseType<TBaseService>(ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true)
        {
            if (rebind)
                _kernel.Unbind<TBaseService>();

            Type baseType = typeof(TBaseService);

            if (baseType.IsInterface)
            {
                _kernel.Bind(x => x
                    .FromAssemblyContaining<TBaseService>()
                    .SelectAllClasses()
                    .InheritedFrom<TBaseService>()
                    .BindAllInterfaces()
                    .Configure(b => SetScope(b, serviceContainerScope))
                );
            } 
            else
            {
                _kernel.Bind(x => x
                    .FromAssemblyContaining<TBaseService>()
                    .SelectAllClasses()
                    .InheritedFrom<TBaseService>()
                    .BindAllBaseClasses()
                    .Configure(b => SetScope(b, serviceContainerScope))
                );

            }
        }

        public void BindSelection(Type baseType, Predicate<Type> selector, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true)
        {
            if (rebind)
                _kernel.Unbind(baseType);

            _kernel.Bind(x => x
                .FromAssemblyContaining(baseType)
                .SelectAllClasses()
                .InheritedFrom(baseType)
                .BindSelection((type, baseTypes) => {
                    if (!selector(type))
                        return new Type[0];
                    return new Type[] { baseType };
                })
                .Configure(b => SetScope(b, serviceContainerScope))
            );
        }

        public void BindSelection<TBaseService>(Predicate<Type> selector, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true)
        {
            if (rebind)
                _kernel.Unbind<TBaseService>();

            Type baseType = typeof(TBaseService);

            _kernel.Bind(x => x
                .FromAssemblyContaining<TBaseService>()
                .SelectAllClasses()
                .InheritedFrom<TBaseService>()
                .BindSelection((type, baseTypes) => {
                    if (!selector(type))
                        return new Type[0];
                    return new Type[] { baseType };
                })
                .Configure(b => SetScope(b, serviceContainerScope))
            );
        }

        public void BindInNamespaceOf<TNamespace>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace>(
                x => x.InNamespaceOf<TNamespace>(),
                bindableType,
                serviceContainerScope,
                excluding
            );
        }

        public void BindInNamespaceOf<TNamespace1, TNamespace2>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace1>(
                x => x
                    .InNamespaceOf<TNamespace1>()
                    .InNamespaceOf<TNamespace2>(),
                bindableType,
                serviceContainerScope,
                excluding
            );
        }

        public void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace1>(
                x => x
                    .InNamespaceOf<TNamespace1>()
                    .InNamespaceOf<TNamespace2>()
                    .InNamespaceOf<TNamespace3>(),
                bindableType,
                serviceContainerScope,
                excluding
            );
        }

        public void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3, TNamespace4>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace1>(
                x => x
                    .InNamespaceOf<TNamespace1>()
                    .InNamespaceOf<TNamespace2>()
                    .InNamespaceOf<TNamespace3>()
                    .InNamespaceOf<TNamespace4>(),
                bindableType,
                serviceContainerScope,
                excluding
            );
        }

        public void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3, TNamespace4, TNamespace5>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace1>(
                x => x
                    .InNamespaceOf<TNamespace1>()
                    .InNamespaceOf<TNamespace2>()
                    .InNamespaceOf<TNamespace3>()
                    .InNamespaceOf<TNamespace4>()
                    .InNamespaceOf<TNamespace5>(),
                bindableType,
                serviceContainerScope,
                excluding
            );
        }

        public object Get(Type serviceType)
        {
            return _kernel.Get(serviceType);
        }

        public TService Get<TService>()
        {
            return _kernel.Get<TService>();
        }
               
        public IEnumerable<object> GetAll(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        public IEnumerable<TService> GetAll<TService>()
        {
            return _kernel.GetAll<TService>();
        }

        private void SetScope<T>(IBindingInSyntax<T> bindable, ServiceContainerScope serviceContainerScope)
        {
            switch (serviceContainerScope)
            {
                case ServiceContainerScope.Singleton:
                    bindable.InSingletonScope();
                    break;
                case ServiceContainerScope.Thread:
                    bindable.InThreadScope();
                    break;
                case ServiceContainerScope.Transient:
                    bindable.InTransientScope();
                    break;
                default:
                    throw new NotImplementedException();
            }

        }


        private void BindInNamespaceOf<TNamespace>(Func<IJoinFilterWhereExcludeIncludeBindSyntax, IJoinFilterWhereExcludeIncludeBindSyntax> namesapceProcessor, BindableType bindableType, ServiceContainerScope serviceContainerScope, Type[] excluding)
        {
            _kernel.Bind(x => {
                var joinFilterWhereExcludeIncludeBindSyntax = x.FromAssemblyContaining<TNamespace>().SelectAllClasses();
                joinFilterWhereExcludeIncludeBindSyntax = namesapceProcessor(joinFilterWhereExcludeIncludeBindSyntax);

                IBindSyntax bindSyntax = joinFilterWhereExcludeIncludeBindSyntax;
                if (excluding != null && excluding.Length > 0)
                    bindSyntax = joinFilterWhereExcludeIncludeBindSyntax.Excluding(excluding);
                var configureSyntax = Bind(joinFilterWhereExcludeIncludeBindSyntax, bindableType);
                configureSyntax.Configure(b => SetScope(b, ServiceContainerScope.Singleton));
            });
        }

        private IConfigureSyntax Bind(IBindSyntax bindSyntax, BindableType bindableType = BindableType.Self)
        {
            switch (bindableType)
            {
                case BindableType.AllBaseClasses:
                    return bindSyntax.BindAllBaseClasses();
                case BindableType.AllInterfaces:
                    return bindSyntax.BindAllInterfaces();
                case BindableType.DefaultInterface:
                    return bindSyntax.BindDefaultInterface();
                default:
                    return bindSyntax.BindToSelf();
            }
        }
    }
}
