﻿namespace Calmsen.Examples.Infrastructure.ServiceContainers
{
    public enum BindableType
    {
        Self = 1,
        AllBaseClasses = 2,
        AllInterfaces = 4,
        DefaultInterface = 8
    }
}
