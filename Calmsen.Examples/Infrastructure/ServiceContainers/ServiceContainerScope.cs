﻿namespace Calmsen.Examples.Infrastructure.ServiceContainers
{
    public enum ServiceContainerScope
    {
        Singleton,

        Thread,

        Transient
    }
}
