﻿using System;

namespace Calmsen.Examples.Infrastructure.ServiceContainers
{
    /// <summary>
    /// Служит для внедрения зависимостей. Является заменой InjectAttribute поставляемого от Ninject. 
    /// Так сделано чтобы избавиться от зависимостей к конкретному DI контейнеру и в случае необходимости можно настроить под любой DI контейнер
    /// </summary>
    [AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class InjectAttribute : Attribute
    {
    }
}
