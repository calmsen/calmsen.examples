﻿using System;

namespace Calmsen.Examples.Infrastructure.ServiceContainers
{
    public interface IServiceContainer
    {
        void Init();
        void Load(params IBindingModule[] modules);

        void BindToSelf(Type type, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton);
        void BindToSelf<TService>(ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton);

        void BindToSelfOfConstant(object service, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton);
        void BindToSelfOfConstant<TService>(TService service, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton);

        void BindToImplementation(Type type, Type typeImpl, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true);
        void BindToImplementation<TService, TImplementation>(ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true) where TImplementation : TService;

        void BindToImplementationOfConstant(Type type, object service, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true);
        void BindToImplementationOfConstant<TService, TImplementation>(TImplementation service, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true) where TImplementation : TService;

        void BindBaseType(Type baseType, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true);
        void BindBaseType<TBaseService>(ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true);

        void BindSelection(Type baseType, Predicate<Type> selector, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true);
        void BindSelection<TBaseService>(Predicate<Type> selector, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, bool rebind = true);

        void BindInNamespaceOf<TNamespace>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null);
        void BindInNamespaceOf<TNamespace1, TNamespace2>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null);
        void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null);
        void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3, TNamespace4>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null);
        void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3, TNamespace4, TNamespace5>(BindableType bindableType = BindableType.Self, ServiceContainerScope serviceContainerScope = ServiceContainerScope.Singleton, Type[] excluding = null);
    }
}
