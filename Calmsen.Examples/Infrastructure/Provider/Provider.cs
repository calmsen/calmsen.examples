﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Calmsen.Examples.Infrastructure.Provider
{
    /// <summary>
    /// Провайдер объектов. Объекты можно зарегистрировать с помощью метода Bind(), так и аннотируя атрибутом KeyAttribute.
    /// Объект может представлять из себя прототип (должен иметь реализацию ICloneable). В этом случае провайдер будет возвращать клон.
    /// Объект может иметь реализацию IInitable, что очень актуально если объект представлен ввиде прототипа, 
    /// потому что обычно клоны создаются неглубоким копированием, а инициализации свойств и полей в методе Init() устраняет эти недостатки.
    /// Так же в провайдере можно регистировать фабрики объектов.
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    public class Provider<TItem>
        where TItem : class
    {
        /// <summary>
        /// Зарегистрированные объекты
        /// </summary>
        protected Dictionary<string, TItem> _items = new Dictionary<string, TItem>();
        
        /// <summary>
        /// Зарегистрированные фабрики
        /// </summary>
        protected Dictionary<string, ITypeFactory<TItem>> _factories = new Dictionary<string, ITypeFactory<TItem>>();

        private ProviderScope RootProviderScope = new ProviderScope();

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="items"></param>
        /// <param name="factories"></param>
        public Provider(IEnumerable<TItem> items, IEnumerable<ITypeFactory<TItem>> factories)
        {
            BindByKeyAttributes(items);
            BindByKeyAttributes(factories);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual TItem Single(IProviderScope scope = null, bool isTransient = false)
        {
            return Get(_items.Keys.Single(), default(EmptyInputData), scope, isTransient);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual TItem Single<TData>(TData data, IProviderScope scope = null, bool isTransient = false)
        {
            return Get(_items.Keys.Single(), data, scope, isTransient);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual T Get<T>(IProviderScope scope = null, bool isTransient = false)
            where T : TItem
        {
            return (T)Get(ResolveKey(typeof(T)), default(EmptyInputData), scope, isTransient);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual T Get<T, TData>(TData data, IProviderScope scope = null, bool isTransient = false)
            where T : TItem
        {
            return (T)Get(ResolveKey(typeof(T)), data, scope, isTransient);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public virtual TItem Get(Type type, IProviderScope scope = null, bool isTransient = false)
        {
            return Get(ResolveKey(type), default(EmptyInputData), scope, isTransient);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public virtual TItem Get<TData>(Type type, TData data, IProviderScope scope = null, bool isTransient = false)
        {
            return Get(ResolveKey(type), data, scope, isTransient);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual TItem Get<TData>(TData data, IProviderScope scope = null, bool isTransient = false)
        {
            if (Equals(data, default(TData)))
                return null;

            string key = null;
            if (data is IHasKey)
                key = ((IHasKey)data).Key;
            if (key == null && data.GetType().IsValueType)
                key = data.ToString();
            if (key == null)
                key = ResolveKey(data.GetType());
            if (key == null)
                return null;

            return Get(key, data, scope, isTransient);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual TItem Get(string key, IProviderScope scope = null, bool isTransient = false)
        {
            return Get(key, default(EmptyInputData), scope, isTransient);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="isTransient">Если scope равен нулю: 
        /// 1) и объект получается из фабрики или клонируется, то isTransient присваивается в true, в независимости от значения передаваемого в аргументе
        /// 2) и объект не получается из фабрики и не клонируется, то isTransient присваивается в false, в независимости от значения передаваемого в аргументе
        /// 
        /// </param>
        /// <returns></returns>
        public virtual TItem Get<TData>(string key, TData data, IProviderScope scope = null, bool isTransient = false)
        {
            if (key == null)
                throw new ArgumentException();

            scope = scope ?? RootProviderScope;
            if (scope.ParentScope == null && scope != RootProviderScope)
                scope.ParentScope = RootProviderScope;

            if (GetItemFromScope(key, scope, out TItem item))
                return item;

            if (_factories.ContainsKey(key))
            {
                var factory = _factories[key];
                if (factory is IFactory<TItem> factoryWithoutInputData)
                    item = factoryWithoutInputData.Create();
                else if (factory is IFactory<TData, TItem> factoryWithInputData)
                    item = factoryWithInputData.Create(data);
                else
                    return null;

                if (scope == RootProviderScope)
                    isTransient = true;
            }                
            else if (_items.ContainsKey(key))
            {
                item = _items[key];
                if (item is ICloneable<TItem> cloneableItem)
                {
                    item = cloneableItem.Clone();
                    if (scope == RootProviderScope)
                        isTransient = true;
                }                    
                else
                {
                    if (scope == RootProviderScope)
                        isTransient = false;
                }
                if (item is IInitable initableItem)
                    initableItem.Init();
                else if (item is IInitable<TData> initableItemWithInputData)
                    initableItemWithInputData.Init(data);
            }
            SetItemToScopeIfNeed(key, scope, item, isTransient);
            return item;
        }

        /// <summary>
        /// Регистрирует объект
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        public virtual void Bind<T>(T item) where T : TItem
        {
            _items.Add(item.GetType().Name, item);
        }

        /// <summary>
        /// Регистрирует объект
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        public virtual void Bind(string key, TItem item)
        {
            _items.Add(key, item);
            string typeName = item.GetType().Name;
            if (!typeName.Equals(key))
            {
                _items.Add(typeName, item);
            }

        }

        /// <summary>
        /// Регистрирует фабрику
        /// </summary>
        /// <param name="key"></param>
        /// <param name="factory"></param>
        protected virtual void Bind(string key, ITypeFactory<TItem> factory)
        {
            _factories.Add(key, factory);
        }

        /// <summary>
        /// Регистрирует фабрику
        /// </summary>
        /// <param name="key"></param>
        /// <param name="factory"></param>
        public virtual void Bind(string key, IFactory<TItem> factory)
        {
            _factories.Add(key, factory);
        }

        /// <summary>
        /// Регистрирует фабрику
        /// </summary>
        /// <param name="key"></param>
        /// <param name="factory"></param>
        public virtual void Bind<TData>(string key, IFactory<TData, TItem> factory)
        {
            _factories.Add(key, factory);
        }

        /// <summary>
        /// Регистрирует фабрику
        /// </summary>
        /// <param name="key"></param>
        /// <param name="factory"></param>
        public virtual void Bind(string key, Func<TItem> factory)
        {
            _factories.Add(key, new DelegateFactory<TItem>(factory));
        }

        /// <summary>
        /// Регистрирует фабрику
        /// </summary>
        /// <param name="key"></param>
        /// <param name="factory"></param>
        public virtual void Bind<TData>(string key, Func<TData, TItem> factory)
        {
            _factories.Add(key, new DelegateFactory<TData, TItem>(factory));
        }

        /// <summary>
        /// Удаляет объект или фабрику
        /// </summary>
        /// <param name="key"></param>
        public virtual void Unbind(string key)
        {
            _items.Remove(key);
            _factories.Remove(key);
        }

        /// <summary>
        /// Очищает полностью все объекты и фабрики
        /// </summary>
        public void Clear()
        {
            _items.Clear();
            _factories.Clear();
        }
        /// <summary>
        /// Регистрирует все объекты по атрибутам
        /// </summary>
        /// <param name="items"></param>
        protected void BindByKeyAttributes(IEnumerable<TItem> items)
        {
            if (items == null)
                return;

            foreach (var item in items)
            {
                var attrs = item.GetType().GetCustomAttributes(true).OfType<KeyAttribute>();
                if (attrs.Any())
                {
                    foreach (var attr in attrs)
                    {
                        string key = attr.Key;
                        Bind(key, item);
                    }
                }
                else
                {
                    string key = ResolveKey(item.GetType());
                    Bind(key, item);
                }

            }
        }

        /// <summary>
        /// Регистрирует все фабрики по атрибутам
        /// </summary>
        /// <param name="factories"></param>
        protected void BindByKeyAttributes(IEnumerable<ITypeFactory<TItem>> factories)
        {
            if (factories == null)
                return;

            foreach (var factory in factories)
            {
                string factoryName = factory.GetType().Name;
                if (factoryName.IndexOf("DelegateFactory") >= 0)
                    continue;
                var attrs = factory.GetType().GetCustomAttributes(true).OfType<KeyAttribute>();
                if (attrs.Any())
                {
                    foreach (var attr in attrs)
                    {
                        string key = attr.Key;
                        Bind(key, factory);
                    }
                }
                else
                {
                    string key = Regex.Replace(factoryName, "Factory$", "");
                    Bind(key, factory);
                }
            }
        }

        /// <summary>
        /// Возвращает ключ по типу объекта
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        protected virtual string ResolveKey(Type itemType)
        {
            if (itemType.IsGenericType)
            {
                return itemType.Name.Substring(0, itemType.Name.IndexOf("`")) +
                    ("<" + string.Join(">,<", itemType.GetGenericArguments().Select(x => x.Name).ToArray()) + ">");
            }
            return itemType.Name;
        }

        private bool GetItemFromScope(string key, IProviderScope providerScope, out TItem item)
        {
            if (providerScope != null && providerScope.Singletones.ContainsKey(key))
            {
                var weakItem = providerScope.Singletones[key];
                if (weakItem.TryGetTarget(out object itemInContext))
                {
                    item = itemInContext as TItem;
                    return true;
                } 
                else
                {
                    providerScope.Singletones.Remove(key);
                }
                    
            }
            item = default(TItem);
            return false;
        }

        private void SetItemToScopeIfNeed(string key, IProviderScope providerScope, TItem item, bool isTransient = false)
        {
            if (item is IHasProviderScope itemWithContext)
                itemWithContext.ProviderScope = providerScope;

            if (!isTransient)
                providerScope.Singletones.Add(key, new WeakReference<object>(item, true));
        }

    }
}
