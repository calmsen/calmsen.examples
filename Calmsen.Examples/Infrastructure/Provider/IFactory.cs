﻿
namespace Calmsen.Examples.Infrastructure.Provider
{

    /// <summary>
    /// Интерфейс фабрики, который можно зарегистрировать в Provider<TContext, TItem>
    /// </summary>
    public interface ITypeFactory<TItem>
    {
    }

    /// <summary>
    /// Интерфейс фабрики, который можно зарегистрировать в Provider<TContext, TItem>
    /// </summary>
    /// <typeparam name="TItem">Абстрактный объект, который создается фабрикой</typeparam>
    public interface IFactory<TItem> : ITypeFactory<TItem>
    {
        /// <summary>
        /// Создает объект
        /// </summary>
        /// <returns></returns>
        TItem Create();
    }


    /// <summary>
    /// Интерфейс фабрики, который можно зарегистрировать в Provider<TContext, TItem>
    /// </summary>
    /// <typeparam name="TData">Данные которые передаются в фабричный метод</typeparam>
    /// <typeparam name="TItem">Абстрактный объект, который создается фабрикой</typeparam>
    public interface IFactory<TData, TItem>: ITypeFactory<TItem>
    {
        /// <summary>
        /// Создает объект
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        TItem Create(TData data);
    }
}
