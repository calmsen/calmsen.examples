﻿using System;
using System.Collections.Generic;

namespace Calmsen.Examples.Infrastructure.Provider
{
    public interface IProviderScope
    {
        Dictionary<string, WeakReference<object>> Singletones { get; }
        IProviderScope ParentScope { get; set; }
    }
}
