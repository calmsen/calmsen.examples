﻿namespace Calmsen.Examples.Infrastructure.Provider
{
    /// <summary>
    /// Интерфейс для инициализации объекта 
    /// </summary>
    public interface IInitable
    {
        /// <summary>
        /// Инициализирует объект
        /// </summary>
        void Init();
    }


    /// <summary>
    /// Интерфейс для инициализации объекта 
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public interface IInitable<TData>
    {
        /// <summary>
        /// Инициализирует объект
        /// </summary>
        /// <param name="data"></param>
        void Init(TData data);
    }
}
