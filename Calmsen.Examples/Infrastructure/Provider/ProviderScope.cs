﻿using System;
using System.Collections.Generic;

namespace Calmsen.Examples.Infrastructure.Provider
{
    public class ProviderScope: IProviderScope
    {
        public Dictionary<string, WeakReference<object>> Singletones { get; } = new Dictionary<string, WeakReference<object>>();
        public IProviderScope ParentScope { get; set; }
    }
}
