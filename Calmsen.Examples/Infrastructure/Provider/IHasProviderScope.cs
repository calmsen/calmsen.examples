﻿namespace Calmsen.Examples.Infrastructure.Provider
{
    public interface IHasProviderScope
    {
        IProviderScope ProviderScope { get;set;}
    }
}
