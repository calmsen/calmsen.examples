﻿using System;

namespace Calmsen.Examples.Infrastructure.Provider
{
    /// <summary>
    /// Фабрика реализующая интерфейс, но по факту фабрикой является делегат, переданный в конструктор.
    /// Используется в Provider<TItem> для регистрации фабрик, не имеющих внешних зависимостей.
    /// Еси фабрика имеет зависимости, то следует создать полноцееный класс, реализующий интерфейс фабрики.
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    public sealed class DelegateFactory<TItem> : IFactory<TItem>
    {
        /// <summary>
        /// Делегат фабрики
        /// </summary>
        private readonly Func<TItem> _factory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="factory"></param>
        public DelegateFactory(Func<TItem> factory)
        {
            _factory = factory;
        }

        /// <summary>
        /// Создает объект
        /// </summary>
        /// <returns></returns>
        public TItem Create()
        {
            return _factory();
        }
    }


    /// <summary>
    /// Фабрика реализующая интерфейс, но по факту фабрикой является делегат, переданный в конструктор.
    /// Используется в Provider<TItem> для регистрации фабрик, не имеющих внешних зависимостей.
    /// Еси фабрика имеет зависимости, то следует создать полноцееный класс, реализующий интерфейс фабрики.
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TItem"></typeparam>
    public sealed class DelegateFactory<TContext, TItem> : IFactory<TContext, TItem>
    {
        /// <summary>
        /// Делегат фабрики
        /// </summary>
        private readonly Func<TContext, TItem> _factory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="factory"></param>
        public DelegateFactory(Func<TContext, TItem> factory)
        {
            _factory = factory;
        }

        /// <summary>
        /// Создает объект
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public TItem Create(TContext context)
        {
            return _factory(context);
        }
    }
}
