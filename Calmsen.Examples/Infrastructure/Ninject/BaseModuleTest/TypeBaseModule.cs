﻿
namespace Calmsen.Examples.Infrastructure.Ninject.BaseModuleTest
{
    public class TypeBaseModule<T>: BaseModule
    {
        public T Prop1 { get; set; }
    }
}
