﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;
using Calmsen.Examples.Infrastructure.Ninject.BaseModuleTest;
using System;
using System.Collections.Generic;
using Calmsen.Examples.Infrastructure.Provider;

namespace Calmsen.Examples.Infrastructure.Ninject
{
    public class MyNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x => x
            .FromAssemblyContaining<BaseModule>()
            .SelectAllClasses()
            .InheritedFrom<BaseModule>()
            .BindSelection(BaseModuleServiceSelector)
            .Configure(b =>
                b.InSingletonScope()
            ));
            Bind<BaseModule>().To<TypeBaseModule<Type2>>().InSingletonScope();
            Bind<BaseModule>().To<TypeBaseModule<Type3>>().InSingletonScope();
            Bind(typeof(Provider<>)).ToSelf().InSingletonScope();
        }

        private IEnumerable<Type> BaseModuleServiceSelector(Type type, IEnumerable<Type> baseTypes)
        {
            if (type.IsGenericType)
                return new Type[0];
            return new Type[] { typeof(BaseModule) };
        }
    }
}
