﻿using Ninject;

namespace Calmsen.Examples.Infrastructure.Ninject
{
    public class NinjectServiceProvider
    {
        private StandardKernel _kernel;

        public void Init()
        {
            _kernel = new StandardKernel(
                new NinjectSettings
                {
                    AllowNullInjection = false
                },
                new MyNinjectModule()
                );
        }

        public T Get<T>()
        {
            return _kernel.Get<T>();
        }
    }
}
