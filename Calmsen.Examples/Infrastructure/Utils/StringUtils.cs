﻿namespace Calmsen.Examples.Infrastructure.Utils
{
    public class StringUtils
    {
        public string GetNullOrSubstringOfSpecificLength(string input, int length)
        {
            return input?.Length > length
                ? input.Substring(0, length)
                : input;
        }
    }
}
