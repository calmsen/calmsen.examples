﻿using System.Collections.Generic;
using System.Linq;

namespace Calmsen.Examples.Infrastructure.Utils
{
    public class KeyValuePairUtils
    {
        public string MapParametersToString(List<KeyValuePair<string, object>> parametres)
        {
            return parametres.Select(x => $"name: {x.Key}, value: {x.Value}").Aggregate((result, item) => $"{result}, {item}");
        }
    }
}
