﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Calmsen.Owin.Security.WebApp.Startup))]
namespace Calmsen.Owin.Security.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
