﻿using Owin;
using System;

namespace Calmsen.Owin.Security
{
    public static class CustomAuthenticationExtensions
    {
        public static IAppBuilder UseCustomAuthentication(this IAppBuilder app,
            CustomAuthenticationOptions options)
        {
            if (app == null)
                throw new ArgumentNullException(nameof(app));
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            app.Use(typeof(CustomAuthenticationMiddleware), app, options);

            return app;
        }
    }
}
