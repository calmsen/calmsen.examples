﻿
namespace Calmsen.Owin.Security
{
    public class CustomAuthenticationEndpoints
    {
        /// <summary>
        /// Endpoint which is used to redirect users to request Custom access
        /// </summary>
        /// <remarks>        /// </remarks>
        public string AuthorizationEndpoint { get; set; }

        /// <summary>
        /// Endpoint which is used to exchange code for access token
        /// </summary>
        /// <remarks>
        /// </remarks>
        public string TokenEndpoint { get; set; }

        /// <summary>
        /// Endpoint which is used to obtain user information after authentication
        /// </summary>
        /// <remarks>
        /// </remarks>
        public string UserInfoEndpoint { get; set; }
    }
}
