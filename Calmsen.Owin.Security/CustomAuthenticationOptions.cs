﻿using Calmsen.Owin.Security.Provider;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calmsen.Owin.Security
{
    public class CustomAuthenticationOptions : AuthenticationOptions
    {
        /// <summary>
        ///     Gets or sets timeout value in milliseconds for back channel communications with Custom.
        /// </summary>
        /// <value>
        ///     The back channel timeout in milliseconds.
        /// </value>
        public TimeSpan BackchannelTimeout { get; set; }

        /// <summary>
        ///     The request path within the application's base path where the user-agent will be returned.
        ///     The middleware will process this request when it arrives.
        ///     Default value is "/signin-vk".
        /// </summary>
        public PathString CallbackPath { get; set; }

        /// <summary>
        ///     Get or sets the text that the user can display on a sign in user interface.
        /// </summary>
        public string Caption
        {
            get { return Description.Caption; }
            set { Description.Caption = value; }
        }

        /// <summary>
        ///     Gets or sets the Custom supplied Client ID
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        ///     Gets or sets the Custom supplied Client Secret
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// Gets the sets of OAuth endpoints used to authenticate against Custom.
        /// </summary>
        public CustomAuthenticationEndpoints Endpoints { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="ICustomAuthenticationProvider" /> used in the authentication events
        /// </summary>
        public ICustomAuthenticationProvider Provider { get; set; }

        /// <summary>
        /// A list of permissions to request.
        /// </summary>
        public IList<string> Scope { get; set; }

        /// <summary>
        /// Type of displayed page. Possible values: page, popup and mobile. Default: page.
        /// </summary>
        public string Display { get; set; }

        /// <summary>
        ///     Gets or sets the name of another authentication middleware which will be responsible for actually issuing a user
        ///     <see cref="System.Security.Claims.ClaimsIdentity" />.
        /// </summary>
        public string SignInAsAuthenticationType { get; set; }

        /// <summary>
        ///     Gets or sets the type used to secure data handled by the middleware.
        /// </summary>
        public ISecureDataFormat<AuthenticationProperties> StateDataFormat { get; set; }

        /// <summary>
        ///     Initializes a new <see cref="CustomAuthenticationOptions" />
        /// </summary>
        public CustomAuthenticationOptions(string defaultAuthenticationType)
            : base(defaultAuthenticationType)
        {
            Caption = defaultAuthenticationType;
            CallbackPath = new PathString($"/signin-{defaultAuthenticationType.ToLower()}");
            AuthenticationMode = AuthenticationMode.Passive;
            Scope = new List<string>();
            BackchannelTimeout = TimeSpan.FromSeconds(60);
            Endpoints = new CustomAuthenticationEndpoints
            {
                AuthorizationEndpoint = "<AuthorizationEndpoint>",
                TokenEndpoint = "<TokenEndpoint>",
                UserInfoEndpoint = "<UserInfoEndpoint>"
            };
        }
    }
}
