﻿using System;
using System.Globalization;
using System.Net.Http;
using Calmsen.Owin.Security.Provider;
using Microsoft.Owin;
using Microsoft.Owin.Logging;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Infrastructure;
using Owin;

namespace Calmsen.Owin.Security
{
    public class CustomAuthenticationMiddleware : AuthenticationMiddleware<CustomAuthenticationOptions>
    {
        private readonly ILogger _logger;

        public CustomAuthenticationMiddleware(OwinMiddleware next, IAppBuilder app,
            CustomAuthenticationOptions options)
            : base(next, options)
        {
            if (string.IsNullOrWhiteSpace(Options.ClientId))
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "The '{0}' option must be provided.", "ClientId"));
            if (string.IsNullOrWhiteSpace(Options.ClientSecret))
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture,
                    "The '{0}' option must be provided.", "ClientSecret"));

            SetDefaults(app);

            _logger = app.CreateLogger<CustomAuthenticationMiddleware>();
        }

        /// <summary>
        ///     Provides the <see cref="T:Microsoft.Owin.Security.Infrastructure.AuthenticationHandler" /> object for processing
        ///     authentication-related requests.
        /// </summary>
        /// <returns>
        ///     An <see cref="T:Microsoft.Owin.Security.Infrastructure.AuthenticationHandler" /> configured with the
        ///     <see cref="T:Owin.Security.Providers.Custom.GitHubAuthenticationOptions" /> supplied to the constructor.
        /// </returns>
        protected override AuthenticationHandler<CustomAuthenticationOptions> CreateHandler()
        {
            return new CustomAuthenticationHandler(_logger);
        }

        private void SetDefaults(IAppBuilder app)
        {
            if (Options.Provider == null)
                Options.Provider = new CustomAuthenticationProvider();

            if (Options.StateDataFormat == null)
            {
                var dataProtector = app.CreateDataProtector(
                    typeof(CustomAuthenticationMiddleware).FullName,
                    Options.AuthenticationType, "v1");
                Options.StateDataFormat = new PropertiesDataFormat(dataProtector);
            }

            if (string.IsNullOrEmpty(Options.SignInAsAuthenticationType))
                Options.SignInAsAuthenticationType = app.GetDefaultSignInAsAuthenticationType();
        }
    }
}
